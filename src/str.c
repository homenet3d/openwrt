/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
* This file contains functions that manipulate data into buffers ready to be 
* sent over websockets.
*
* see [insert path here] for details on the websockets message protocol syntax
*/

#include "str.h"


/*
* attr_to_string(attr)
* description:
*   This function converts an enumerated attribute into a string name that is
*   then printed to the 'value' char array. 
* 
* passed:
*   attr: an integer as an enumerated attribute (see homenet3d.h -> enum attribs)
* 
*/
void attr_to_string(char * value, int attr){
	switch(attr){
		case POSITION:
			value = "position";
			break;
		case BOUNCE_HEIGHT:
			sprintf(value,"bounce_height");
			break;
		case BOUNCE_FREQ:
			sprintf(value,"bounce_freq");
			break;
		case ROTATE_SPEED:
			sprintf(value,"rotate_speed");
			break;
		case COLOUR:
			sprintf(value,"colour");
			break;
		case LABEL:
			sprintf(value,"label");
			break;
		case RADIUS:
			sprintf(value,"radius");
			break;
		case SCALE:
			sprintf(value,"scale");
			break;
		case SHAPE:
			sprintf(value,"shape");
			break;
		case SYS_HOST:
			sprintf(value,"hostname");
			break;
		case SYS_SYS:
			sprintf(value,"system");
			break;
		case SYS_MODEL:
			sprintf(value,"model");
			break;
		case SYS_UPTIME:
			sprintf(value,"uptime");
			break;
		case MEM_TOTAL:
			sprintf(value,"mem_total");
			break;
		case MEM_INUSE:
			sprintf(value,"mem_inuse");
			break;
		case MEM_CACHED:
			sprintf(value,"mem_cached");
			break;
		case MEM_BUFFERS:
			sprintf(value,"mem_buffers");
			break;
		case MEM_FREE:
			sprintf(value,"mem_free");
			break;
		case LAN_IP:
			sprintf(value,"lan_ip");
			break;
		case LAN_NM:
			sprintf(value,"lan_nm");
			break;
		case LAN_GW:
			sprintf(value,"lan_gw");
			break;
		case WAN_IP:
			sprintf(value,"wan_ip");
			break;
		case WAN_NM:
			sprintf(value,"wan_nm");
			break;	
		case WAN_GW:
			sprintf(value,"wan_gw");
			break;
		case ACT_CONNS:
			sprintf(value,"act_conns");
			break;
		case MAX_CONNS:
			sprintf(value,"max_conns");
			break;
		case RX_RATE:
			sprintf(value,"rx_rate");
			break;
		case TX_RATE:
			sprintf(value,"tx_rate");
			break;	
		case RADIO:
			sprintf(value,"radios");
			break;
		case LEASES:
			sprintf(value,"leases");
			break;
		case NAT_TABLE:
			sprintf(value,"nat");
			break;
		default:
			sprintf(value,"");
			break;
	}
}

/*
* string_to_attr(attr)
* description:
*   This function converts a string name into an enumerated attribute. 
*   (see homenet3d.h -> enum attribs)
* 
* passed:
*   attr: a string of the attribute 
* 
* returns:
*    An int of the enumerated attribute
*/
int string_to_attr(char *attr){
	int attr_id = -1;
	if(strstr(attr,"position"))
		attr_id = POSITION;
	else if(strstr(attr,"rotation"))
		attr_id = ROTATION;
	else if(strstr(attr,"state_types"))
		attr_id = STATETYPES;
	else if(strstr(attr,"bounce_height"))
		attr_id = BOUNCE_HEIGHT;
	else if(strstr(attr,"bounce_freq"))
		attr_id = BOUNCE_FREQ;
	else if(strstr(attr,"rotate_speed"))
		attr_id = ROTATE_SPEED;
	else if(strstr(attr,"colour"))
		attr_id = COLOUR;
	else if(strstr(attr,"text_colour"))
		attr_id = TEXT_COLOUR;
	else if(strstr(attr,"label"))
		attr_id = LABEL;
	else if(strstr(attr,"radius"))
		attr_id = RADIUS;
	else if(strstr(attr,"scale"))
		attr_id = SCALE;
	else if(strstr(attr,"shape"))
		attr_id = SHAPE;
	
	return attr_id;
}

/*
* array_to_string(buf,id,attr)
* description:
*   This function prints an array attribute into a buffer to be used in 
*   a message.
*
*   types of arrays:
*     colour/text_colour: int array of RGB format colour
*     rotation: double array of x,y,z vavlues
*     position: int arry of x,y,z 3D positioning
*     statetypes: char pointer arrays of the state_types of the entity
*     mappings: mapping_t array of metric to attribute mappings
* 
* passed:
*   buf: A char buffer where the printed colour is stored
*   id: the ID of the entity whose attribute is being printed
*   attr: an integer as an enumerated attribute (see homenet3d.h -> enum attribs)
* 
*/
void array_to_string(char *buf, entity_t *entity, int attr){
  int i,j;
  char temp[512];
  char str[2048] = "";
  *temp = 0;
  *str = 0;
  switch(attr){
    case COLOUR:
      if(entity->colour)
        sprintf(buf,"\"colour\":[%d~%d~%d]", entity->colour[RED], entity->colour[GREEN], entity->colour[BLUE]);
      break;
    case TEXT_COLOUR:
      if(entity->text_colour)
        sprintf(buf,"\"text_colour\":[%d~%d~%d]", entity->text_colour[RED], entity->text_colour[GREEN], entity->text_colour[BLUE]);
      break;
    case ROTATION:
      if(entity->rotation)
        sprintf(buf,"\"rotation\":[%.5f~%.5f~%.5f]", entity->rotation[X_COORD], entity->rotation[Y_COORD], entity->rotation[Z_COORD]);
      break;
    case POSITION:
    if(entity->position)
      sprintf(buf,"\"position\":[%d~%d~%d]", entity->position[X_COORD], entity->position[Y_COORD], entity->position[Z_COORD]);
      break;
    case STATETYPES:
      for(i = 0; i < entity->state_type_count; i++){
        if(i == entity->state_type_count-1)
          sprintf(temp,"%s", entity->state_types[i]);
        else
          sprintf(temp,"%s~", entity->state_types[i]);
        strcat(str,temp);
      }
      sprintf(buf,"\"state_types\":[%s]",str);
      break;
    case MAPPINGS:
      if(entity->mappings)
      for(i = 0; i < entity->mappings_count; i++){
        sprintf(temp,"metric:[%s~%s~%s]attrs:{", entity->mappings[i].metric.name, entity->mappings[i].metric.upper_thres, entity->mappings[i].metric.lower_thres);
        strcat(str,temp);
        for(j = 0; j < entity->mappings[i].attrs_count; j++){
          sprintf(temp,"[%s~%s~%s~%s];",entity->mappings[i].attrs[j].name, entity->mappings[i].attrs[j].max, entity->mappings[i].attrs[j].min, entity->mappings[i].attrs[j].gran);
          strcat(str,temp);
        }
        sprintf(temp,"}");
        strcat(str,temp);
      }
      sprintf(buf,"\"mappings\":{%s}",str);
      break;
  }
}

/*
* leases_to_string(buf,group_id)
* description:
*   This function prints all leases of a group into a buffer to be used in
*   a message.
*
* passed:
*   buf: A char buffer where the printed leases are stored
*   group_id: the ID of the group whose lease is being printed
*
*/
void leases_to_string(char * buf, int bufsize, group_t *group){
  char temp[256], ret[10] = "";
  char *str;
  struct lease_struct *lease;
  str = (char *)malloc(bufsize);
  *str = 0;
  TAILQ_FOREACH(lease,&group->leases,next){
    if(lease->expiry != -1){
      if(lease->wifi)
        sprintf(ret,"true");
      else
        sprintf(ret,"false");
      sprintf(temp,"%s~%d~%d~%s~%s~%s~%s;", lease->mac, lease->num, lease->expiry,  lease->ip, lease->name, lease->id,ret);
      strcat(str,temp);
      sprintf(temp,"");
    }
  }
  sprintf(buf,"\"leases\":[%s]",str);
  free(str);
}
/*
* leaseToString(buf,group_id,index)
* description:
*   This function prints a particular lease into a buffer to be used in
*   a message.
*
* passed:
*   buf: A char buffer where the printed lease is stored
*   group_id: the ID of the group whose lease is being printed
*   index: the index of the lease to be printed
*
*/
void lease_to_string(char * buf, int group_id, int index){
  char ret[10];
  group_t *group;
  struct lease_struct *lease;
  group = &(find_group(group_id)->group);
  
  TAILQ_FOREACH(lease,&group->leases,next){
    if(lease->num == index)
      break;
  }
  
  if(lease->wifi)
    sprintf(ret,"true");
  else
    sprintf(ret,"false");
  
  sprintf(buf,"[%s~%d~%d~%s~%s~%s~%s;]", lease->mac, index, lease->expiry, lease->ip, lease->name, lease->id, ret);
}

/*
* wifis_to_string(buf,group)
* description:
*   This function prints all wifi radios for a given group into a buffer 
*   to be used in a message.
*
* passed:
*   buf: A char buffer where the printed radios are stored
*   group: a pointer to the group of whose wifi info will be printed
*
*/
void wifis_to_string(char * buf, int bufsize, group_t *group){
  char temp[1899];
  char *str = NULL;
  struct radio_struct *radio;
  str = (char *)malloc(bufsize);
  *str = 0;
  TAILQ_FOREACH(radio,&group->radios,next){
    if(radio->channel != -1){
      sprintf(temp,"%d~%s~%s~%s~%s;", radio->channel, radio->mode, radio->ssid, radio->interface, radio->assocs);
      strcat(str,temp);
    }
  }
  sprintf(buf,"\"radios\":[%s]",str);
}

/*
* wifi_to_string(buf,group_id,radio_id)
* description:
*   This function prints a particular wifi radio info for a particular 
*   group into a buffer to be used in a message.
*
* passed:
*   buf: A char buffer where the printed radios are stored
*   group_id: the ID of the group whose wifi radio is being printed
*   radio_id: the id of the radio being printed
*/
void wifi_to_string(char * buf,int group_id, int radio_id){
  group_t *group;
  struct radio_struct *radio;
  group = &(find_group(group_id)->group);
  TAILQ_FOREACH(radio,&group->radios,next){
    if(radio->id == radio_id)
      break;
  }
  sprintf(buf,"[%d~%s~%s~%s~%s;]", radio->channel, radio->mode, radio->ssid, radio->interface, radio->assocs);
  if(debug)
      lwsl_notice("DEBUG: wifiToString -> %s\r\n",buf);
}


/*
* nats_to_string(buf,group)
* description:
*   This function prints all nat entries into a buffer to be used in
*   a message.
*
* passed:
*   buf: A char buffer where the printed nat entries are stored
*   group_id: a pointer to the group of whose nat table info will be printed
*
*/
void nats_to_string(char * buf, int bufsize,group_t *group){
  char temp[250] = "", *str;
	str = (char *)malloc(bufsize);
  *str = 0;
  struct nat_entry_struct *entry;
  
  TAILQ_FOREACH(entry,&group->nat_table,next){
    
    if(entry){
      if (entry->src == NULL)
        entry->src = "";
      if (entry->dst == NULL)
        entry->dst = "";
      if (entry->layer3 == NULL)
        entry->layer3 = "";
      if (entry->layer4 == NULL)
        entry->layer4 = "";
      sprintf(temp,"%d~%d~%d~%d~%d~%s~%s~%s~%s;", entry->id, entry->bytes, entry->sport, entry->dport, entry->packets, entry->src, entry->dst, entry->layer3, entry->layer4);
      strcat(str,temp);
      sprintf(temp, "");
    }
  }
	sprintf(buf,"\"nat\":[%s]",str);
	free(str);
}

/*
* nat_to_string(buf,group_id,index)
* description:
*   This function prints a particular nat entry into a buffer to be used
*   in a message.
*
* passed:
*   buf: A char buffer where the printed nat entry is stored
*   group_id: the ID of the group whose nat entry is being printed
*   index: the id of the nat entry being printed
*/
void nat_to_string(char * buf,int group_id, int index){
  group_t *group;
  char *str;
	str = (char *)malloc(500);
  *str = 0;
  group = &(find_group(group_id)->group);
  struct nat_entry_struct *entry = NULL;
  TAILQ_FOREACH(entry,&group->nat_table,next){
    if(entry->id == index)
      break;
  }
  if(entry){
    sprintf(buf,"[%d~%d~%d~%d~%d~%s~%s~%s~%s;]", entry->id, entry->bytes, entry->sport, entry->dport, entry->packets, entry->src, entry->dst, entry->layer3, entry->layer4);
  }
  else
    sprintf(buf,"[]");
  free(str);
  if(debug)
    lwsl_notice("DEBUG: natToString -> %s\r\n",buf);
}



/*
* entToString(buf,entity)
* description:
*   This function prints an entity with all it's attributes to a buffer in 
*   the form of a message ready to be sent.
* 
* passed:
*   buf: A char buffer where the printed message is stored
*   entity: a pointer to the entity which is being printed
*
*/
void ent_to_string(char * buf, entity_t *entity){
  char *term_str = "}}]}";
  char colour[40] = "", text_colour[40] = "", rotation[100] = "", position[200] = "";
  char state_types[2048] = "", mappings[4096] = "";
  array_to_string(colour,entity,COLOUR);
  array_to_string(text_colour,entity,TEXT_COLOUR);
  array_to_string(rotation,entity,ROTATION);
  array_to_string(position,entity,POSITION);
  array_to_string(state_types,entity,STATETYPES);
  array_to_string(mappings,entity,MAPPINGS);
  sprintf(buf,"{\"name\":\"spawn_entity\",\"args\":[{\"id\":%d"
              ",\"group_id\":%d"
              ",\"element\":{%s"
              ",%s"
              ",\"bounce_height\":%i"
              ",\"bounce_freq\":%.3f"
              ",\"rotate_speed\":%.3f"
              ",%s"
              ",%s"
              ",\"label\":%s"
              ",\"radius\":%i"
              ",\"scale\":%.3f"
              ",\"shape\":%i"
              ",%s"
              ",%s"
              ",\"width\":%d,\"height\":%d,"
              ",\"objfile\":%s,\"mtlfile\":%s%s",
              entity->id,
              entity->group_id,
              position, 
              state_types,
              entity->bounce_height, 
              entity->bounce_freq, 
              entity->rotate_speed, 
              colour, 
              text_colour, 
              entity->label, 
              entity->radius, 
              entity->scale, 
              entity->shape, 
              rotation,
              mappings,
              entity->width,entity->height,
              entity->objfile,entity->mtlfile,term_str);
              // fprintf(stderr,"Ent to String: %s\n",buf);
}


void group_to_string(char * buf, group_t *group){
  char *term_str = "}}]}";
  int nat_size = 0, lease_size = 0, radio_size = 0;
  char *leases = NULL, *radios = NULL, *nat_table = NULL;
  nat_size = get_update_bufsize(NAT_TABLE, -1, group->id, 0);
  lease_size = get_update_bufsize(LEASES, -1, group->id, 0);
  radio_size = get_update_bufsize(RADIO, -1, group->id, 0);
  if(debug)
    lwsl_notice("DEBUG: group_to_string() -> NAT buffer size: %d,LEASES buffer size: %d, RADIOS buffer size: %d\n",nat_size,lease_size,radio_size);
  nat_table = (char *)malloc(nat_size);
  leases = (char *)malloc(lease_size);
  radios = (char *)malloc(radio_size);
  *nat_table = 0;
  *leases = 0;
  *radios = 0;
  leases_to_string(leases,lease_size,group);
  wifis_to_string(radios,radio_size,group);
	nats_to_string(nat_table,nat_size,group);
  sprintf(buf,"{\"name\":\"spawn_info\",\"args\":[{\"group_id\":%d"
              ",\"element\":{"
              "\"hostname\":%s"
              ",\"system\":%s"
              ",\"model\":%s"
              ",\"uptime\":%d"
              ",\"mem_inuse\":%d"
              ",\"mem_buffers\":%d"
              ",\"mem_cached\":%d"
              ",\"mem_free\":%d"
              ",\"mem_total\":%d"
              ",\"lan_ip\":%s"
              ",\"lan_gw\":%s"
              ",\"lan_nm\":%s"
              ",\"wan_ip\":%s"
              ",\"wan_gw\":%s"
              ",\"wan_nm\":%s"
              ",\"act_conns\":%d"
              ",\"max_conns\":%d"
              ",\"rx_rate\":%d"
              ",\"tx_rate\":%d"
              ",%s"
              ",%s"
							",%s%s",
              group->id,
              group->hostname,
              group->system,
              group->model,
              group->uptime,
              group->mem_inuse, 
              group->mem_buffers, 
              group->mem_cached, 
              group->mem_free,
              group->mem_total,
              group->lan_ip,
              group->lan_gw,
              group->lan_nm,
              group->wan_ip,
              group->wan_gw,
              group->wan_nm,
              group->act_conns,
              group->max_conns,
              group->rx_rate,
              group->tx_rate,
              leases, 
              radios,
							nat_table, term_str
              );
              
	free(nat_table);
  free(leases);
  free(radios);
  // fprintf(stderr,"Group to String: %s\n",buf);
}

/*
* updateToString(buf,attr,value,id,index)
* description:
*   This function prints an entity update to a buffer in the form of a 
*   message ready to be sent.
* 
* passed:
*   buf: A char buffer where the printed message is stored
*   attr: the attribute (enum) to be updated
*   value: the new value of the entity's attribute
*   id: the ID of the entity which is being printed
*   index: if the attribute is an array this should be the array index 
*          for the element to be updated. Else this should be -1
*
*/
void update_to_string(char * buf,int attr, const char *value, int group_id, int index){
  char *strs[4] = {"{\"name\":\"update_info\",\"args\":[{\"group_id\":",
  ",\"element\":{\"",
  "\":",
  "}}]}"};
  char attr_string[256];
  char *buf2 = (char *)malloc(8192);
  *buf2 = 0;
  if(debug)
    lwsl_notice("DEBUG: updateToString() -> Index:%d\r\n",index);
  attr_to_string(attr_string,attr);
  if(index != -1){
    if(attr == LEASES){
      lease_to_string(buf2,group_id,index);
    }
    else if(attr == RADIO){
      wifi_to_string(buf2,group_id,index);
    }
		else if(attr == NAT_TABLE){
			nat_to_string(buf2,group_id,index);
		}
  }
  else{
    sprintf(buf2,"%s",value);
  }
  sprintf(buf,"%s%d%s%s%s%s%s",strs[0],group_id,strs[1],attr_string,strs[2],buf2,strs[3]);
  free(buf2);
}


int get_update_bufsize(int attr, int index, int group_id, int msglen){
  group_t *group = NULL;
  struct nat_entry_struct *entry = NULL;
  struct lease_struct *lease = NULL;
  struct radio_struct *radio = NULL;
  int bufsize = 0;
  group = &(find_group(group_id)->group);
  if(attr == NAT_TABLE){
    TAILQ_FOREACH(entry,&group->nat_table,next){
      if(index >= 0){
        if(entry->id == index){
          bufsize += 8*5; // Add enough buffer space for the 5 integers (id,sport,dport,packets,bytes)
          if(entry->src)
            bufsize += strlen(entry->src);
          if(entry->dst)
            bufsize += strlen(entry->dst);
          if(entry->layer3)
            bufsize += strlen(entry->layer3);
          if(entry->layer4)
            bufsize += strlen(entry->layer4);
          break;
        }
      }
      else{
        bufsize += 8*5; // Add enough buffer space for the 5 integers (id,sport,dport,packets,bytes)
        bufsize += strlen(entry->src) + strlen(entry->dst) + strlen(entry->layer3) + strlen(entry->layer4);
      }
    }
  }
  else if(attr == LEASES){
    TAILQ_FOREACH(lease,&group->leases,next){
      if(index >= 0){
        if(lease->num == index){
          bufsize += 8*3; // Add enough buffer space for the 3 integers (num,expiry,(bool)wifi)
          bufsize += strlen(lease->ip) + strlen(lease->mac) + strlen(lease->name) + strlen(lease->id);
          break;
        }
      }
      else{
        bufsize += 8*5; // Add enough buffer space for the 5 integers (id,sport,dport,packets,bytes)
        bufsize += strlen(lease->ip) + strlen(lease->mac) + strlen(lease->name) + strlen(lease->id);
      }
    }
  }
  else if(attr == RADIO){
    TAILQ_FOREACH(radio,&group->radios,next){
      if(radio->id == index){
        bufsize += 8*2; // Add enough buffer space for the 2 integers (id,channel)
        bufsize += strlen(radio->mode) + strlen(radio->ssid) + strlen(radio->interface) + strlen(radio->assocs);
        break;
      }
    }
  }
  else
    bufsize += msglen;
  
  bufsize += 110; // Add enough buffer for message overhead.
  
  return bufsize;
}