/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/


#ifndef CONF_H_
#define CONF_H_

#include "homenet3d.h"

int config_start(const char* filename);
void parse_app_config(char * name, char * version, char * url);
int config_get_ws_port();
int config_get_s_port();
int get_entity_count(int group);
int get_group_count();
void get_ip(const char *buf, int group_id);

entity_t get_entity_config(int group,int id);
entity_t get_entity(entity_t *source, entity_t* to_merge);

void parse_config(config_setting_t *cfg);
void parse_entity_groups(config_setting_t * setting);
void parse_entities(config_setting_t *setting, int group);
void parse_entity_attr(entity_t *entity, config_setting_t *setting);
void parse_list(entity_t *entity,config_setting_t *setting);
void parse_array(entity_t *entity, config_setting_t *setting);
void parse_mapping(mapping_t* mapping,config_setting_t *setting);
void parse_mapping_metric(metric_t *metric,config_setting_t *setting);
void parse_attr(attr_t *attr,config_setting_t *setting);
void parse(entity_t *entity, config_setting_t *setting);

void add_entry(int id, entity_t entity);
void delete_entry(entity_config_t *config);
void delete_all();
entity_config_t *find_entry(int id);


#endif