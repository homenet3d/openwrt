/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
* This file contains functions that create and manipulate entities and 
* groups.
*/

#include "entity.h"


/*
 * entity shapes 
 *   these should be set in the config file 
 *   (default location: /usr/share/homenet3d/homenet3d.cfg):
 * 1: cube
 * 2: sphere
 * 3: crappy tetrahedron that doesn't work well
 * 4: star
 * 5: cylinder
 * 6: plane
 * 7: OBJ/MTL model (need to specify obj and mtl files in config file)
 *
 * These correlate to the switch() statement in www-threejs/js/entity.js
 *
 */
group_hash_t *groups_table = NULL;

/*
* createWorld()
* description:
*   Reads from the config file and instantiates all the groups and their 
*   entities with their settings as defined in said config file.
* 
*/
void create_world(){
  group_t *groups;
	int i,j;
  group_count = get_group_count();
  
	/* 
	* This loop cycles through each entity and reads their configs.
	*/
  groups = (group_t *)malloc(group_count*sizeof(group_t));
  entity_count = (int *)malloc(sizeof(int)* group_count);
  for( i = 0; i < group_count; i++)
  {
    init_group(&(groups[i]));
    entity_count[i] = get_entity_count(i);
    groups[i].host_ip = (const char *)malloc(NI_MAXHOST);
    get_ip(groups[i].host_ip,i);
    groups[i].hostname = ""; 
    groups[i].system = ""; 
    groups[i].model = ""; 
    groups[i].lan_ip = "";
    groups[i].lan_nm = "";
    groups[i].lan_gw = "";
    groups[i].wan_ip = "";
    groups[i].wan_nm = "";
    groups[i].wan_gw = "";
    groups[i].entities = (entity_t *)malloc((entity_count[i]+1)* sizeof(entity_t));
  	for(j = 1; j <= entity_count[i]; j++){
      groups[i].entities[j] = get_entity_config(i*100,j);
      groups[i].entities[j].id = j;
      groups[i].entities[j].group_id = i;
      
      // if(!groups[i].entities[j].label)
   //      groups[i].entities[j].label = "";
   //    if(!groups[i].entities[j].url)
   //      groups[i].entities[j].url = "";
   //    if(!groups[i].entities[j].objfile)
   //      groups[i].entities[j].objfile = "";
   //    if(!groups[i].entities[j].mtlfile)
   //      groups[i].entities[j].mtlfile = "";
      if(debug)
        print_entity(&(groups[i].entities[j]));
  	}
    add_group(i,groups[i]);
  }
}


/*
* init_entity(entity)
* description:
*   This function sets initial values for an entity. 
*     ints      -> -1
*     doubles   -> -1.0
*     pointers  -> NULL
* 	
* passed:
*   entity: a pointer to the entity to be initialised
*   
*/
void init_entity(entity_t *entity){
  int default_int = -1;
  double default_float = -1.0;
  void *ptr = NULL;
  
	entity->id = default_int;
  entity->group_id = default_int;
  entity->width = default_int;
  entity->height = default_int;
	entity->bounce_height = default_int; 
  entity->bounce_freq = default_float; 
  entity->radius = default_int;
	entity->scale = default_float; 
  entity->rotate_speed = default_float;
	entity->shape = default_int; 
  entity->state_type_count = default_int;
  
  entity->position = ptr;
  entity->colour = ptr;
  entity->text_colour = ptr;
  entity->rotation = ptr;
	entity->label = ptr;
  entity->url = ptr;
  entity->objfile = ptr;
  entity->mtlfile = ptr;
  entity->state_types = ptr;
	entity->mappings_count = default_int;
  entity->mappings = ptr;
}

/*
* init_group(group)
* description:
*   This function sets initial values for a group. 
*     ints      -> -1
*     pointers  -> NULL
* 	
* passed:
*   group: a pointer to the group to be initialised
*   
*/
void init_group(group_t *group){
  int default_int = -1;
  void *ptr = NULL;
  group->entities = ptr;
    
  group->host_ip = ptr;
  
  // For System state type
  group->hostname = ptr; 
  group->system = ptr; 
  group->model = ptr; 
  group->uptime = default_int;
  
  // For Memory state types
  group->mem_inuse = default_int;
  group->mem_cached = default_int;
  group->mem_buffers = default_int;
  group->mem_free = default_int;
  group->mem_total = default_int;
  
  // For Network state type
  group->lan_ip = ptr;
  group->lan_nm = ptr;
  group->lan_gw = ptr;
  group->wan_ip = ptr;
  group->wan_nm = ptr;
  group->wan_gw = ptr;
  group->act_conns = default_int;
  group->max_conns = default_int;
  group->rx_rate = default_int;
  group->tx_rate = default_int;
  
   // For Wireless state type
	TAILQ_INIT(&(group->radios));
   // For DHCP Leases state type
  TAILQ_INIT(&(group->leases));
   // For NAT table state type
	TAILQ_INIT(&(group->nat_table));
	
}


/*
* print_entity(entity)
* description:
*   This function prints the contents of a entity to stderr (console)
* 	
* passed:
*   entity: a pointer to the entity to be printe
*   
*/
void print_entity(entity_t *entity){
  int i,j;
  fprintf(stderr, "Entity %d:\n",entity->id);
  if(entity->state_types){
    fprintf(stderr,"  State Types: ");
    for(i = 0; i < entity->state_type_count; i++){
      fprintf(stderr, "%s, ",entity->state_types[i]);
    }
    fprintf(stderr, "\n");
  }
  if(entity->position)
    fprintf(stderr, "  Position: %d,%d,%d\n",entity->position[X_COORD],entity->position[Y_COORD],entity->position[Z_COORD]);
  if(entity->colour)
    fprintf(stderr, "  Colour: %d,%d,%d\n",entity->colour[RED],entity->colour[GREEN],entity->colour[BLUE]);
  if(entity->text_colour)
    fprintf(stderr, "  Text Colour: %d,%d,%d\n",entity->text_colour[RED],entity->text_colour[GREEN],entity->text_colour[BLUE]);
  if(entity->rotation)
    fprintf(stderr, "  Rotation: %f,%f,%f\n",entity->rotation[X_COORD],entity->rotation[Y_COORD],entity->rotation[Z_COORD]);
  if(entity->bounce_height >= 0)
    fprintf(stderr, "  Bounce Height: %d\n",entity->bounce_height);
  if(entity->bounce_freq >= 0)
    fprintf(stderr, "  Bounce Freq: %f\n",entity->bounce_freq);
  if(entity->radius >= 0)
    fprintf(stderr, "  Radius: %d\n",entity->radius);
  if(entity->scale >= 0)
    fprintf(stderr, "  Scale: %f\n",entity->scale);
  if(entity->rotate_speed >= 0)
    fprintf(stderr, "  Rotate Speed: %f\n",entity->rotate_speed);
  if(entity->shape >= 0)
    fprintf(stderr, "  Shape: %d\n",entity->shape);
  if(entity->width >= 0)
    fprintf(stderr, "  Width: %d\n",entity->width);
  if(entity->height >= 0)
    fprintf(stderr, "  Height: %d\n",entity->height);
  if(entity->label)
    fprintf(stderr, "  Label: %s\n",entity->label);
  if(entity->url)
    fprintf(stderr, "  URL: %s\n",entity->url);
  if(entity->objfile)
    fprintf(stderr, "  OBJ File: %s\n",entity->objfile);
  if(entity->mtlfile)
    fprintf(stderr, "  MTL File: %s\n",entity->mtlfile);
  
  if(entity->mappings){
    fprintf(stderr, "  Mappings:\n");
    for(i = 0; i < entity->mappings_count; i++){
      fprintf(stderr, "    Attr: %s\n", entity->mappings[i].metric.name);
      fprintf(stderr, "      Upper Threshold: %s\n", entity->mappings[i].metric.upper_thres);
      fprintf(stderr, "      Lower Threshold: %s\n", entity->mappings[i].metric.lower_thres);
      for(j = 0; j < entity->mappings[i].attrs_count; j++){
        fprintf(stderr, "    Metric: %s\n", entity->mappings[i].attrs[j].name);
        fprintf(stderr, "      Max: %s\n", entity->mappings[i].attrs[j].max);
        fprintf(stderr, "      Min: %s\n", entity->mappings[i].attrs[j].min);
        fprintf(stderr, "      Granularity: %s\n", entity->mappings[i].attrs[j].gran);
      }
    }
  }
}


/*
* print_group(group)
* description:
*   This function prints the contents of all the entities in a group
*   to stderr (console)
* 	
* passed:
*   group_id: the ID of the group whose entities are to be printed
*   entity_count: the number of entities in the group
*   
*/
void print_group(int group_id, int entity_count){
  int i;
  group_t *group;
  
  fprintf(stderr, "Group %d\n", group_id);
  for(i = 1; i <= entity_count; i++)
  {
    group = &(find_group(group_id)->group);
    print_entity(&(group->entities[i]));
  }
}


/*
* load_entity(wsi,entity)
* description:
*   This function loads an entity to a client.
* 
* passed:
*   wsi: the websocket identifier for a single client connection
*   entity: the entity to be loaded
*/
void load_entity(struct libwebsocket * wsi, entity_t *entity){
	char message[13000];
	ent_to_string(message,entity);
	libwebsocket_write(wsi,(unsigned char*)message,strlen(message),LWS_WRITE_TEXT);
	if(debug)
	  lwsl_notice("DEBUG: loadEntity() -> Message sent: %s",message);
}

/*
* load_entities(wsi)
* description:
*   This function loads all entities to a client.
* 
* passed:
*   wsi: the websocket identifier for a single client connection
*/
void load_entities(struct libwebsocket *wsi){
	int i,j;
	for(i = 0; i < group_count; i++){
    for(j = 1; j <= entity_count[i]; j++){      
  		load_entity(wsi,&(find_group(i)->group.entities[j]));
    }
	}
}

/*
* load_entities(wsi)
* description:
*   This function loads all groups and their associated system state information 
*   to a client.
* 
* passed:
*   wsi: the websocket identifier for a single client connection
*/
void load_system_state(struct libwebsocket *wsi){
  char * message = NULL;
  int i = 0, j = 0;
  struct nat_entry_struct *temp;
  group_t *group;
  for(i = 0; i < group_count; i++){
    group = &(find_group(i)->group);
    TAILQ_FOREACH(temp,&group->nat_table,next){
      j++;
    }
    message = (char *)malloc(13000+j*70);
    group_to_string(message,group);
    libwebsocket_write(wsi,(unsigned char*)message,strlen(message),LWS_WRITE_TEXT);
  	if(debug)
  	  lwsl_notice("DEBUG: loadState() -> Message sent: %s!",message);
  }
}

/*
* update_client(wsi)
* description:
*   This function gets called on a Libwebsocket callback and is looped for 
*   each of the clients. It will loop through all queued updates until each
*   client is send each update.
* 
* passed:
*   wsi: the websocket identifier for a single client connection
* 
*/
void update_client(struct libwebsocket *wsi, int no_client){
	int i = 0, j = 0, bufsize = 0, fd;
	char *message = NULL;
  bool fd_serviced = false;
  fd = libwebsocket_get_socket_fd(wsi);
	if(debug)
	  lwsl_notice("DEBUG: updateAllClients() -> Number of Updates: %d",top);
	for (i = top; i >= 0; i--){
    usleep(200);
    bufsize = get_update_bufsize(updates[i].attr, updates[i].index,updates[i].group_id, strlen(updates[i].message));
    message = (char *)malloc(bufsize);
    memset(&message[0], 0, bufsize);
  	update_to_string(message,updates[i].attr, updates[i].message, updates[i].group_id, updates[i].index);
    
    if(debug)
		  lwsl_notice("DEBUG: update_client() -> Update info:  ID: %d, Index: %d, Attr: %d, Message: %s\r\n", updates[i].group_id, updates[i].index, updates[i].attr, updates[i].message);
		libwebsocket_write(wsi,(unsigned char*)message,strlen(message),LWS_WRITE_TEXT);
    if(debug)
		  lwsl_notice("DEBUG: update_client() -> Update message: %s",message);
    
    if(updates[i].clients_size == 0){
      updates[i].clients[updates[i].clients_size++] = fd;
      fd_serviced = true;
    }
    else{
      for(j = 0; j < updates[i].clients_size; j++){
        if(fd == updates[i].clients[j]){
          fd_serviced = true;
          break;
        }
      }
    }
    if(!fd_serviced){
      updates[i].clients[updates[i].clients_size++] = fd;
    }
    if(updates[i].clients_size == no_client || updates[i].clients_size == MAX_CLIENTS )
      pop_update(updates,i);
    
    free(message);
	}
}


/*
* set_group_value(attr,value,group_id)
* description:
*   This function updates a group's attribute with a new value.
*   For a list of attributes see homenet3d.h -> enum attribs
* passed:
*   attr: an enum of the group attribute to be updated
*   value: a string containing the value the attribute will be set to
*   group_id: the ID of the group to be updated
* 
*/
void set_group_value(int attr, const char * value, int group_id){
  group_t * group;
  group = &(find_group(group_id)->group);
	if(debug)
	    lwsl_notice("DEBUG: Set Group Entity %i-> Attr: %i -> Value: %s\r\n",group_id,attr,value);
	switch(attr){
		case SYS_HOST:
      group->hostname = (const char*)malloc(strlen(value) * sizeof(char));
		  group->hostname = value;
			break;
		case SYS_SYS:
      group->system = (const char*)malloc(strlen(value) * sizeof(char));
		  group->system = value;
			break;
		case SYS_MODEL:
      group->model = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->model = value;
			break;
		case SYS_UPTIME:
      group->uptime = atoi(value);
			break;
    case MEM_TOTAL:
      group->mem_total = atoi(value);
      break;
    case MEM_BUFFERS: 
			group->mem_buffers = atoi(value);
			break;
    case MEM_CACHED:
			group->mem_cached = atoi(value);
			break;
	  case MEM_FREE:
			group->mem_free = atoi(value);
			break;
		case MEM_INUSE:
			group->mem_inuse = atoi(value);
			break;
		case LAN_IP:
      group->lan_ip = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->lan_ip = value;
			break;
		case LAN_NM:
      group->lan_nm = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->lan_nm = value;
			break;
		case LAN_GW:
      group->lan_gw = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->lan_gw = value;
			break;
		case WAN_IP:
      group->wan_ip = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->wan_ip = value;
			break;
		case WAN_NM:
      group->wan_nm = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->wan_nm = value;
			break;
		case WAN_GW:
      group->wan_gw = (const char*)malloc(strlen(value) * sizeof(char));
  	  group->wan_gw = value;
			break;
		case ACT_CONNS:
      if(group->act_conns == atoi(value))
        update_needed = false;
      else
  			group->act_conns = atoi(value);
			break;
		case MAX_CONNS:
			group->max_conns = atoi(value);
			break;
		case RX_RATE:
			group->rx_rate = atoi(value);
			break;
		case TX_RATE:
			group->tx_rate = atoi(value);
			break;
		default:
			break;
	}
}


/*
* set_array_group_value(attr,value,group_id,index)
* description:
*   This function updates a group's array attribute with a new value.
*   For a list of attributes see homenet3d.h -> enum attribs
*
* passed:
*   attr: an enum of the group attribute to be updated
*   value: a string containing the value the attribute will be set to
*   group_id: the ID of the group to be updated
*   index: the index of the array element to be updated
*/
void set_array_group_value(int attr,const char * value,int group_id, int index){
	switch(attr){
		case RADIO:
			update_wifi(value,group_id,index);
			break;
		case LEASES:
			update_lease(value,group_id,index);
			break;
    case NAT_TABLE:
      update_nat_table(value,group_id,index);
      break;
	}
}

/*
* update_wifi(value,group_id,index)
* description:
*   This functions updates a wifi radio for a given group. It will overwrite the 
*   radio information at group->radios[index]
* 
* 	The value string contains the radio info in the format:
*      [WLAN interface]~[channel]~[SSID]~[WLAN mode]~[Associated Devices]
*   The associate devices are in the format:
*      [mac1$mac2$ ... macn$]
*
*   Wifi update example:
*      wlan0~11~OpenWRT~AP~00-01-02-03-04-05$06-07-08-09-0a-0b$
* 	
* passed:
*   value: A string containing the new values of the radio
*   group_id: the ID of the group to be updated
*   index: The index of the attribute array to be set
* 
*/
void update_wifi(const char * value_in, int group_id, int index){
	char *tok = NULL;
  char * value = strdup(value_in);
	struct radio_struct *new_radio;
  group_t * group;
  group = &(find_group(group_id)->group);
	
  TAILQ_FOREACH(new_radio,&group->radios,next){
    if(new_radio->id == index)
      break;
  }
  if(!new_radio){
    new_radio = (struct radio_struct *)malloc(sizeof(struct radio_struct));
    new_radio->id = index;
    new_radio->channel = -1;
    new_radio->mode = strdup("");
    new_radio->ssid = strdup("");
    new_radio->assocs = strdup("");
    new_radio->interface = strdup("");
    TAILQ_INSERT_HEAD(&(group->radios), new_radio, next);
    
    // if(debug)
//         lwsl_notice("DEBUG: update_nat_table() -> entry %s.\n",TAILQ_FIRST(&(group->nat_table))->id);
  } 
  
  
	lwsl_notice("NOTICE: updateWifi() -> Update Radio Value: %s\n",value);
	tok = strsep(&value,"~");
  new_radio->interface = (char *)malloc(strlen(tok)+1);
	new_radio->interface = strdup(tok);
	tok = strsep(&value,"~");
	new_radio->channel = atoi(tok);
	tok = strsep(&value,"~");
  new_radio->ssid = (char *)malloc(strlen(tok)+1);
  new_radio->ssid = strdup(tok);
	tok = strsep(&value,"~");
  new_radio->mode = (char *)malloc(strlen(tok)+1);
  new_radio->mode = strdup(tok);
	tok = strsep(&value,"~");
  new_radio->assocs = (char *)malloc(strlen(tok)+1);
  new_radio->assocs = strdup(tok);
	
	if(debug)
	  lwsl_notice("DEBUG: updateWifi() -> Update Radio:%s,%d,%s,%s,%s\r\n",new_radio->interface, new_radio->channel, new_radio->ssid, new_radio->mode, new_radio->assocs);
}

/*
* update_lease(value,group_id,index)
* description:
*   This functions updates a DHCP lease for a given group. It will overwrite the lease 
*   information at group->leases[index]
* 
* 	The value string contains the radio info in the format:
*      [expiry date in seconds]~[MAC]~[IP]~[Hostname]~[Client ID]~[Wireless (true/false)]
*
*   Lease update example:
*      15963475~00-01-02-03-04-05~192.168.1.10~iPhone~01-00-01-02-03-04-05
* 	
* passed:
*   value: A string containing the new values of the lease
*   group_id: the ID of the group to be updated
*   index: The index of the attribute array to be set
* 
*/
void update_lease(const char* value_in, int group_id, int index){
	char *tok = NULL;
  char * value = strdup(value_in);
	struct lease_struct *temp;
  group_t * group;
  group = &(find_group(group_id)->group);
  
  TAILQ_FOREACH(temp,&group->leases,next){
    if(temp->num == index)
      break;
  }
  if(!temp){
    temp = (struct lease_struct *)malloc(sizeof(struct lease_struct));
    temp->num = index;
    temp->expiry = -1;
    temp->wifi = false;
    temp->mac = strdup("");
    temp->ip = strdup("");
    temp->name = strdup("");
    temp->id = strdup("");
    TAILQ_INSERT_HEAD(&(group->leases), temp, next);
    
    // if(debug)
//         lwsl_notice("DEBUG: update_nat_table() -> entry %s.\n",TAILQ_FIRST(&(group->nat_table))->id);
  } 
  if(debug)
  	lwsl_notice("DEBUG: updateLease() -> Update Lease Value: %s\n",value);
	tok = strsep(&value,"~");
	temp->expiry = atoi(tok);
	tok = strsep(&value,"~");
  temp->mac = (char *)malloc(strlen(tok) + 1);
  strcpy(temp->mac,tok);

	tok = strsep(&value,"~");
  temp->ip = (char *)malloc(strlen(tok) + 1);
  strcpy(temp->ip,tok);

	tok = strsep(&value,"~");
  temp->name = (char *)malloc(strlen(tok) + 1);
  strcpy(temp->name,tok);

	tok = strsep(&value,"~");
  temp->id = (char *)malloc(strlen(tok) + 1);
  strcpy(temp->id,tok);

	if (!temp->wifi)
	    temp->wifi = is_wifi_lease(temp->mac,group_id);
	else
      temp->wifi = true;
  
  if(debug)
    lwsl_notice("DEBUG: updateLease() -> Update Lease:%d,%d,%s,%s,%s,%s\r\n",temp->num, temp->expiry, temp->mac, temp->ip, temp->name, temp->id);
}

/*
* is_wifi_lease(mac,group_id)
* description:
*   This functions checks if a device is connected wirelessly by 
*   matching it's mac address to a device associated to a wireless
*   radio.
* 	
* passed:
*   mac: The MAC address of the lease.
*   group_id: the ID of the group to be updated.
*   
* returns:
*   true if the lease's device is connected wirelessly. 
*   false if not.
*/
bool is_wifi_lease(char * mac, int group_id){
	bool ret = false;
  group_t * group;
  group = &(find_group(group_id)->group);
  struct radio_struct *new_radio;
  if(debug)
  	lwsl_notice("DEBUG: isWifiLease() -> Mac:%s\r\n",mac);
	TAILQ_FOREACH(new_radio,&group->radios,next){
    if(new_radio->assocs)
  		if(strstr(new_radio->assocs,mac))
  			ret = true;
	}
	return ret;
}

/*
* update_nat_table(value,group_id,index)
* description:
*   This functions updates a attribute of a NAT table entry for a given group. 
*   It will update the nat entry at group->nat_table[index]
*   
*   'value' will be in the form of: 
*     [attr]_[value]
* 
*   E.G. for a nat entry where the source port is 80 (http) the char array 'value' will be:
*     sport_80 
* 	
*   NAT attributes are:
*     bytes,sport,dport,packets (integers)
*     src,dst,layer3,layer4 (strings)
*
* passed:
*   value: A string containing the new values of the lease
*   group_id: the ID of the group to be updated
*   index: The index of the attribute array to be set
* 
*/
void update_nat_table(const char *value_in, int group_id, int index){
  char * tok = NULL;
  char * value = strdup(value_in);
	char *nat_attr = NULL, *nat_value = NULL;
  struct nat_entry_struct *temp;
	group_t *group = &(find_group(group_id)->group);
  if(debug)
    lwsl_notice("DEBUG: update_nat_table() -> value %s index %d.\n",value_in, index);
  if(strstr(value,"_")){
    tok = strsep(&value,"_");
    nat_attr = (char *)malloc(strlen(tok)+1);
    strcpy(nat_attr,tok);
    tok = strsep(&value,"_");
    nat_value = (char *)malloc(strlen(tok)+1);
    strcpy(nat_value,tok);
    
    TAILQ_FOREACH(temp,&group->nat_table,next){
      if(temp->id == index)
        break;
    }
    if(!temp){
      temp = (struct nat_entry_struct *)malloc(sizeof(struct nat_entry_struct));
      temp->id = index;
      temp->bytes = 0;
      temp->sport = 0;
      temp->dport = 0;
      temp->packets = 0;
      temp->src = strdup("");
      temp->dst = strdup("");
      temp->layer3 = strdup("");
      temp->layer4 = strdup("");
      TAILQ_INSERT_HEAD(&(group->nat_table), temp, next);
      
      // if(debug)
//         lwsl_notice("DEBUG: update_nat_table() -> entry %s.\n",TAILQ_FIRST(&(group->nat_table))->id);
    } 
    if(debug)
      lwsl_notice("DEBUG: update_nat_table() -> entry %d.\n",temp->id);
    
    if(index <= group->act_conns && temp){
      if(strcmp(nat_attr,"bytes") == 0){
    		temp->bytes = atoi(nat_value);
      }
      else if(strcmp(nat_attr,"sport") == 0){
    		temp->sport = atoi(nat_value);
      }
      else if(strcmp(nat_attr,"dport") == 0){
    		temp->dport = atoi(nat_value);
      }
      else if(strcmp(nat_attr,"packets") == 0){
    		temp->packets = atoi(nat_value);
      }
      else if(strcmp(nat_attr,"src") == 0){
    		temp->src = (char *)malloc(strlen(nat_value)+1);
    		sprintf(temp->src,"%s",nat_value);
        if(debug)
          lwsl_notice("DEBUG: update_nat_table() -> src: %s",temp->src);
      }
      else if(strcmp(nat_attr,"dst") == 0){
    		temp->dst = (char *)malloc(strlen(nat_value)+1);
    		sprintf(temp->dst,"%s",nat_value);
      }
      else if(strcmp(nat_attr,"layer3") == 0){
    		temp->layer3 = (char *)malloc(strlen(nat_value)+1);
    		sprintf(temp->layer3,"%s",nat_value);
      }
      else if(strcmp(nat_attr,"layer4") == 0){
    		temp->layer4 = (char *)malloc(strlen(nat_value)+1);
    		sprintf(temp->layer4,"%s",nat_value);
      }
    }
    if(debug)
      lwsl_notice("DEBUG: update_nat_table() -> nat_attr: %s nat_value %s.\n",nat_attr,nat_value);
    free(nat_value);
    free(nat_attr);
  }
}


/*
* add_group(group_id,group)
* description:
*   adds a group struct to the group hash table
* 	
* passed:
*   group_id: the ID of the group to be added.
*   group: the group object to be added.
*   
*/
void add_group(int group_id, group_t group) {
    group_hash_t *s;
    int id = group_id*100;
    HASH_FIND_INT(groups_table, &id, s);  /* id already in the hash? */
    if (s==NULL) {
      s = (group_hash_t*)malloc(sizeof(group_hash_t));
      s->id = id;
      HASH_ADD_INT( groups_table, id, s );  /* id: name of key field */
    }
    s->group = group;
    s->group.id = group_id;
    
    if(debug)
      lwsl_notice("NOTICE: add_group() -> Adding Group %d\n",s->group.id);
}

/*
* delete_group(group)
* description:
*   deletes a group struct from the group hash table
* 	
* passed:
*   group: the group object to be deleted.
*   
*/
void delete_group(group_hash_t *group) {
    HASH_DEL( groups_table, group);  /* user: pointer to delete */
    free(group);              /* optional; it's up to you! */
}

/*
* delete_all_groups()
* description:
*  deletes all groups from the group hash table
* 	
*/
void delete_all_groups() {
  group_hash_t *config, *tmp;

  HASH_ITER(hh, groups_table, config, tmp) {
    HASH_DEL(groups_table,config);  /* delete; users advances to next */
    free((void*)config->group.host_ip);
    free((void*)config->group.hostname); 
    free((void*)config->group.system); 
    free((void*)config->group.model); 
    free((void*)config->group.lan_ip);
    free((void*)config->group.lan_nm);
    free((void*)config->group.lan_gw);
    free((void*)config->group.wan_ip);
    free((void*)config->group.wan_nm);
    free((void*)config->group.wan_gw);
    free((void*)config->group.entities);
    free(config);            /* optional- if you want to free  */
  }
}

/*
* find_group(group_id)
* description:
*   Gets a pointer to a group_hash object for a given hash table id
* 	
* passed:
*   group_id: the group ID
*
* returns:
*   A pointer to the to group_hash table entry assocaited with the id.
*   If the entry cannot be matched it returns a NULL pointer.
*   
*/
group_hash_t * find_group(int group_id) {
    group_hash_t *s = NULL;
    int id = group_id*100;
    
    HASH_FIND_INT( groups_table, &id, s );  /* s: output pointer */
    return s;
}
