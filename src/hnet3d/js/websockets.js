/*
Copyright (c) 2012-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan				(6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/


/*
*	Javascript websockets client
*/

/*
*  Global variables 
*      websocketServer -> Initialises the websocket server to be accessed 
*                  on TCP port 10001
*      data ->     used to store the parsed message and create/update
*                  the entities 
*      lastHit ->  Used by info box to make sure dynamic updates change the 
*                  info for the currently displayed entity.
*/
var websocketServer = window.location.hostname.toString() + ":10001";
var data = new Array();



/*
*   WebSocket communication
*       If WebSockets is supported the browser connects to the websocket
*       server and waits for messages to parse.
*/
if ("WebSocket" in window){
	var connection = new WebSocket("ws://" + websocketServer);
  console.log("Websocket created");
	connection.onopen = function(){
		console.log("Connection Open");
	}
	
	connection.onclose = function(){
		console.log("Connection Closed");
	}
	
	connection.onerror = function(error){
		console.log('Error detected: ' + error);
	}
	
	connection.onmessage = function(e){
		var server_message = e.data;
		
		data = parseMessage(server_message);
		// console.log("Event:" + data.name);
    console.log("Received Message: " + server_message);

		switch(data.name){
			case "\"spawn_info\"":
        // console.log("Received Message: " + server_message);
        if(typeof groups[data.group_id] === 'undefined')
        groups[data.group_id] = new Group(data.group_id);
				for (var key in data.element){
					// console.log(data.id + " - " + key + " - " + data.element[key][0]);
					groups[data.group_id].update(key, data.element[key]);
				}
        console.log("Spawned Group " + data.group_id + " Info");
				break;
			case "\"update_info\"":
				for (var key in data.element){
					// console.log(data.id + " - " + key + " - " + data.element[key][0]);
					groups[data.group_id].update(key, data.element[key]);
				}
        console.log("Update: " + server_message);
				break;
			case "\"spawn_entity\"":
				data.element.shape = getShape(data.element.shape);
        if(typeof groups[data.group_id] === 'undefined')
          groups[data.group_id] = new Group(data.group_id);
				if(data.element.shape == 'plane')
					createEntity(data.id,data.group_id,data.element.position,data.element.shape,data.element.radius,data.element.width,data.element.height);
				else if(data.element.shape == 'model')
					createEntity(data.id,data.group_id,data.element.position,data.element.shape,data.element.radius,null,null,data.element.objfile,data.element.mtlfile);
				else
					createEntity(data.id,data.group_id,data.element.position,data.element.shape,data.element.radius);
        for (var key in data.element)
        {
            groups[data.group_id].entities[data.id].update(key, data.element[key]);
        }
        // console.log("Created element:" + data.id + " in group:" +data.group_id);
				break;
			case "\"deleteEverything\"":
				if(!groups[data.group_id] == 'undefined'){
					for (var i = 0; i < groups[data.group_id].entities.length; i ++){
						scene.remove(groups[data.group_id].entities[i].mesh);
					}
				}
				break;
			case "\"set_webpage\"":
				url = data.url;
        document.getElementById('webpage').data = url;
        console.log("Webpage set to: " + url);
				break;
			default:
				break;
		}
	}
}
else
{
	//Websocket not supported by browser
	console.log("Websockets not supported by this browser");
}


/*
*   parseMessage()
*       Parses incoming messages in the form of the Homenet3D syntax
*
*
*   NOTE:When adding new attributes to the server, you need to add them to the 'names' array 
*   following the format of the other array elements.
*/
function parseMessage(message){
	var data = {};
	data['element'] = {};
	var names = new Array("\"name\"","\"id\"","\"group_id\"","\"url\"","\"position\"","\"state_types\"","\"bounce_height\"",
                        "\"bounce_freq\"","\"rotate_speed\"","\"colour\"","\"text_colour\"","\"label\"",
                        "\"radius\"","\"scale\"","\"shape\"","\"objfile\"","\"mtlfile\"","\"rotation\"","\"mappings\"",
                        "\"hostname\"","\"system\"","\"model\"","\"uptime\"",
                        "\"mem_inuse\"","\"mem_cached\"","\"mem_buffers\"","\"mem_free\"","\"mem_total\"",
                        "\"lan_ip\"","\"lan_nm\"","\"lan_gw\"","\"wan_ip\"","\"wan_nm\"","\"wan_gw\"",
                        "\"act_conns\"","\"max_conns\"","\"rx_rate\"","\"tx_rate\"",
                        "\"radios\"","\"leases\"","\"nat\"","\"width\"","\"height\""
                        );
	var start, end, value, i, j;
	
	var split = message.split(",");
	for(j = 0; j < split.length; j++){
		for (i = 0; i < names.length; i++){
			if(split[j].search(names[i]) != -1){
				start = split[j].search(names[i]) + names[i].length + 1;
				value = split[j].slice(start);
				value = value.replace(/{/g, '');
				value = value.replace(/}/g, '');
				if(j == split.length - 1 )
					value = value.slice(0,value.length-1);
				if (i < 4)
					data[names[i].slice(1,names[i].length -1)] = value;
				data.element[names[i].slice(1,names[i].length -1)] = value;
			}
		}
	}
	return data;
}

/*
*   sendMessage()
*       Will send String 'message' on the WebSocket.
*
*   NOTE: Not currently in use (v0.1 - v0.2)
*/
function sendMessage(message){
	if(connection.send(message))
	console.log("Message sent: " + message);
}