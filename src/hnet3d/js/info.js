/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

function setInfo(state_type,names,values,isTable){
  var infobox = document.getElementById("infobox");
  if(names != null){
    if(!isTable){
      var info = document.createElement('p');
      info.className = "info";
      if(state_type !== "memory"){
        for(var i in names){
          var text = document.createTextNode(names[i] + ": " + values[i]);
          var text_wrapper = document.createElement('pre');
          text_wrapper.appendChild(text);
          info.appendChild(text_wrapper);
        }
      }
    }
    else{
      var info = document.createElement('table');
      var table = document.createElement('tbody');
      info.id = state_type;
      // console.log("Table State Type: " + state_type);
      for(var i = 0; i < values.length; i++){
        var row = document.createElement('tr');
        //console.log("Row: " + typeof values[i]);
        for(var j = 0; j < names.length; j++){
          // console.log("Column: " + j + " of " + values.length);
          if(i == 0){
            var cell = document.createElement('th');
            var celltext = document.createTextNode(names[j]); 
            cell.appendChild(celltext);
            // console.log("Column Heading:" + names[j]);
            row.appendChild(cell);
          }
          else if(values[i] != null){
            cell = document.createElement('td');
            // console.log("Cell contents [" + i + "]["+ j +"]: " + values[i][j]);
            cell_wrapper = document.createElement('pre');
            var celltext = document.createTextNode(values[i][j]); 
            cell_wrapper.appendChild(celltext)
            cell.appendChild(cell_wrapper);
            row.appendChild(cell);
          }
        }
        table.appendChild(row);
      }
      info.appendChild(table);
    }
    infobox.appendChild(info);    
  }
}


// Converts full subnet notation to slash notation
//      e.g. 255.255.255.0 -> 24
function getNetmaskSlash(netmask) {
    if ( netmask.length > 0 && !(typeof netmask == 'undefined')) {
        var bytes = "";
        var count = 0;
        var split = netmask.split(".");
        for (var i = 0; i < split.length; i++) {
            bytes += parseInt(split[i], 10).toString(2);


        }
        for (var i = 0; i < bytes.length; i++) {
            if (bytes.charAt(i) === '1') {
                count++;
            }
        }
        return count;
    }
    else return "";
}


// Formats time in secs to days:hours:mins:secs format. 
//      Used for lease expiries and system uptime.
function formatTime(time) {

    var days = Math.floor(time / (24 * 3600));
    var hours = Math.floor((time - (days * 24 * 3600)) / 3600);
    var minutes = Math.floor((time - (days * 24 * 3600) - (hours * 3600)) / 60);
    var seconds = Math.floor(time - (days * 24 * 3600) - (hours * 3600) - (minutes * 60));
    var formattime = "";
    if (days > 0) {
        formattime += days + "d " + hours + "h " + minutes + "m " + seconds + "s";
    }
    else if (hours > 0) {
        formattime += hours + "h " + minutes + "m " + seconds + "s";
    }
    else if (minutes > 0) {
        formattime += minutes + "m " + seconds + "s";
    }
    else {
        formattime += seconds + "s";
    }
    return formattime;
}