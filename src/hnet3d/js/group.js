/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

function Group(id){
  this.id = id;
  this.entities = [];
  this.memory = {};
  this.leases = [];
  this.radios = [];
  this.nat_table = [];
  this.wired_devs = new DeviceHandler(this.id,new THREE.Vector3(0,0,0),1,"Wired",5,55,30);
  this.wifi_devs = new DeviceHandler(this.id,new THREE.Vector3(0,0,0),2,"Wifi",5,55,30);
  this.auto_mem_configured = false;
  
  this.update = function(name,value){
    if(name == "name")
      return;
    // console.log("Name: " + name + " Value: " + value);
    if (!isNaN(value)) {
        if (value % 1 === 0) {
            value = parseInt(value);
        }
        else {
            value = parseFloat(value);
        }
    }
      switch (name) {
     
          // DHCP
        case 'leases':
          this.updateLease(value);
          break;
          // Wifi
        case 'radios':
          this.updateWifi(value);
          break;
        case 'nat':
          this.updateNAT(value);
          break;
        case 'rx_rate': case 'tx_rate':
          this[name] = value;
          if(typeof this.rx_rate !== 'undefined')
            this.datarate = this.rx_rate + 0;
          else if( typeof this.tx_rate !== 'undefined' )
            this.datarate = this.tx_rate + 0;
          else
            this.datarate = this.tx_rate + this.rx_rate;
          break;
        case 'mem_inuse': case 'mem_buffers': case 'mem_cached': case 'mem_free':
          this.memory[name] = value;
          break;
        default:
          this[name] = value;
          break;
      }
  }
  
  this.updateLease = function(value) {
      var time = new Date();
      var pow = 6;
      var cols = ["wifi","id","hostname","ip","expiry","num","mac"];
      if (value != "") {
        value = value.slice(1,value.length-1);
          var leases = value.split(";");
          for (var j = 0; j < leases.length - 1; j++) {
            var split = leases[j].split("~");
            // console.log("Lease: " + leases[j]);
            if(split[0].length > 0){
              var key = split[0];

              if(typeof this.leases[key] == 'undefined')
              this.leases[key] = {};
              this.leases[key].mac = key;
              if(split[1].length > 0)
                this.leases[key].num = split[1];

              if(split[2].length > 0)
                this.leases[key].expiry = split[2];

              if(split[3].length > 0)
                this.leases[key].ip = split[3];

              if(split[4].length > 0)
                this.leases[key].hostname = split[4];
 
              if(split[5].length > 0)
                this.leases[key].id = split[5];

              if(split[6].length > 0)
                this.leases[key].wifi = split[6] == "true" ? true : false;
              // console.log("id: " + this.id + " Wireless?  " +  this.leases[key].wifi);
            
              if(!this.leases[key].wifi){
                if(!this.wired_devs.exists(key))
                  this.wired_devs.add(key);
              }
            }
            // else{
//               if(this.wired_devs.exists(key))
//                 this.wired_devs.remove(key);
//             }
          }
      }
  }
  this.updateWifi = function(value) {
      var exists = false;
      var assocExists = false;
      var exist_no = 0;
      // console.log("Radios: " + value);
      value = value.slice(1,value.length -1);
      if (value != "") {
        if (value.indexOf('#') == 0){
          
        }
        else{
          var radios = value.split(";");
          for (var j = 0; j < radios.length - 1; j++) {
            var split = radios[j].split("~");
            var assocs = split[4].split("$");

            if (split[3] != '') {
              var key = split[3];
              this.radios[key] = {};
              this.radios[key].channel = parseInt(split[0]);
              this.radios[key].ssid = split[2];
              this.radios[key].interface = split[3];
              this.radios[key].assocs = {};
              for (var k = 0; k < assocs.length - 1; k++) {
                  this.radios[key].assocs[k] = assocs[k];
                  if(this.wired_devs.exists(assocs[k]))
                    this.wired_devs.remove(assocs[k]);
                  if(!this.wifi_devs.exists(assocs[k]))
                    this.wifi_devs.add(assocs[k],this.radios[key].ssid);
              }
            }
            this.checkStaleDevs();
          }
        }
      }
  }
  
  this.updateNAT = function(value) {
      // console.log("Nat: " + value);
      value = value.slice(1,value.length -1);
      if (value != "") {
        var nat_table = value.split(";");
        for (var j = 0; j < nat_table.length - 1; j++) {
          var split = nat_table[j].split("~");
          var index = parseInt(split[0]);
          this.nat_table[index] = {};
          this.nat_table[index].index = index;
          this.nat_table[index].bytes = parseInt(split[1]);
          this.nat_table[index].sport = parseInt(split[2]);
          this.nat_table[index].dport = parseInt(split[3]);
          this.nat_table[index].packets = parseInt(split[4]);
          this.nat_table[index].src = split[5];
          this.nat_table[index].dst = split[6];
          this.nat_table[index].layer3 = split[7];
          this.nat_table[index].layer4 = split[8];
        }
      }
  }

  // Used to check if a previously connected device is still connected.
  // If it isn't, it is removed.
  this.checkStaleDevs = function() {
    var exists = [];
    var index;
    
      for (var key in this.radios) {
        for (var i in this.radios[key].assocs) {
          exists.push(this.wifi_devs.existsIndex(this.radios[key].assocs[i]));
        }
      }
      for (var i = 0; i < this.wifi_devs.devs.length; i++) {
        var is_stale = true;
        for (var j = 0; j < exists.length; j++) {
          if (exists[j] == i) {
              is_stale = false;
            }
        }
        if (is_stale) {
          var mac = this.wifi_devs.devs[i].mac;
          this.wifi_devs.remove(mac);
        }
      }
  }
  

}






//
// case 'leases':
//   value = value.slice(1, value.length - 1);
//   this.updateLease(value);
// break;
// case 'radios':
//   value = value.slice(1, value.length - 1);
//   this.updateWifi(value);
// break;
// case 'url':
//   this.url = value;
//   console.log("URL: " + this.url);
//   var webpage = document.getElementById('webpage');
//   webpage.data = this.url;
// break;
//
//
//
//




//
