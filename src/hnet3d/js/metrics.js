/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/


function Mapping(metric, attrs, group_id, entity_id){

  this.metric = metric;
  this.attrs = [];
  this.attrs = attrs;
  
  this.group_id = group_id;
  this.entity_id = entity_id;
  
  this.update = function(value){
    // console.log("Update Metric" + this.metrics[0].name + ", Attr:" + this.attr.name);
    if(value >= 0 || typeof value == 'string'){
      if(this.metric.name == 'auto_mem'){
        groups[group_id].entities[entity_id].updateAutoMem();
      }  
      else{
        for( var i in this.attrs){
          
          // console.log("Metric: " + this.attrs[i].name + ", Entity Value before: " + groups[group_id].entities[entity_id][this.attrs[i].name]);
            switch(this.attrs[i].name){
            case 'colour': case 'text_colour':
              var temp = this.attrs[i].max.slice(1,this.attrs[i].max.length - 1);
              var max_colour = temp.split(",");
              max_colour = new THREE.Color("rgb(" + max_colour[0] + "," + max_colour[1] + ","+ max_colour[2] + ")");
              
              temp = this.attrs[i].min.slice(1,this.attrs[i].min.length - 1);
              var min_colour = temp.split(",");
              min_colour = new THREE.Color("rgb(" + min_colour[0] + "," + min_colour[1] + ","+ min_colour[2] + ")");
              
              if(value >= this.metric.max){
                groups[group_id].entities[entity_id].update( this.attrs[i].name, "[" + max_colour.r + "~"+ max_colour.g + "~"+ max_colour.b + "]");
              }
              else if (value <= this.metric.min){
                groups[group_id].entities[entity_id].update( this.attrs[i].name, "[" + min_colour.r + "~"+ min_colour.g + "~"+ min_colour.b + "]");
              }
              else{
                var colour = new THREE.Color();
                colour.r = min_colour.r + value*(max_colour.r - min_colour.r)/(this.metric.max - this.metric.min);
                colour.g = min_colour.r + value*(max_colour.g - min_colour.g)/(this.metric.max - this.metric.min);
                colour.b = min_colour.r + value*(max_colour.b - min_colour.b)/(this.metric.max - this.metric.min);
                
                groups[group_id].entities[entity_id].update( this.attrs[i].name, "[" + colour.r*255 + "~"+ colour.g*255 + "~"+ colour.b*255 + "]");
              }
              break;
            case 'shape':
              if(metric.max > shapes.length)
                metric.max = shapes.length;
              if(metric.min < 0)
                metric.min = 0;
              if(value >= this.metric.max){
                groups[group_id].entities[entity_id].update(this.attrs[i].name,this.attrs[i].max);
              }
              else if (value <= this.metric.min){
                groups[group_id].entities[entity_id].update(this.attrs[i].name,this.attrs[i].min);
              }
              else{
                var attr_value = this.attrs[i].min + value*(this.attrs[i].max - this.attrs[i].min)/(this.metric.max - this.metric.min);
                attr_value =  Math.round(attr_value/this.attrs[i].gran)*this.attrs[i].gran;
                // console.log("Attr value: " + attr_value);
                groups[group_id].entities[entity_id].update(this.attrs[i].name,shapes[parseInt(attr_value)]);
              }
              break;
            default:
              if(value >= this.metric.max){
                groups[group_id].entities[entity_id].update(this.attrs[i].name,this.attrs[i].max);
              }
              else if (value <= this.metric.min){
                groups[group_id].entities[entity_id].update(this.attrs[i].name,this.attrs[i].min);
              }
              else{
                var attr_value = this.attrs[i].min + value*(this.attrs[i].max - this.attrs[i].min)/(this.metric.max - this.metric.min);
                attr_value =  Math.round(attr_value/this.attrs[i].gran)*this.attrs[i].gran;
                // console.log("Attr value: " + attr_value);
                groups[group_id].entities[entity_id].update(this.attrs[i].name,attr_value);
              }
            } 
        	}
        }
      }
    } 
  
  this.print = function(){
    console.log("Mapping: \n\tmetric:" +
                "\n\t\t" + this.metric.name + 
                "\n\t\t" + this.metric.max + 
                "\n\t\t" + this.metric.min + 
                "\n\tattr:" + 
                "\n\t\t" + this.attrs[0].name + 
                "\n\t\t" + this.attrs[0].max + 
                "\n\t\t" + this.attrs[0].min +
                "\n\t\t" + this.attrs[0].gran 
                );
  }
}

function parseMappings(mappings_str, group_id, entity_id){
  var mappings = [];
  var mapping = mappings_str.split("metric:[");

  for( var i in mapping){
    if(mapping[i].length > 0){
      
      var metric_end = mapping[i].search(']');
      var metric_str = mapping[i].slice(0, metric_end);
      var attrs_str = mapping[i].slice(metric_end+1);
      metric = parseMetric(metric_str);
      attrs = parseAttrs(attrs_str);
      mappings[i] = new Mapping(metric, attrs, group_id, entity_id);
    }
  }
  return mappings;
}

function parseMetric(metric){
  var split = metric.split('~');
  var metric = {};
  metric.name = split[0];
  metric.max = parseFloat(split[1]);
  metric.min = parseFloat(split[2]);
  return metric;
}

function parseAttrs(attrs_str){
  attrs_str = attrs_str.slice(6);
  var split = attrs_str.split(';');
  // console.log("Metrics: " +attrs_str);
  var attrs = [];
  for (var i in split){
    if(split[i].length > 0){
      var sub_split = split[i].split('~');
      attrs[i] = {};
      attrs[i].name = sub_split[0].slice(1);
      if(attrs[i].name == "text_colour" || attrs[i].name == "colour"){
        attrs[i].max = sub_split[1] + "," + sub_split[2] + "," + sub_split[3];
        attrs[i].min = sub_split[4] + "," + sub_split[5] + "," + sub_split[6];
        attrs[i].gran = parseFloat(sub_split[7]);
      }
      else{
        attrs[i].max = parseFloat(sub_split[1]);
        attrs[i].min = parseFloat(sub_split[2]);
        attrs[i].gran = parseFloat(sub_split[3]);
      }
      
      // console.log("Metric: " + attrs[i].name + " max: " + attrs[i].max + " min: " + attrs[i].min+ " gran: " + attrs[i].gran)
    }
  }
  return attrs;
}






