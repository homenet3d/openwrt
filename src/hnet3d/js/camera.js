/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
*   This document sets up the virtual world skeleton (Camera,Scene,Controls) 
*/

// Here are the global variables used to render to world
var camera, scene, renderer;
var geometry, material, mesh, domEvents;
var controls, time = Date.now();
var webpage, webpage2D;
var objects = [];
var webpageUp = false;
var infohide = 'autohide';
var ray;





/* 
* These two functions setup and render the world and are called here.
*
* 	init()      -> Initiliases the camera, scene and renderer of the world.
* 	animate()   -> Contains a recursive call to itself that means that 
*              the world is rendered in a continuous loop. 
*/
init();
animate();


/* 
*   init() initialises the virtual world.
*       camera      -> Point of view of the world.
*       scene       -> The scene that objects are added to.
*       light       -> Light sources that are added to the scene.
*       renderer    -> Three.js WebGL renderer init and appended to the webpage
*       controls    -> The trackball controls that allow mouse manipulation of 
*                      the world. linked to camera and the webpage renderer 
*                      element.
*       domEvents   -> Javascript DOM events for Three.js to enable mouse events
*                      on entities.
*       stats       -> Used to display Frames Per Second stats for rendering.
*/
function init() {
    // Checks if Web browser supports WebGL
    if(!Detector.webgl)
        noWebGL();
    else{
    	camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 10000);
      camera.position.z = 700;
	
	
    	scene = new THREE.Scene();
    	scene.matrixAutoUpdate = true;
    	scene.add(camera);

        // Light Sources
        var light = new THREE.DirectionalLight(0xffffff, 1);
        light.position.set(1, 1, 1);
        scene.add(light);

        var light = new THREE.DirectionalLight(0xffffff, 0.75);
        light.position.set(-1, -0.5, -1);
        scene.add(light);
    
        var light = new THREE.SpotLight( 0xffffff, 3.0);
        light.position.set( 0, 300, -300 );
        light.castShadow = true;
        scene.add( light );
        
        // Disc used as a reference point for objects (sits flat underneath them)
        

    	ray = new THREE.Raycaster();
    	ray.ray.direction.set(0, -1, 0);

    	// WebGL Renderer
    	renderer = new THREE.WebGLRenderer();
    	renderer.setClearColor(0x1f1f1f,0);
    	renderer.setSize(window.innerWidth, window.innerHeight);
    	document.getElementById("container").appendChild(renderer.domElement);
    	renderer.domElement.id = "renderer";
	
    	// Trackball Controls
    	controls = new THREE.TrackballControls( camera, renderer.domElement );
    	controls.rotateSpeed = 0.25;
    	controls.minDistance = 0;
    	controls.maxDistance = 5000;
      controls.addEventListener( 'change', render );
	
    	// THREEX Dom events
      domEvents = new THREEx.DomEvents(camera, renderer.domElement);
	
    	// FPS stats
    	stats = new Stats();
    	stats.domElement.style.position = 'absolute';
    	stats.domElement.style.bottom = '0px';
    	container.appendChild( stats.domElement );
	    
    	window.addEventListener('resize', onWindowResize, false); 
      
	}
}

/*
*   animate()
*       This function is looped by requestAnimateFrame() and means that
*       the rendered world, controls events and FPS stats are updated.
*/
function animate() {
	requestAnimationFrame(animate);
	controls.update();
  // camera.rotation.x = camera.rotation.z = 0;
  // camera.position.setY(0);
	render();
	stats.update();
}

function render(){
    renderer.render( scene, camera );
}

/*
*   onWindowResize()
*       Callback function called on a resize event. If browser window size 
*       is changed, the rendered world scales along with it.
*/
function onWindowResize() {
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize(window.innerWidth, window.innerHeight);
	$('#webpage').css('width',window.innerWidth -60);
	$('#webpage').css('height',window.innerHeight -60);
}

/*
*   noWebGL()
*       If the browser used does not support WebGL. This function is run. It 
*       will display a message to tell the user that WebGL is not supported.
*/
function noWebGL() {
    var alert = $("<div id=\"noWebGL\"><p>WebGL not supported by this browser. <br />Have a look <a href=\"http://en.wikipedia.org/wiki/WebGL#Desktop_browsers\">here</a></p></div> ");
    $("#container").append(alert);
    console.log("WebGL not supported in this browser.");
}

/*
*   Contains jQuery event driven functionality:
*       autohide    -> The autohide button cycles through 'autohide', 'always on' 
*                      and 'off' when clicked
*       webinterface-> The webpage button toggles a 2D router web GUI superimposed
*                      over the 3D world and toggles button text between "Web 
*                      Interface" and "Back to 3D".
*       reset       -> Resets camera view to default when 'r' is pressed
*/
jQuery(document).ready(function(){
    
	$('#webpage').css('width',window.innerWidth -60);
	$('#webpage').css('height',window.innerHeight -60);
	
	/*
	*   InfoBox hide/autohide/show logic
	*/
	$("#infoAutohide").click(function(){
		if($("#infoAutohide").text() == "autohide"){
			$("#infoAutohide").text("always on");
			infohide = $("#infoAutohide").text();
			$("#infobox").show();
		}
		else if($("#infoAutohide").text() == "always on"){
			$("#infoAutohide").text("off");
			infohide = $("#infoAutohide").text();
			$("#infobox").hide();
		}
		else if($("#infoAutohide").text() == "off"){
			$("#infobox").hide();
			$("#infoAutohide").text("autohide");
			infohide = $("#infoAutohide").text();
		}
	});
	
	/*
	*  Web Interface hide/show logic
	*/
	$("#webButton").click(function(){
	    if(webpageUp){
		    webpageUp = false;
        // console.log("Hiding webpage");
		    $("#webpage2D").css("visibility","hidden");
		    $("#webpage2D").css("z-index","-1");
		    $("#webButton").text("Web Interface");
	    }
		else{
		    webpageUp = true;
        // console.log("Showing webpage");
    		$("#webpage2D").css("z-index","2");
    		$("#webpage2D").css("visibility","visible");
    		$("#webButton").text("Back to 3D");
	    }
	});
    
	/*
	*  Resets camera view to default when 'R' is pressed
	*/
	$(document).keydown(function(e){
		if(e.which == 82){
			console.log("Reset Camera");
      controls.reset();      
		}
	});
})
