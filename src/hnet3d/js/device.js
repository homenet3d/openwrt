/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
*   This document defines Device Spawners and Device objects. 
*/


/*
*   DeviceHandler()
*       This is an object used by the Wired and Wireless entities to automate device spawning.
*/
function DeviceHandler(group_id,center,id,name,devRad,r,rStep,angStart,angStep,lineLimit,numLevels,firstZ,spacing){
	
	/* Set the initial values to configure plotting algorithm.
	*
	*  The idea is to plot lines through the Up/Down Axis at varying
	*  depths curving through a sphere centered at the 'center'. 
	*  
	*/
  // console.log("Created new device spawner");
  this.canSpawn = false;
  this.name = name;
  this.id = id;
  this.group_id = group_id;
  
	this.c = center;
	this.devs = [];
	this.devCount = 0;
	this.devRad = (typeof devRad !== 'undefined' ? devRad : 5);
	/*  RADIUS
	*   this.r          ->  the distance of the first layer of plotting
	*                       from the centre
	*   this.rStep      ->  the extra distance for the following layer of
	*                       plotting
	*/
	this.r = (typeof r !== 'undefined' ? r : 20);

	this.rStep = typeof rStep !== 'undefined' ? rStep : 10;
	
	/*  ANGLE
	*   this.angStart   ->  the starting angle of the plotting (in radians)
	*   this.angStep	->  the angular spacing between each plot
	*/
	this.angStart = typeof angStart !== 'undefined' ? angStart : Math.PI/4;
	this.ang = this.angStart;
	this.angStep = typeof angStep !== 'undefined' ? angStep : Math.PI/8;
	
	/*   LINES and DEPTH 
	*    Each line curving through the UP/DOWN axis has a limit of devices 
	*    set by 'lineLimit'
	*    this.lineLimit   -> Amount of devices allow in each line
	*    this.numLevels   -> Amount of lines in DEPTH plane
	*    this.firstLevelZ -> The start depth of plotting
	*    this.spacing     -> The spacing (depth-wise) between levels.
    *    this.outerz      -> 
	*/  
	this.lineLimit = typeof lineLimit !== 'undefined' ? lineLimit : 5;
	this.numLevels = typeof numLevels !== 'undefined' ? numLevels : 3;
	this.firstLevelZ = typeof firstZ !== 'undefined' ? firstZ : 20;
	this.spacing = typeof spacing !== 'undefined' ? spacing : 20;
	this.outerz = this.firstLevelZ;
	
	// Used to offset every second level by half an angle step
	this.isEven = 1;
	this.points = [];
	this.freepoints = [];
	
    /*
    *   getNextPoint()
    *       Contains an algorithm that will take the initial attributes 
    *       detailed above and plots a point that is next in the line.
    */
	this.getNextPoint = function(i){
		var newpoint = new THREE.Vector3((parseFloat(this.r)*Math.sin(-this.ang)+parseFloat(this.c.x)), 
                                          parseFloat(this.r)*Math.cos(-this.ang)+parseFloat(this.c.y), 
                                          parseFloat(this.c.z)+parseFloat(this.outerz));
		this.ang += this.angStep;
    if( i > 0){
      if(i%this.lineLimit == 0){
        this.outerz -= this.spacing;
        this.ang = this.angStart + (this.angStep/2)*this.isEven;
        if(this.isEven == 1)
          this.isEven = 0;
        else
          this.isEven = 1;
      }
      if(i%(this.lineLimit*this.numLevels) == 0){
        this.r += this.rStep;
        this.outerz = this.firstLevelZ;
      }
    }
		this.count++;
		return newpoint;
	}
	
    /*
    *   Plots 100 points that can be used for spawning points of devices
    */
	for(var i = 0; i < 100; i++){
		var point = this.getNextPoint(i);
		this.points[i] = point;
		this.freepoints[i] = true;
	}

	/*
    *   getFreePoint()
    *       This function is used when a device is created to find a free 
    *       place to plot it.
    */
	this.getFreePoint = function(){
		var count = 0;
		while(!this.freepoints[count]){
			count++;
		}
			
		var point = this.points[count];
		this.freepoints[count] = false;
		count = 0;
		return point;
	}
    
	/*
    *   freePoint()
    *       This function is used when a device is deleted to free the point 
    *       that it occupied.
    */
	this.freePoint = function(point){
		for(var i = 0; i < this.points.length; i++){
			if(this.points[i].equals(point)){
				this.freepoints[i] = true;
				break;
			}
		}
	}
	
	/*
    *   add()
    *       Adds a device to the list handled by the DevHandler
    */
	this.add = function(mac,ssid,radio_id){
		var point = this.getFreePoint();
		var dev = new Device(point,this.c,mac,this.devRad);
		dev.radio_id = (typeof radio_id == 'undefined' ? null : radio_id);
		dev.ssid = (typeof ssid == 'undefined' ? null : ssid);
		dev.interface = (typeof interface == 'undefined' ? null : interface);
    entity_id = parseInt(100*this.id+this.devCount);
    dev.id = entity_id;
		this.devs.push(dev);
	  if(this.canSpawn)
      this.spawn(dev);
    
		groups[group_id].entities[entity_id] = new Entity(entity_id, dev.mesh,"sphere");
    groups[group_id].entities[entity_id].line = dev.line;
		groups[group_id].entities[entity_id].mac = dev.mac;
    groups[group_id].entities[entity_id].group_id = this.group_id;
    groups[group_id].entities[entity_id].label = this.name + " Device " + this.devCount;
    groups[group_id].entities[entity_id].state_types.push("device");
		groups[group_id].entities[entity_id].ssid = ssid;
		groups[group_id].entities[entity_id].initDomEvents();
		
		this.devCount++;
    // console.log("Added Device:" + mac);
	}
  
  this.spawn = function(dev){
		scene.add(dev.line);
		scene.add(dev.mesh);
  }
  this.unSpawn = function(dev){
		scene.add(dev.line);
		scene.add(dev.mesh);
  }
    
	/*
    *   remove()
    *       Removes a device from the list handled by the DevHandler
    */
	this.remove = function(mac){
		for (i in this.devs){
			if(this.devs[i].mac == mac){
				this.freePoint(this.devs[i].mesh.position);
        groups[this.group_id].entities[this.devs[i].id].removeEntity();
        scene.remove(this.devs[i].line);
				this.devs.splice(i,1);
				this.devCount--;
				break;
			}
		}
        // console.log("Removed Device:" + mac);
	}
    
	/*
    *   exists()
    *       Boolean return. Checks if a device exists in the list.
    */
	this.exists = function(mac){
		var exists = false;
		for (var i = 0; i < this.devs.length; i++){
			if(this.devs[i].mac == mac){
				exists = true;
				break;
			}
		}
		return exists;
	}
	/*
    *   exists()
    *       Int returned. If a device exists it returns it's index in
    *       in the list.
    */
	this.existsIndex = function(mac){
		var exists = null;
		for (var i = 0; i < this.devs.length; i++){
			if(this.devs[i].mac == mac){
				exists = i;
				break;
			}
		}
		return exists;
	}
  
  
  this.setCenter = function(c){

    for(var i in this.devs){
      this.devs[i].mesh.position.sub(this.c);
    }
    for (i in this.points){
      this.points[i].sub(this.c);
    }
    this.c = c;
    for( i in this.devs){
      this.devs[i].mesh.position.add(this.c);
      this.devs[i].redrawLine(this.c);
    }
    for (i in this.points){
      this.points[i].add(this.c);
    }

    this.loadDevs();
    this.canSpawn = true;
    // console.log("Device Spawner recentred");
  }
  
  this.loadDevs = function(){
    for (i in this.devs){
      this.spawn(this.devs[i]);
    }
  }
  
}

/*
*   Device()
*       This is an object used a DeviceHandler to create/delete a device.
*       This object adds itself to the scene so that it will be rendered.
*/
function Device(p,c,mac,rad){
	this.mac = mac;
	geometry = new THREE.SphereGeometry(rad);
	material = new THREE.MeshPhongMaterial({
		color: 0x888888
	}) ;
	this.mesh = new THREE.Mesh(geometry,material);
	this.mesh.position = new THREE.Vector3(parseFloat(p.x),parseFloat(p.y),parseFloat(p.z));
  // console.log("X:" + p.x + ",Y:" + p.y + ",Z:" + p.z);
	geometry = new THREE.Geometry();
	material = new THREE.LineBasicMaterial({
		color: 0xffffff
	}) ;
	geometry.vertices.push(c);
	geometry.vertices.push(this.mesh.position);
	this.line = new THREE.Line(geometry,material);
  // console.log("X:" + p.x + ",Y:" + p.y + ",Z:" + p.z);


  this.redrawLine = function(c){
    scene.remove(this.line);
  	geometry = new THREE.Geometry();
  	material = new THREE.LineBasicMaterial({
  		color: 0xffffff
  	}) ;
  	geometry.vertices.push(c);
  	geometry.vertices.push(this.mesh.position);
  	this.line = new THREE.Line(geometry,material);
    scene.add(this.line);
  }
}
