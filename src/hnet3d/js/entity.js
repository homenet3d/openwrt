/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/* 
This javascript file contains the functions that create and modify the
Homenet3D entities. 
*/



// To add new shape types, you need to add it to the list here as well as 
// modifying the switch statements in createEntity and updateType (using 
// the appropriate three.js geometry)
// You can also create your own shape by mapping vertices and faces, see 
// starGeometry function.
// These shapes map to numbers so ORDER IS IMPORTANT in the shapes array.
//  cube         -> 1
//  sphere       -> 2
//  tetrahedron  -> 3
//  star         -> 4
//  cylinder     -> 5
//  plane        -> 6
//  custom object -> 7
var shapes = ['', 'cube', 'sphere', 'tetrahedron', 'star', 'cylinder', 'plane', 'model'];
var textoffset = {};
var height = 50;
var lastHit = -1;
var lastHit_group = -1;
var count = 0;


/*
* createEntity(id,x,y,z,label,shape,width,height,objfile,mtlfile)
* description:
*   A shell function that creates the object's mesh and then creates an Entity object.
* passed:
*   index: The index of the entity array in the config file
*   setting: A pointer to the config file setting object
*   attr: A string of the attribute that is to be retrieved
* 
* returns:
*   The integer value of the config setting requested.
*/


function createEntity(id, group_id, pos, shape, radius, width, height, objfile, mtlfile) {
  var colour = new THREE.Color(0x93EDC0);
  var position = toArray(pos,'vector');
  // Different geometry dependent on shape
  var model = false;
  
  
  if (shape == "model") {
    
    // Load OBJ MTL model
    var objLoader = new THREE.OBJMTLLoader();
    var obj = new THREE.Object3D();
    objLoader.load(objfile, mtlfile, (function(object) {

        var obj = object.clone();
        obj.position = groups[group_id].entities[id].mesh.position;
        obj.rotation = groups[group_id].entities[id].mesh.rotation;
        obj.scale = groups[group_id].entities[id].mesh.scale;
        groups[group_id].entities[id].mesh = obj;
        objects.push(groups[group_id].entities[id].mesh);
        groups[group_id].entities[id].initDomEvents();
        groups[group_id].entities[id].clickmesh.position = groups[group_id].entities[id].mesh.position;
        scene.add(obj);

        // console.log("object is loaded")
    }));
    // console.log("OBJ shape:" + obj.shape);
    
    groups[group_id].entities[id] = new Entity(id, obj, shape, radius);
    // console.log(typeof object);    
  }
  else {
    var mesh = getShapeMesh(shape,radius,colour,height,width);
    // console.log("Shape:" + shape);
    mesh.position = position;
    groups[group_id].entities[id] = new Entity(id, mesh, shape, radius);
    groups[group_id].entities[id].initDomEvents();
    objects.push(groups[group_id].entities[id].mesh);
    scene.add(groups[group_id].entities[id].mesh);
  }
}

function getShapeMesh(shape,radius,colour,height,width){
  var geometry, material;
  material = new THREE.MeshPhongMaterial();
  switch (shape) {
    case 'cube':
    geometry = new THREE.CubeGeometry(2 * radius, 2 * radius, 2 * radius);
    break;
    case 'sphere':
    geometry = new THREE.SphereGeometry(radius, 100, 100);
    break;
    case 'tetrahedron':
    geometry = new THREE.TetrahedronGeometry(radius);
    break;
    case 'star':
    geometry = starGeometry(radius);
    break;
    case 'cylinder':
    geometry = new THREE.CylinderGeometry(radius, radius, height, 100, 100);
    break;
    case 'plane':
    geometry = new THREE.PlaneGeometry(width, height, 100, 100);
    material = new THREE.MeshBasicMaterial();
    material.opacity = 0.0;
    material.blending = THREE.NoBlending;
    material.visible = false;
    break;
  }
  material.color = colour;

  return new THREE.Mesh(geometry,material);
}


// Entity class
function Entity(id, mesh, shape, radius)
{
    // Common Attributes
    this.id = id;
    this.shape = shape;
    this.radius = radius;
    this.mesh = mesh;
    this.centre = this.mesh.position.y;
    this.bounce_dir = 1;
    this.text_size = 8;
    this.text_colour = new THREE.Color('rgb(0,0,0)');
    // Initialise variables 
    this.info = '';
    this.state_types = [];
    
    if(this.shape == "model"){
      this.clickmesh = new THREE.Mesh(new THREE.SphereGeometry(this.radius*4,100,100),new THREE.MeshBasicMaterial({color: 'white', transparent: true, opacity: 0}));
      this.clickmesh.position = this.mesh.position;
      this.clickmesh.scale.copy(this.mesh.scale);
      scene.add(this.clickmesh);
    }
    
    // Entity specific attributes
    //
    // Check if the entity has multiple state_types
   
  this.rotate = function(){
    if(this.rotate_speed >0){
      var neg = 1;
      if(this.mesh.rotation.x < 2*Math.PI)
        neg = -1;
      else
        neg = 1;
      var rotate = (this.rotate_speed * neg * Math.PI) / 100;
      
      this.mesh.rotation.x +=rotate;
      this.mesh.rotation.y +=rotate;
    }
  }
  
  this.bounce = function() {
    if(this.bounce_freq > 0){
      var top = this.centre +this.bounce_height/2;
      var bottom = this.centre -this.bounce_height/2;
      if(this.mesh.position.y > top){
        this.bounce_dir = -1;
      }
      else if(this.mesh.position.y < bottom){
        this.bounce_dir = 1;
      }
      this.mesh.position.y += this.bounce_dir*this.bounce_height*this.bounce_freq/20;
      
      if(typeof this.textMesh !== 'undefined'){
        this.textMesh.position.y += this.bounce_dir*this.bounce_height*this.bounce_freq/20;
      }
      for(var i in this.state_types){
        if( this.state_types[i] == "wired"){
          groups[this.group_id].wired_devs.setCenter(this.mesh.position);
        }
        if( this.state_types[i] == "wifi"){
          groups[this.group_id].wifi_devs.setCenter(this.mesh.position);
        }
      }
    }
  }

  this.step = function() {
    this.rotate();
    this.bounce();
    if(count % 20 == 0){
      for(var i in this.mappings){
        if(this.mappings[i].metric.name === 'auto_mem'){
          // console.log("this.step -> auto mem mapping");
          this.updateAutoMem();
        }
        else{ 
          this.mappings[i].update(groups[this.group_id][this.mappings[i].metric.name]);
        }
      }
    }
  }

    // Updates the Entities properties (position, colour, scale, etc)
    this.update = function(name, value) {
        // Checks to see if the value is a number and parses it accordingly.
        if(name == "name")
          return;
        // console.log("Name: " + name + " Value: " + value);
        if (!isNaN(value)) {
            if (value % 1 === 0) {
                value = parseInt(value);
            }
            else {
                value = parseFloat(value);
            }
        }

        if (this[name] != value) {
            switch (name)
            {
            case 'state_types':
              this.state_types = toArray(value,"string");
              for (i in this.state_types){
                if(this.state_types[i] == "wifi"){
                  groups[this.group_id].wifi_devs.setCenter(this.mesh.position);
                }
                else if (this.state_types[i] == "wired"){
                  groups[this.group_id].wired_devs.setCenter(this.mesh.position);
                }
              }
            break;
            case 'position':
              this.mesh.position = toArray(value,"vector");
            break;
            case 'height':
              if(value >= 0){
                this.height = value;
                this.updateShape(this.shape);
              }
              break;
            case 'radius':
              if(this.shape == 'model'){
                var scale = value/this.radius*this.mesh.scale;
                this.mesh.scale = new THREE.Vector3(scale,scale,scale);
                this.clickmesh.scale = new THREE.Vector3(scale,scale,scale);
                this.radius = value;
              }
              else{
                this.radius = value;
                this.updateShape(this.shape);
              }
            break;
            case 'shape':
              if (isShape(value)) {
                  this.updateShape(value);
                  this.shape = value;
              }
            break;
            case 'scale':
            if (value >= 0.0) {
              if (this.state_types.indexOf('memory') < 0){
                if (this.shape != "cylinder")
                  this.mesh.scale = new THREE.Vector3(value,value,value);
                else if (this.shape == "model"){
                  this.mesh.scale = new THREE.Vector3(value,value,value);
                  this.clickmesh.scale.set(new THREE.Vector3(value,value,value));
                }
                else{
                  this.mesh.scale = new THREE.Vector3(this.mesh.scale.x,value,this.mesh.scale.z);
                }
              }
            }
              
            break;
            case 'colour':
              if (isNaN(value) && this.mesh instanceof THREE.Mesh ) {
                this.mesh.material.color = toArray(value,"colour");
              }
            break;
            case 'label':
              this.label = value;
              if (this.label !== "(null)"){
                this.initDomEvents();
                this.textMesh = textLabel(this.label,this.text_size,this.radius,this.mesh.position, this.text_colour);
                this.text_center = this.textMesh.position.y;
              }
            break;
            case 'text_colour':
            if (isNaN(value)) {
              this.text_colour = toArray(value,"colour");
              if(typeof this.textMesh !== 'undefined')
                this.textMesh.material.color = toArray(value,"colour");
            }
            break;
            case 'rotation':
            if (isNaN(value)) {
                // console.log(this.id);
                this.mesh.rotation = toArray(value,"euler");
            }
            break;
            case 'mappings':
            if(value.length > 0){
              this.mappings = parseMappings(value, this.group_id, this.id);
              for(var i in this.mappings){
                if(this.mappings[i].metric.name == 'auto_mem' && this.state_types.indexOf("memory") > -1 && !groups[this.group_id].auto_mem_configured){
                  this.autoMem('y');
                  groups[this.group_id].auto_mem_configured = true;
                }
                // this.mappings[i].print();
              }
            }
            break;
            default:
                // console.log("Name: " + name + " Value: " +value );
              this[name] = value;
            break;
            }
        }


    }

    // Changing the entity's shape requires to object to be removed and respawned
    this.updateShape = function(value) {

      if(value != "model"){
        var newmesh = getShapeMesh(value,this.radius,this.mesh.material.color,this.height, this.width);
        newmesh.position = this.mesh.position;
        newmesh.rotation = this.mesh.rotation;
        scene.remove(this.mesh);
        this.mesh = newmesh;
        scene.add(this.mesh);
      }
      else{
        var objLoader = new THREE.OBJMTLLoader();
        var obj = new THREE.Object3D();
        var scope = this;
        scene.remove(this.mesh);
        objLoader.load(this.objfile, this.mtlfile, (function(object) {
            // console.log("Object: " + typeof object);
            var obj = object.clone();
            obj.position = scope.mesh.position;
            obj.rotation = scope.mesh.rotation;
            obj.scale = scope.mesh.scale;
            scope.mesh = obj;
            
            scope.initDomEvents();
            scene.add(scope.mesh);
            scope.clickmesh.position = scope.mesh.position;
            // console.log("object is loaded")
        }));
      }
    }
    
    
    this.updateAutoMem = function(axis){
      axis = (typeof axis == 'undefined' ? 'y' :axis );
      var group = groups[this.group_id];
      var memory = group.memory;
      
      var old_scale = this.mem_objects.getObjectByName('mem_inuse').scale[axis];
      for (var i in memory){
        if (memory[i] > 0.0)
        this.mem_objects.getObjectByName(i).scale[axis] = memory[i]/group.mem_total;
      }
      var new_scale = this.mem_objects.getObjectByName('mem_inuse').scale[axis];
      for (var i in memory){
        // console.log("Current memory type:  " + i)
        currentmem = this.mem_objects.getObjectByName(i);
        if(i == 'mem_inuse'){
          currentmem.position[axis] += parseFloat(this.height * (new_scale - old_scale) / 2); 
        }
        else{
          prevmem = this.mem_objects.getObjectByName(prev_name);
          currentmem.position[axis] = prevmem.position[axis] + this.height*(prevmem.scale[axis] +currentmem.scale[axis])/2;
        }
        prev_name = i;
      }
      // this.mem_objects.getObjectByName(memory[1]).position[axis] = parseFloat(elements[prevmem].mesh.position[axis]) + parseFloat(height * (elements[prevmem].scale + this.mesh.scale[axis]) / 2);
 //      this.mem_objects.getObjectByName(memory[2]).position[axis] = parseFloat(elements[prevmem].mesh.position[axis]) + parseFloat(height * (elements[prevmem].scale + this.mesh.scale[axis]) / 2);
 //      this.mem_objects.getObjectByName(memory[3]).position[axis] = parseFloat(elements[prevmem].mesh.position[axis]) + parseFloat(height * (elements[prevmem].scale + this.mesh.scale[axis]) / 2);
    }

    // Writes Entity specific info to an overlay box 
    this.writeInfo = function() {
      var infobox = document.getElementById('infobox');
      infobox.innerHTML = '';
      var heading = document.createElement('h2');
      heading.id = 'heading';
      heading.innerHTML = this.label;
      infobox.appendChild(heading);
      var group = groups[this.group_id];
      var isTable = false;
      
      
      for (var i in this.state_types){
        switch(this.state_types[i]){
        case 'system':
          var names = ["Hostname","System","Model","Uptime"];
          var values = [group.hostname,group.system,group.model,group.uptime];
          break;
        case 'network':
          var names = ["LAN IP","LAN GW","WAN IP","WAN GW","Connections", "Datarate (Rx|Tx)"];
          if(isNaN(group.lan_gw)){
            group.lan_gw = "";
          }
          if(isNaN(group.wan_gw)){
            group.wan_gw = "";
          }
          var values = [group.lan_ip + "/" + group.lan_nm, 
                        group.lan_gw, 
                        group.wan_ip + "/" + group.wan_nm, 
                        group.wan_gw, 
                        group.act_conns + "/" + group.max_conns,
                        group.rx_rate + "|" + group.tx_rate + " B/s"]
          break;
        case 'memory':
          var names = ["In Use","Cached","Buffers","Free"];
          values = [];
          var count = 0;
          if(group.auto_mem_configured){
            var mem_legend = document.createElement('div');
            mem_legend.className = "mem_legend";
          }
          for (var i in group.memory){
            values[count] = Math.round(group.memory[i]/1024 * 100) / 100 + " MB";
            count += 1;
            if(group.auto_mem_configured){
              var mem_key = document.createElement('div');
              mem_key.style.backgroundColor = '#' + this.mem_objects.getObjectByName(i).material.color.getHexString();
              mem_legend.appendChild(mem_key);
            }
            infobox.appendChild(mem_legend);
          }
          break;
        case 'wifi':
          var names = ["SSID","Channel","Assoc Stns"];
          var values = [];
          values[0] = null;
          var count = 1;
          isTable = true;
          for (var key in group.radios) {
            values[count] = null;
            if(typeof group.radios[key] !== 'undefined'){
              var assocs = "";
              values[count] = [];
              values[count][0] = group.radios[key].ssid;
              values[count][1]  = group.radios[key].channel;
              for (var j = 0; j < group.wifi_devs.devs.length; j++) {
                var hostname = ( typeof group.leases[group.wifi_devs.devs[j].mac] == 'undefined' ? group.wifi_devs.devs[j].mac: group.leases[group.wifi_devs.devs[j].mac].hostname);
                assocs += hostname + "\n";
              }
              values[count][2] = assocs;
              count += 1;
            }
          }
          break;
        case 'wired':
          var names = ["Hostname","IP","MAC","Expiry"];
          var values = [];
          values[0] = null;
          var count = 1;
          isTable = true;
          for (var key in group.leases) {
            values[count] = null;
            if(typeof group.leases[key] !== 'undefined' && !group.leases[key].wifi){
              values[count] = [];
              values[count][0] = group.leases[key].hostname;
              values[count][1] = group.leases[key].ip;
              values[count][2] = group.leases[key].mac;
              values[count][3] = group.leases[key].expiry;
              count++;
            }
          }
          break;
        case 'dhcp':
          var names = ["Hostname","IP","MAC","Expiry",""];
          var values = [];
          values[0] = null;
          var count = 1;
          isTable = true;
          for (var key in group.leases) {
            values[count] = null;
            if(typeof group.leases[key] !== 'undefined'){
              values[count] = [];
              values[count][0] = group.leases[key].hostname;
              values[count][1] = group.leases[key].ip;
              values[count][2] = group.leases[key].mac;
              values[count][3] = group.leases[key].expiry;
              values[count][4] = (group.leases[key].wifi ? "Wireless" : "Wired");
              count += 1;
            }
          }
          break;
        case 'nat':
          var names = ["Source","Destination","Bytes/Packets","Layer3/Layer4"];
          var values = [];
          values[0] = null;
          var count = 1;
          isTable = true;
          for (var key in group.nat_table) {
            // console.log("Nat Entry " + key + " count " + count);
            values[count] = [];
            values[count][0] = group.nat_table[key].src + ":" + group.nat_table[key].sport;
            values[count][1] = group.nat_table[key].dst + ":" + group.nat_table[key].dport;
            values[count][2] = group.nat_table[key].bytes + "/" + group.nat_table[key].packets;
            values[count][3] = group.nat_table[key].layer3 + "/" + group.nat_table[key].layer4;
            count += 1;
          }
          break;
        case 'device':
          var names = ["Hostname","IP","MAC"];
          var hostname = ( typeof group.leases[this.mac] == 'undefined' ? this.mac: group.leases[this.mac].hostname)
          var values = [group.leases[this.mac].hostname,group.leases[this.mac].ip,this.mac];
          if(this.ssid != null){
            names[3] = "SSID";
            values[3] = this.ssid;
          }
          break;
        default:
          names = null;
          break;
        }
        if(isTable)
          setInfo(this.state_types[i],names,values,isTable);
        else
          setInfo(this.state_types[i],names,values);
      }
      lastHit_group = this.group_id;
      lastHit = this.id;
    }
    
    this.autoMem = function(axis){
      this.mesh.material.transparent = true;
      this.mesh.material.opacity = 0.1;
      this.height = this.mesh.geometry.height;
      this.mem_objects = new THREE.Object3D();
      
      var offset = -1.5;
      var count = 0;
      var colour = [new THREE.Color('rgb(211,211,211)'),new THREE.Color('rgb(127,202,159)'),new THREE.Color('rgb(233,109,99)'),new THREE.Color('rgb(74,120,156)')]
      
      for(var i in groups[this.group_id].memory){
        
        var newmesh =  getShapeMesh(this.shape,this.radius,colour[count],this.height);
        newmesh.name = i;
        newmesh.position.copy(this.mesh.position);
        newmesh.position[axis] += .25*this.height*offset;
        newmesh.rotation = this.mesh.rotation;
        newmesh.scale[axis] = 0.25;
        
        this.mem_objects.add(newmesh);
        offset += 1;
        count += 1;
      }
      scene.add(this.mem_objects); 
    }

    // Sets up event listeners to enable to info box to appear on
 //    entity mouseover.
  this.initDomEvents = function() {
    var scope = this;
    if( this.state_types.length > 0){
      THREEx.DomEvents.eventNames.forEach(function(eventName) {
        if (eventName === 'mousemove') return
        if (scope.shape == 'model') {
          scope.domEvents(scope.clickmesh,eventName,window.event);
            // scope.mesh.getDescendants().forEach(function(child) {
//                 scope.domEvents(child, eventName, window.event);
//             });
        }
        else {
            scope.domEvents(scope.mesh, eventName, window.event);
        }
      });
    }
  }
    this.domEvents = function(mesh, eventName, event) {
        var scope = this;
        domEvents.addEventListener(mesh, eventName,
        function(event) {
            scope.writeInfo();
            // console.log("This group_id: " + scope.group_id + "this.id: " + scope.id);
            //var clickEvents = $(event.fromElement).data("events");
            // console.log("Mouseover: " + clickEvents);
            switch (infohide) {
                case 'autohide':
                  if (eventName === 'mouseover' ) {
                    $("#infobox").show();
                    // console.log("Showing Infobox");
                    infoVisible = true;
                }
                else if (eventName === 'mouseout') {
                    $("#infobox").hide();
                    // console.log("Hiding Infobox");
                    infoVisible = false;
                }
                break;
                case 'always on':
                if (eventName === 'mouseover') {
                    $("#infobox").show();
                    // console.log("Showing Infobox");
                    infoVisible = true;
                }
                break;
                case 'off':
                $("#infobox").hide();
                break;
                break;
            }
        },
        false)
    }
    
    this.removeEntity = function(){
      scene.remove(this.mesh);
      if( typeof this.textMesh !== 'undefined' )
        scene.remove(this.textMesh);
      if( typeof this.clickmesh !== 'undefined' )
        scene.remove(this.clickmesh);
      domEvents.removeEventListener(this.mesh,'mouseover');
      domEvents.removeEventListener(this.mesh,'mouseout');
    }
}


function getEntities(name){
  ents = [];
  for (var key in elements) {
    for (var s_type in elements[key].state_types){
      if(s_type === name)
        ents.push(elements[key]);
    }
  }
  return ents;
}




// Get the name of the shape type given the shape type enumerated.
function getShape(value) {
    value = parseInt(value);
    return shapes[value];

}

function toArray(string,type) {
    string = string.slice(1, string.length - 1);
    string = string.split("~");
    switch(type){
    case 'string':
      var array = [];
      for (s in string){
        array.push(string[s]);
      }
      return array;
      break;
    case 'vector':
      var x = parseInt(string[0]);
      var y = parseInt(string[1]);
      var z = parseInt(string[2]);
      return new THREE.Vector3(x, y, z);
      break;
    case 'euler':
      var x = parseFloat(string[0]);
      var y = parseFloat(string[1]);
      var z = parseFloat(string[2]);
      return new THREE.Euler(x,y,z,'XYZ');
      break;
    case 'colour':
      var r = parseInt(string[0]);
      var g = parseInt(string[1]);
      var b = parseInt(string[2]);
      return new THREE.Color("rgb("+r+","+g+","+b+")");
      break;
    }
}

// Checks if the enumerated shape type is a valid shape.
function isShape(value) {
    for (var i = 0; i < shapes.length; i++) {
        if (value == shapes[i])
        return true;
    }
    return false;
}



// Returns a 5-pointed star shaped geometry.
function starGeometry(radius) {
    var geom = new THREE.Geometry();
    geom.vertices.push(new THREE.Vector3(0, 0, 2));
    geom.vertices.push(new THREE.Vector3(0, 0, -2));
    geom.vertices.push(new THREE.Vector3(0, 5, 0));
    geom.vertices.push(new THREE.Vector3(0, -2, 0));
    geom.vertices.push(new THREE.Vector3(3, -4, 0));
    geom.vertices.push(new THREE.Vector3( - 3, -4, 0));
    geom.vertices.push(new THREE.Vector3(2, -1, 0));
    geom.vertices.push(new THREE.Vector3( - 2, -1, 0));
    geom.vertices.push(new THREE.Vector3(1, 2, 0));
    geom.vertices.push(new THREE.Vector3( - 1, 2, 0));
    geom.vertices.push(new THREE.Vector3(4, 2, 0));
    geom.vertices.push(new THREE.Vector3( - 4, 2, 0));

    geom.faces.push(new THREE.Face3(0, 8, 2));
    geom.faces.push(new THREE.Face3(0, 2, 9));
    geom.faces.push(new THREE.Face3(0, 9, 11));
    geom.faces.push(new THREE.Face3(0, 11, 7));
    geom.faces.push(new THREE.Face3(0, 7, 5));
    geom.faces.push(new THREE.Face3(0, 5, 3));
    geom.faces.push(new THREE.Face3(0, 3, 4));
    geom.faces.push(new THREE.Face3(0, 4, 6));
    geom.faces.push(new THREE.Face3(0, 6, 10));
    geom.faces.push(new THREE.Face3(0, 10, 8));
    geom.faces.push(new THREE.Face3(1, 2, 8));
    geom.faces.push(new THREE.Face3(1, 9, 2));
    geom.faces.push(new THREE.Face3(1, 11, 9));
    geom.faces.push(new THREE.Face3(1, 7, 11));
    geom.faces.push(new THREE.Face3(1, 5, 7));
    geom.faces.push(new THREE.Face3(1, 3, 5));
    geom.faces.push(new THREE.Face3(1, 4, 3));
    geom.faces.push(new THREE.Face3(1, 6, 4));
    geom.faces.push(new THREE.Face3(1, 10, 6));
    geom.faces.push(new THREE.Face3(1, 8, 10));

    geom.computeCentroids();
    geom.computeFaceNormals();
    geom.computeVertexNormals();
    var m = new THREE.Matrix4();
    m.multiplyScalar((radius - 2) / 2);
    geom.applyMatrix(m);
    return geom;
}

function textLabel(text,size,radius,obj_pos,colour){
  //Creation of Text Label Geometry
  var textGeo = new THREE.TextGeometry(text, {
      size: size,
      height: 1,
      font: 'helvetiker'
  });
  var material = new THREE.MeshBasicMaterial({
      color: colour
  });
  var textMesh = new THREE.Mesh(textGeo, material);
  var textoffset = {};
  textoffset.x = parseFloat(radius) * ( - 5);
  textoffset.y = parseFloat(radius) * 2;
  textoffset.z = parseFloat(radius);
  textMesh.position.x = parseInt(obj_pos.x) + textoffset.x;
  textMesh.position.y = parseInt(obj_pos.y) + textoffset.y;
  textMesh.position.z = parseInt(obj_pos.z) + textoffset.z;
  scene.add(textMesh);
  // console.log("Adding text:\"" + text + "\" at x:" + textMesh.position.x + " y:" + textMesh.position.y + " z:" + textMesh.position.z);
  return textMesh;
}

String.prototype.capitalise = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}


// Triggers each entity's step function every 50ms to render metrics
//      (spinning, bouncing, etc.)
window.setInterval(function() {
  count += 1;
  for (var group_id in groups){
    for (var id in groups[group_id].entities) {
        groups[group_id].entities[id].step();
    }
  }
  if(infoVisible){
    groups[lastHit_group].entities[lastHit].writeInfo();
  }
},
50);