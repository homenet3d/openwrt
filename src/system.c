/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
*   This file contains functions that receive and queue messages that are 
*   received on the local UDP port 27960
*   
*   For more information see docs/homenet3d-sys-state-comms.txt in 
*   Homenet3D v0.3 source code pacakge 
*   (http://caia.swin.edu.au/urp/homenet3d/openwrt#downloads)
*/


#include "system.h"


/*
* push_update(stack,item)
* description:
*   This function pushes and update to a stack of updates that are queued
*   here to be accessed later wehn sent to all clients.
* 
* passed:
*   stack: a stack of updates
*   item: a new update to be added to the stack
* 
*
*/
void push_update(update_t stack[],update_t item){
	if (top == (MAX_UPDATES-1))
		status = 0;
	else{   
		status = 1;
		++top;
		stack [top] = item;
	}
	lwsl_notice("NOTICE: pushUpdate() -> Update %d added",top);
}

/*
* pop_update(stack,index)
* description:
*   This is a pop function that removes the top update from the stack.
* 
* passed:
*   stack: The updates stack
* 
* returns:
*    the update that was just removed
*/
void pop_update (update_t stack[], int index){  
  int i = 0;
  if (top == -1){
		status = 0;
  }
  else{
    free(stack[index].clients);
    for(i = index; i > top; i++){
      stack[i] = stack[i+1]; 
    }
		status = 1;
		--top;
  }
	lwsl_notice("NOTICE: popUpdate() -> Update %d removed",index);
}

/*
* flush_update_buffer()
* description:
*   This function will clear the Update stack.
* 
*/
void flush_update_buffer(){
	int i;
  group_t *group;
	for(i = 0; i <= top; i++){
		pop_update(updates,i);
	}
  for(i = 0; i < group_count; i++){
    group = &(find_group(i)->group);
  }
}

/*
* init_connection(port)
* description:
*   This function initialised the UDP socket connection to allow updates 
*   to be sent to it.
* 
* passed:
*   port: the UDP port to listen for updates on
*
* returns:
*    a socket descriptor used in reading the updates from the socket
*/
int init_connection(port){
  const char * hostname = NULL;
  char * portname = (char *)malloc(10);   
  struct addrinfo stSockAddr, *res = 0;
	int socketFD = 0, err;

  sprintf(portname,"%d",port);

	memset(&stSockAddr,0,sizeof(stSockAddr));
	stSockAddr.ai_family = AF_INET6;
  stSockAddr.ai_socktype = SOCK_DGRAM;
  stSockAddr.ai_protocol= 0;
  stSockAddr.ai_flags=AI_PASSIVE|AI_ADDRCONFIG;
  
  
  err = getaddrinfo(hostname,portname,&stSockAddr,&res);
  if (err!=0) {
      lwsl_err("Failed to resolve local socket address (err=%d)",err);
      exit(EXIT_FAILURE);
  }
  
  if ((socketFD = socket(res->ai_family,res->ai_socktype,res->ai_protocol)) < 0){
    lwsl_err("ERROR: initConnection() -> Cannot create socket\n");
    exit(EXIT_FAILURE);
  }
  
  if((bind(socketFD,res->ai_addr, res->ai_addrlen) == -1)){
    lwsl_err("ERROR: initConnection() -> Bind failed");
    close(socketFD);
    exit(EXIT_FAILURE);
  }
  lwsl_notice("NOTICE: initConnection() -> Listening on port: %i\n", port);
  
  freeaddrinfo(res);
  free(portname);
	return socketFD;
}

/*
* listen_connection(socketFD)
* description:
*   This function listens in on the socket to see if any messages
*   have been received 
* 
* passed:
*   socketFD: the UDP socket file descriptor
* 
*/
void listen_connection(int socketFD){
	struct sockaddr_storage client;
  struct sockaddr_in *sin;
	socklen_t clen = sizeof(client);
	ssize_t recvlen = 0;
  char buf[13000];
  char host[NI_MAXHOST];
  char port[NI_MAXSERV];

  int rc =-1, group_id = -1; 
  
	recvlen = recvfrom(socketFD,buf,sizeof(buf),0,(struct sockaddr *)&client,&clen);
	if (recvlen !=-1){
    sin = (struct sockaddr_in *)&client;
    rc = getnameinfo((struct sockaddr *)&client, clen, host, sizeof(host), port, sizeof(port), NI_NUMERICHOST | NI_NUMERICSERV);
    
		update_ready = true;
    
    if(strstr(host,".")){
    	lwsl_notice("NOTICE: listenConnection() -> Received %d bytes from %s : %s\n", recvlen ,&host[7], port);
      group_id = get_group_id(&host[7]);
    }
    else{
      lwsl_notice("NOTICE: listenConnection() -> Received %d bytes from %s : %s\n", recvlen ,host, port);
      group_id = get_group_id(host);
    }
		buf[recvlen] = '\0';
		if(debug)
	    lwsl_notice("DEBUG: listenConnection() -> Received message: \"%s\"\n",buf);
		update_system_state(group_id,buf);
	}
}

/*
* get_group_id(host_ip)
* description:
*   This function returns the group ID given the client IP
* 
* passed:
*   hostname: String of Client IP address (v4/v6)
* 
* returns:
*   group_id of associated group
*/
int get_group_id(char *host_ip){
  int i, group_id = -1;
  group_t *group;
  for(i = 0; i < group_count; i++){
    group = &(find_group(i)->group);
    if(strcmp(host_ip,group->host_ip) == 0){
      group_id = i;
      break;
    }
  }
  return group_id;
}


/*
* get_attr(name)
* description:
*   This function returns the entity ID given the state type string
* 
* passed:
*   name: state type string that is used to match the static state
*         types
* 
* returns:
*   the entity's ID given a matched name.
*   If name not matche returns -1
*/
int get_attr(char *name){
	int id = -1;
	if(strstr(name,"hostname"))
		id = SYS_HOST;
	else if(strstr(name,"system"))
		id = SYS_SYS;
	else if(strstr(name,"model"))
		id = SYS_MODEL;
	else if(strstr(name,"uptime"))
		id = SYS_UPTIME;
	else if(strstr(name,"mem_total"))
		id = MEM_TOTAL;
	else if(strstr(name,"mem_inuse"))
		id = MEM_INUSE;
	else if(strstr(name,"mem_buffers"))
		id = MEM_BUFFERS;
	else if(strstr(name,"mem_cached"))
		id = MEM_CACHED;
	else if(strstr(name,"mem_free"))
		id = MEM_FREE;
	else if(strstr(name,"lan_ip"))
		id = LAN_IP;
	else if(strstr(name,"lan_nm"))
		id = LAN_NM;
	else if(strstr(name,"lan_gw"))
		id = LAN_GW;
	else if(strstr(name,"wan_ip"))
		id = WAN_IP;
	else if(strstr(name,"wan_nm"))
		id = WAN_NM;
	else if(strstr(name,"wan_gw"))
		id = WAN_GW;
	else if(strstr(name,"act_conns"))
		id = ACT_CONNS;
	else if(strstr(name,"max_conns"))
		id = MAX_CONNS;
	else if(strstr(name,"rx_rate"))
		id = RX_RATE;
	else if(strstr(name,"tx_rate"))
		id = TX_RATE;
	else if(strstr(name,"wifi"))
		id = RADIO;
	else if(strstr(name,"dhcp"))
		id = LEASES;
	else if(strstr(name,"nat"))
		id = NAT_TABLE;

	return id;
}


/*
* parse_message(group_id, buf)
* description:
*   This function parses a system state update and grabs the attribute
*   and it's value.
*   For more information see docs/homenet3d-sys-state-comms.txt in 
*   Homenet3D v0.3 source code pacakge 
*   (http://caia.swin.edu.au/urp/homenet3d/openwrt#downloads)
*
* 
* passed:
*   group_id: the ID of the group the update is intended for
*   buf: a char buffer that contains the update
* 
* returns:
*   
*/
update_t parse_message(int group, char *buf){
	update_t update;
  char * tok = NULL;
  char * value = NULL;
  char str[100];
	char * strs[12];
	int count = 0, i = 0, value_strlen = 0;
  *str = 0;
  update.group_id = group;
  update.attr = -1;
  update.index = -1;
  update.clients_size = 0;
  update.message = NULL;
  update.clients = (int *)malloc(MAX_CLIENTS*sizeof(int));
	
  while((tok = strsep(&buf,":")) != NULL && count < 12){
    strs[count] = (char *)malloc(strlen(tok)+1);
    strcpy(strs[count++],tok);
  }
  
  if(count > 1){
    if(strstr(strs[ATTR_ID],"_") && (strstr(strs[ATTR_ID],"wifi") || strstr(strs[ATTR_ID],"dhcp") || strstr(strs[ATTR_ID],"nat"))){
      tok = NULL;
      tok = strsep(&strs[ATTR_ID],"_");
      update.attr = get_attr(tok);
      tok = strsep(&strs[ATTR_ID],"_");
      update.index = atoi(tok);
    }
    else
      update.attr = get_attr(strs[ATTR_ID]);
    if(count > 2){
      for(i = 2; i <= count; i++){
        value_strlen = strlen(strs[i]);
      }
      update.message = (char * )malloc(value_strlen+1);
      for(i = 2; i < count; i++){
        value = (char * )malloc(strlen(strs[i])+1);
        value = strdup(strs[i]);
        strcat(str,value);
        if(i != count-1)
          strcat(str,"-");
      }
      update.message = (char *)malloc(strlen(str)+1);
      update.message = strdup(str);
    }
    else{
      update.message = (char *)malloc(strlen(strs[VALUE])+1);
      update.message = strdup(strs[VALUE]);
    }
    		
    for(i = 0; i < count; i++){
      free(strs[i]);
    }
    if(debug)
      lwsl_notice("DEBUG: parse_message-> Message: %s\n",update.message);
  }
  return update;
}

/*
* update_system_state(group_id,buf)
* description:
*   Parses and acts on updates read from buf.
* 
* passed:
*   group_id: the ID of the group to be updated
*   buf: the buffer containing an update message.
* 
*/
void update_system_state(int group_id, char * message){
  update_t update = parse_message(group_id,message);
  if(update.group_id < group_count){
    if(update.index < 0)
      set_group_value( update.attr,  update.message, update.group_id);
    else
      set_array_group_value(update.attr, update.message, update.group_id, update.index);
    if(count_pollfds > 1)
      push_update(updates,update);
  }
}

