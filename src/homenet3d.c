/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/* 	
* 	Homenet3d websocket server
* 
*   This document contains the main function and handles all websocket
*   communication for the system. 
*
*   There is a callback function 'callback_homenet3d' that handles websocket
*   callbacks for the app. This callback will call appropriate functions that
*   will send and update entity state to all clients.
*
*/

#include "homenet3d.h"

/* 
*   These global variables allow for the callback to access the websocket 
*   descriptors across the system. 
*      
*   As well and allowing updates to be stored and sent to multiple clients.
*/

// webpage string
char url[256];

// For handling termination signals.
int force_exit = 0;

// The socket file descriptor for the local UDP Socket used for updates.
int sockFD;

// Polling that allows for multiple client handling.
int max_poll_elements = 20;
struct pollfd *pollfds = NULL;
int *fd_lookup = NULL;

// Variables used to handle updates for multiple clients.
int counter = 0;

// Protocols used by the websocket context.
enum protocols {
        /* always first */
        PROTOCOL= 0,
        /* always last */
        DEMO_PROTOCOL_COUNT
};

// Struct that defines the information needed for each client connection.
// In our case we just need a file descriptor to identify each client.
struct per_session_data{
	int fd;
};



/*
* dump_handshake_info(wsi)
* description:
*   This function outputs the handshake info to the debug level of the log file.
* 
* passed:
*   wsi: a libwebsocket struct pointer that points to the websocket connection 
*        identifier
*/
static void
dump_handshake_info(struct libwebsocket *wsi)
{
  int n; 
	char buf[256];
      static const char *token_names[WSI_TOKEN_COUNT] = {
              /*[WSI_TOKEN_GET_URI]                =*/ "GET URI",
              /*[WSI_TOKEN_HOST]                =*/ "Host",
              /*[WSI_TOKEN_CONNECTION]        =*/ "Connection",
              /*[WSI_TOKEN_KEY1]                =*/ "key 1",
              /*[WSI_TOKEN_KEY2]                =*/ "key 2",
              /*[WSI_TOKEN_PROTOCOL]                =*/ "Protocol",
              /*[WSI_TOKEN_UPGRADE]                =*/ "Upgrade",
              /*[WSI_TOKEN_ORIGIN]                =*/ "Origin",
              /*[WSI_TOKEN_DRAFT]                =*/ "Draft",
              /*[WSI_TOKEN_CHALLENGE]                =*/ "Challenge",

              /* new for 04 */
              /*[WSI_TOKEN_KEY]                =*/ "Key",
              /*[WSI_TOKEN_VERSION]                =*/ "Version",
              /*[WSI_TOKEN_SWORIGIN]                =*/ "Sworigin",

              /* new for 05 */
              /*[WSI_TOKEN_EXTENSIONS]        =*/ "Extensions",

              /* client receives these */
              /*[WSI_TOKEN_ACCEPT]                =*/ "Accept",
              /*[WSI_TOKEN_NONCE]                =*/ "Nonce",
              /*[WSI_TOKEN_HTTP]                =*/ "Http",
              /*[WSI_TOKEN_MUXURL]        =*/ "MuxURL",

      };
  if(debug){
	  lwsl_notice("DEBUG: dump_handshake_info() ->");
    for (n = 0; n < WSI_TOKEN_COUNT; n++) {
      if (!lws_hdr_total_length(wsi,n))
        continue;
		  lws_hdr_copy(wsi,buf,sizeof(buf), n);
      lwsl_notice("    %s = %s\n", token_names[n], buf);
    }
  }
}

/*
* callback_homenet3d(context,wsi,reason,user,in,len)
* description:
*   web socket callback function that gets called on websocket events 
*   (e.g. receiving messages, writable connection)
* 
* passed:
*   context: A context from the websocket connection.
*   wsi: a libwebsocket struct pointer that points to the websocket connection 
*        identifier
*   reason: the websocket event that caused the callback
*   user: a pointer to an instacne of the user that established the connection
*   in: a pointer to the data that was received 
*   len: the size of the received data
* 
* returns:
*   an status integer indicating whether the function executed correctly(0) or 
*   not (any other number)
*/
static int
callback_homenet3d(struct libwebsocket_context * context, struct libwebsocket *wsi, enum libwebsocket_callback_reasons reason, void *user, void *in, size_t len)
{
	int m;
	char buf[512];
	struct libwebsocket_pollargs *pa = (struct libwebsocket_pollargs *)in;
  *buf = 0;
	switch (reason) {
		case LWS_CALLBACK_ESTABLISHED:
			lwsl_notice("NOTICE: callback_homenet3d() -> callback_w3bworld: LWS_CALLBACK_ESTABLISHED\n");
			sprintf((char *)buf,"{\"name\":\"deleteEverything\",\"args\":[{}]}}");
			if(libwebsocket_write(wsi, (unsigned char *)buf, strlen(buf), LWS_WRITE_TEXT))
				if(debug)
        	lwsl_notice("DEBUG: callback_homenet3d() -> %s\n",(char*)buf);
			sprintf((char *)buf,"{\"name\":\"set_webpage\",\"args\":[{\"url\":%s}]}}",url);
			if(libwebsocket_write(wsi, (unsigned char *)buf, strlen(buf), LWS_WRITE_TEXT))
				if(debug)
        	lwsl_notice("DEBUG: callback_homenet3d() -> %s\n",(char*)buf);
			lwsl_notice("NOTICE: callback_homenet3d() -> Initialising Entities\r\n");
      load_system_state(wsi);
			load_entities(wsi);

			libwebsocket_callback_on_writable(context,wsi);
			break;
		case LWS_CALLBACK_RECEIVE:
			if(debug)
    		lwsl_notice("DEBUG: callback_homenet3d() -> Reading from Client:\n%s\n",(char*)in);
			break;
		case LWS_CALLBACK_SERVER_WRITEABLE:
				if(debug)
        	lwsl_notice("DEBUG: callback_homenet3d() -> Client Count: %d\r\n",count_pollfds);
				if(top > -1)
          update_client(wsi, count_pollfds-1);
			
			break;
    case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
			lwsl_notice("NOTICE: callback_homenet3d() -> Websocket Connection established:\n");
            dump_handshake_info(wsi);
			break;
		case LWS_CALLBACK_ADD_POLL_FD:
			if (count_pollfds >= max_poll_elements) {
				lwsl_err("ERROR: callback_homenet3d() -> LWS_CALLBACK_ADD_POLL_FD: too many sockets to track\n");
				return 1;
			}

			fd_lookup[pa->fd] = count_pollfds;
			pollfds[count_pollfds].fd = pa->fd;
			pollfds[count_pollfds].events = pa->events;
			pollfds[count_pollfds++].revents = 0;
			break;

		case LWS_CALLBACK_DEL_POLL_FD:
			if (!--count_pollfds)
				break;
			m = fd_lookup[pa->fd];
			/* have the last guy take up the vacant slot */
			pollfds[m] = pollfds[count_pollfds];
			fd_lookup[pollfds[count_pollfds].fd] = m;
			break;

		case LWS_CALLBACK_CHANGE_MODE_POLL_FD:
			pollfds[fd_lookup[pa->fd]].events = pa->events;
			break;
		case LWS_CALLBACK_CLOSED:
			break;
		default:
			break;
	}
	return 0;
}

/*
* set_socket_blocking_enabled(fd,blocking)
* description:
*   Sets socket blocking mode (blocking or non-blocking)
* 
* passed:
*   fd: a file descriptor for the local UDP socket used for updates
*   blocking: boolean (true/false) to set socket to blocking or no-blocking
* 
* returns:
*   boolean where true means setting was successful and false unsuccessful
*/
bool set_socket_blocking_enabled(int fd, bool blocking)
{
   if (fd < 0) return false;

   int flags = fcntl(fd, F_GETFL, 0);
   if (flags < 0) return false;
   flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
   return (fcntl(fd, F_SETFL, flags) == 0) ? true : false;
}

/*
* libwebsocket_protocols
* description:
*   A struct containing all the supported protocols for this program
* 
* protocols:
*   homenet3d -> named 'protocol'
*             -> used callback function 'callback_homent3d'
*             -> per-session (multiple clients)
*/
static struct libwebsocket_protocols protocols[] = {
	{
		"protocol",
		callback_homenet3d,
		sizeof(struct per_session_data),
		0, 0, 0 ,0, 
	},
	{
		NULL, NULL, 0, 0, 0 ,0, 0,/* End of list */
	}
};

/*
* options
* description:
*   A struct containing all the command line options supported
* 
* options:
*   E.G.
*     help -> takes no argument
*          -> usage 'homenet3d -h' or 'homenet3d --help
*     debug -> takes agrument
*           -> usage 'homenet3d -d [debug_level]' 
*              or 'homenet3d --debug [debug_level]'
*
* debug levels:
*   0 -> none
*   1 -> error
*   2 -> warning
*   4 -> notice
*   8 -> info
*   16 -> debug
*
*   For multiple levels add:
*      E.G. For error + warning + debug = 1 + 2 + 16 = 19
*           usage: homenet3d -d 19
*/
static struct option options[] = {
	{ "help",	no_argument,		NULL, 'h' },
	{ "file",	required_argument,	NULL, 'f'},
	{ "debug",	required_argument,	NULL, 'd' },
	{ "port",	required_argument,	NULL, 'p' },
	{ "random", no_argument,		NULL, 'r' },
	{ "interface",  required_argument,	NULL, 'i' },
	{ "closetest",  no_argument,		NULL, 'c' },
	{ "daemonize", 	no_argument,		NULL, 'D' },
	{ NULL, 0, 0, 0 }
};


void free_mem(){
  struct radio_head radio_head;
  struct lease_head lease_head;
  struct nat_head nat_head;
  struct radio_struct *radio;
  struct lease_struct *lease;
  struct nat_entry_struct *nat;
  
  while (!TAILQ_EMPTY(&radio_head)) {
    radio = TAILQ_FIRST(&radio_head);
    TAILQ_REMOVE(&radio_head, radio, next);
    free(radio);
  }
  while (!TAILQ_EMPTY(&lease_head)) {
    lease = TAILQ_FIRST(&lease_head);
    TAILQ_REMOVE(&lease_head, lease, next);
    free(lease);
  }
  while (!TAILQ_EMPTY(&nat_head)) {
    nat = TAILQ_FIRST(&nat_head);
    TAILQ_REMOVE(&nat_head, nat, next);
    free(nat);
  }
  delete_all_groups();
  delete_all();
  free(entity_count);
}


/*
* sighandler(sig)
* description:
*   Exits the program cleanly when ctrl-c is used
* 
* passed:
*   sig: the signal number
*/
void sighandler(int sig)
{
	lwsl_notice("NOTICE: sighandler() -> Exiting: Caught signal: %i\n",sig);
	close(sockFD);
  free_mem();
  closelog();
	force_exit = 1;
}




/*
* main(argc,argv)
* description:
*   Runs homenet3d 
* 
* passed:
*   argc: a count of command line arguments
*   argv: a pointer to an array of strings of the command line arguments
* 
* returns:
*   an integer with an exit status
*/
int main(int argc, char **argv){

	// Declare all local variables
	struct lws_context_creation_info info;
	struct libwebsocket_context *context = NULL;
	int n = 0, port = 10001, i = 0;
	unsigned int oldus;
	char filename[128] = "";
  char name[128], version[20];
	int debug_level = 1;
	int daemonize = 0;
  int socket_port = 27960;
	bool random = false;
	debug = false;
  update_needed = true;
  update_ready = false;
  count_pollfds = 0;
	// Initialise global variables
	top = -1;
	topLease = -1;
  group_count = -1;
	topRadio = -1;
    
	// Handle the command line arguments
	while (n >= 0) {
		n = getopt_long(argc, argv, "hpf:d:Dr:", options, NULL);
		if (n < 0)
			continue;
		switch (n) {
		case 'D':
			daemonize = 1;
			break;
		case 'f':
			sprintf(filename,"%s",optarg);
			break;
		case 'd':
			debug_level = atoi(optarg);
			break;
		case 'p':
			port = atoi(optarg);
			break;
		// case 'i':
//       strncpy(interface_name, optarg, sizeof interface_name);
//       interface_name[(sizeof interface_name) - 1] = '\0';
//       iface = interface_name;
//       break;
		case 'r':
			random = true;
			break;
		case 'h':
			fprintf(stderr, "Usage: homenet3d "
					"[--port=<p>] "
					"[-d <loglevel> (default=1)] "
					"[--file <path to config file>]\n"
          "\n    NOTE: Log level (add to combine): \n"
          "        0 : off\n"
          "        1 : errors\n"
          "        2 : warnings\n"
          "        4 : info\n"
          "       16 : debug\n\n\n");
			exit(1);

		}
	}
  
	// Set debug level 
	if(debug_level >= 16 && debug_level < 32){
        debug = true;
	}
	lws_set_log_level(debug_level, lwsl_emit_syslog);  
  
	// Read in configuration from file
  
  if(strlen(filename) < 1){
    sprintf(filename, "/usr/local/share/homenet3D/homenet3d.cfg");
  }
  if(config_start(filename) == 1){
    fprintf(stderr,"Error reading Config file.\n Perhaps a syntax error in the file?\n");
    exit(1);
  }
  
  // Start Homenet3D
  parse_app_config(name,version,url);
  fprintf(stderr,"%s \nVersion: %s\n",name,version);
  
  
	// Configure websocket info
	memset(&info, 0, sizeof info);
  if(config_get_ws_port() > 0)
	  info.port = config_get_ws_port();
  else
    info.port = port;
	info.protocols = protocols;
	info.iface = NULL;
	info.ssl_cert_filepath = NULL;
	info.ssl_private_key_filepath = NULL;
	info.extensions = libwebsocket_get_internal_extensions();
	info.gid = -1;
	info.uid = -1;
	info.options = 0;
  

	lwsl_notice("NOTICE: main() -> Loading default entity values\n");
	create_world();

	// Daemonize the process (if option is selected)
	if (daemonize && lws_daemonize("/tmp/.lwsts-lock")) {
		lwsl_err( "ERROR: main() -> Failed to daemonize\n");
		return 1;
	}

	// Handle the termination signal (Ctrl-C)
	signal(SIGINT, sighandler);


	// Create file descriptors to be used to poll/service multiple clients
  max_poll_elements = getdtablesize();
	pollfds = malloc(max_poll_elements * sizeof (struct pollfd));
	fd_lookup = malloc(max_poll_elements * sizeof (int));
	if (pollfds == NULL || fd_lookup == NULL) {
		lwsl_err("ERROR: main() ->Out of memory pollfds=%d\n", max_poll_elements);
		return -1;
	}

	// Create Websocket
	context = libwebsocket_create_context(&info);
	if (context == NULL) {
		lwsl_err("ERROR: main() -> Creating libwebsocket context failed\n");
		return -1;
	}

	// Create Socket
  if(config_get_s_port() > 0)
	  socket_port = config_get_s_port();
	sockFD = init_connection(socket_port);
	if(set_socket_blocking_enabled(sockFD, false)){
		lwsl_debug("NOTICE: main() -> Socket set to non-blocking mode\n");
	}


	// Service websockets
	n = 0;
	while (n >= 0 && !force_exit){
		struct timeval tv;
		gettimeofday(&tv, NULL);

		if (((unsigned int)tv.tv_usec - oldus) > 1000) {
			libwebsocket_callback_on_writable_all_protocol(&protocols[PROTOCOL]);
			oldus = tv.tv_usec;
		}
    // Listen on UDP socket for updates
    listen_connection(sockFD);
    
		n = poll(pollfds, count_pollfds, 50);
		if (n){
			for (i = 0; i < count_pollfds; i++){
				if (pollfds[i].revents){
					/*
					* returns immediately if the fd does not
					* match anything under libwebsockets
					* control
					*/
					if (libwebsocket_service_fd(context,&pollfds[i]) < 0)
						continue;
				}
			}
		}
	}
	libwebsocket_context_destroy(context);
	lwsl_notice("NOTICE: main() -> W3bworld server exited cleanly\n");
	return 0;
}

