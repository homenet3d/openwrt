/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#ifndef ENTITY_H_
#define ENTITY_H_

#include "homenet3d.h"


void init_group(group_t *group);
void add_group(int id, group_t group);
void delete_group(group_hash_t *config);
void delete_all_groups();
group_hash_t *find_group(int id);

void init_entity(entity_t *entity);
void print_entity(entity_t *entity);
void print_group(int group_id, int entity_count);
void create_world();

void load_system_state(struct libwebsocket *wsi);
void load_entity(struct libwebsocket *wsi, entity_t *ent);
void update_client(struct libwebsocket *wsi, int no_clients);
void load_entities(struct libwebsocket *wsi);

void set_group_value(int attr, const char * value, int group_id);
void set_array_group_value(int attr, const char * value, int group_id, int index);

void update_wifi(const char * value, int group_id, int index);
void update_lease(const char * value, int group_id, int index);
void update_nat_table(const char *value, int group_id, int index);
bool is_wifi_lease(char *mac, int group_id);

#endif
