#!/usr/bin/lua
--[[ 
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
]]
socket = require "socket"
host = "127.0.0.1"
port = 27960

devs = {}
devCount = 0
leases = {}
numDevs = 29

function init()
	for i = 0,numDevs do
		local expiry = 120000
		leases[i] = {["mac"] = nil, ["hostname"] = nil, ["ip"] = nil}
		leases[i]["mac"] = "00-00-00-00-00-" .. string.format("%02d",i)
		leases[i]['hostname'] = "Client" .. string.format("%02d",i)
		leases[i]['ip'] = "192.168.1." .. i
		local msg = "dhcp:" .. i .. ":" .. expiry .. "~" .. leases[i]['mac'] .. "~" .. leases[i]["ip"] .. "~" ..  leases[i]["hostname"] .. "~"
		c:sendto(msg,host,port)
		--print (msg)
		sleep(0.5)	
	end
	print('Loaded DHCP Leases')
end

function addDev(mac,dev)
	msg = "wifi:0:wlan0~11~OpenWRT-HN3D~~"
	devs[mac] = mac
	for k,v in pairs(devs) do
		if v ~= nil then
			msg = msg .. v .."$"
		end
	end
	devCount = devCount + 1
	--print(msg)
	print(devCount)
	c:sendto(msg,host,port)
	
end
function removeDev(mac)
	devs[mac] = nil
	msg = "wifi:0:wlan0~11~OpenWRT-HN3D~~"
	for k,v in pairs(devs) do
		if v ~= nil then
			msg = msg .. v .."$"
		end
	end
	devCount = devCount - 1
	-- print(msg)
	print(devCount)
	c:sendto(msg,host,port)
end
	

function sleep(n)
	socket.select(nil,nil,n)
end

function getIndex(answer)
	math.randomseed(os.time())
	local index
	if(answer == 'a' and devCount <= numDevs) then
		repeat
			index = math.random(0,numDevs)
		until devs[leases[index].mac] == nil
	elseif (answer == 'r' and devCount > 0) then
		repeat
			index = math.random(0,numDevs)
		until devs[leases[index].mac] ~= nil
	end
	return index
end

function main()
	c = socket.udp()
	print("Running Device Spawning Demo")
	init()

	local index = getIndex('a')
	local dev = leases[index]
	addDev(dev.mac,dev)
	sleep(0.05)
	index = getIndex('a')
	local answer = 'a'
	while true do
		-- repeat
		-- 	   io.write("Add device (a) or Remove device (r)? ")
		-- 	   io.flush()
		-- 	   answer=io.read()
		-- 	until answer=="a" or answer=="r"
		if(devCount == numDevs) then
			answer = 'r'
		elseif(devCount == 2) then
			answer = 'a'
		end
		
		index = getIndex(answer)
		dev = leases[index]
		if(answer == 'r') then
			if(devCount == 0) then
				print("No device to remove")
			else
				removeDev(dev.mac)
			end
		elseif (answer == 'a') then
			if(devCount > numDevs) then
				print("Maximum devs has been reached (".. numDevs+1 ..").")
			else
				addDev(dev.mac,dev)
			end
		end
		sleep(1)
	end
	c:close()
end

main()