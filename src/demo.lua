#!/usr/bin/lua
--[[ 
Copyright (c) 2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
]]

-- Syntax:
--    demo.lua [host] [port] [ip version]

-- If you want to run multiple instances of this script as though the information 
-- was coming from multiple routers call demo.lua multiple times:
--    demo.lua 127.0.0.1 27960 


socket = require "socket"
host = arg[1]
port = arg[2]
ip_version = arg[3]


memtotal = 32000
data_min = 300
data_max = 300000
max_conns = 16563

devs = {}
devCount = 0
numGroups = 1
leases = {}
nat_table = {}
act_conns = 150
numDevs = 20

answer = 'a'


function sleep(n)
  socket.sleep(n)
end

function send(msg)
    sleep(0.01)
    c:sendto(msg,host,port)
    print("Sent Message:" .. msg)
end

function makemsg(attr,update)
  return attr .. ":" .. update
end

function getIndex(answer)
	local index = -1
  local c = 0
	if(answer == 'a' and devCount < numDevs) then
		repeat
      c = c + 1
      if c == numDevs then
        break
      end
			index = math.random(0,numDevs)
		until devs[leases[index].mac] == nil
	elseif (answer == 'r' and devCount >= 0) then
		repeat
      c = c + 1
      if c == numDevs then
        break
      end
			index = math.random(0,numDevs)
		until devs[leases[index].mac] ~= nil
	end
	return index
end


function system_init()
    local hostname = "Homenet3D"
    send(makemsg("hostname",hostname))
    local system = "OpenWRT"
    send(makemsg("system",system))
    local model = "QEMU"
    send(makemsg("model",model))
end

function memory_init()
    send(makemsg("mem_total",memtotal))
    send(makemsg("mem_inuse",0))
    send(makemsg("mem_free",0))
    send(makemsg("mem_buffers",0))
    send(makemsg("mem_cached",0))
end

function network_init()
    send(makemsg("lan_ip","192.168.0.1"))
    -- send(makemsg("network","lan_gw",""))
    send(makemsg("lan_nm","255.255.255.0"))
    send(makemsg("wan_ip","144.144.144.144"))
    send(makemsg("wan_gw","144.144.144.1"))
    send(makemsg("wan_nm","255.255.255.252"))
    send(makemsg("act_conns",act_conns))
    send(makemsg("max_conns",max_conns))
end
function nat_init()
  for i = 0,act_conns do
    nat_table[i] = {["bytes"] = nil, ["sport"] = nil, ["dport"] = nil, ["packets"] = nil, ["src"] = nil, ["dst"] = nil, ["layer3"] = nil, ["layer4"] = nil}
    nat_table[i].bytes = i
    local msg = "nat_".. i .. ":" .. nat_table[i].bytes
    send(makemsg("nat_".. i,"bytes_" .. nat_table[i].bytes))
    nat_table[i].sport = i
    msg = "nat_".. i .. ":" .. nat_table[i].sport
    send(makemsg("nat_".. i, "sport_" .. nat_table[i].sport))
    nat_table[i].dport = i
    msg = "nat_".. i .. ":" .. nat_table[i].dport
    send(makemsg("nat_".. i,"dport_" .. nat_table[i].dport))
    nat_table[i].packets = i
    msg = "nat_".. i .. ":" .. nat_table[i].packets
    send(makemsg("nat_".. i,"packets_" .. nat_table[i].packets))
    nat_table[i].layer3 = "ip"
    msg = "nat_".. i .. ":" .. nat_table[i].layer3
    send(makemsg("nat_".. i,"layer3_" .. nat_table[i].layer3))
    nat_table[i].layer4 = "tcp"
    msg = "nat_".. i .. ":" .. nat_table[i].layer4
    send(makemsg("nat_".. i,"layer4_" .. nat_table[i].layer4))
    nat_table[i].src = "host-src" .. i
    msg = "nat_".. i .. ":" .. nat_table[i].src
    send(makemsg("nat_".. i,"src_" .. nat_table[i].src))
    nat_table[i].dst = "host-dst" .. i
    msg = "nat_".. i .. ":" .. nat_table[i].dst
    send(makemsg("nat_".. i,"dst_" .. nat_table[i].dst))
  end
end

function devices_init()
	for i = 0,numDevs do
		local expiry = 120000
		leases[i] = {["mac"] = nil, ["hostname"] = nil, ["ip"] = nil,["expiry"] = nil,["num"] = nil}
		leases[i]["expiry"] = expiry
		leases[i]["mac"] = "00-00-00-00-00-" .. string.format("%02d",i)
		leases[i]['hostname'] = "Client" .. string.format("%02d",i)
		leases[i]['ip'] = "192.168.1." .. i
		leases[i]["num"] = i
	end
  -- print('Loaded DHCP Leases')
end

function system()
    local uptime = os.time() - starttime
    send(makemsg("uptime",uptime))
end

function memory()
    local tot_value = math.random(0,math.floor(memtotal/3))
    send(makemsg("mem_inuse",tot_value))
    local buff_value = math.random(0,math.floor(memtotal/3))
    send(makemsg("mem_buffers",buff_value))
    local cache_value = math.random(0,math.floor(memtotal/3))
    send(makemsg("mem_cached",cache_value))
    local free_value = memtotal - math.floor(tot_value + buff_value + cache_value)
    send(makemsg("mem_free",free_value))
end

function network()
  -- local act_conns = math.random(0,max_conns)
  send(makemsg("act_conns",act_conns))
  local tx_rate = math.random(math.floor(data_min),math.floor(data_max*.25))
  send(makemsg("tx_rate",tx_rate))
  local rx_rate = math.random(math.floor(data_min),math.floor(data_max*.25))
  send(makemsg("rx_rate",rx_rate))
	-- nat(group,act_conns)
end

function devices()
    
  if(devCount == numDevs) then
		answer = 'r'
	elseif(devCount == 2) then
		answer = 'a'
	end
	
	index = getIndex(answer)
	dev = leases[index]
	if(answer == 'r') then
		if(devCount == 0) then
			print("No device to remove")
		else
			removeDev(dev.mac)
		end
	elseif (answer == 'a') then
		if(devCount > numDevs) then
			print("Maximum devs has been reached (" .. numDevs+1 .. ").")
		else
			addDev(dev.mac,dev)
		end
	end
end

function addDev(mac,dev)
  print("mac: " .. mac)
  if devCount %2 == 0 then
    update = "wlan0~11~OpenWRT-HN3D~~"
	  devs[mac] = mac
  	for k,v in pairs(devs) do
  		if v ~= nil and k ~= 0 then
  			update = update .. k .."$"
  		end
  	end
    send(makemsg("wifi_"..0,update))
  end
  devCount = devCount + 1
	local update = dev.expiry .. "~" .. dev.mac .. "~" .. dev.ip .. "~" ..  dev.hostname .. "~"
  send(makemsg("dhcp_"..dev.num,update))
end

function removeDev(mac)
	devs[mac] = nil
	local update = "wlan0~11~OpenWRT-HN3D~~"
	for k,v in pairs(devs) do
		if v ~= nil and k ~= 0 then
			update = update .. v .. "$"
		end
	end
	devCount = devCount - 1
	send(makemsg("wifi_" .. 0,update))
end

-- function nat(group)
--
--   nat_table[]
-- end

function init()
   system_init() 
   network_init()
   nat_init()
end

function doUpdate()
    memory()
    system()
    network()
    devices()
    -- nat()
		sleep(2);
end


function main()
  if(ip_version == nil or (ip_version ~= 4 and ip_version ~= 6) ) then
    ip_version = 4
  end
  if(host == nil) then
    if(ip_version == 4) then
      host = "127.0.0.1"
    elseif(ip_version == 6) then
      host = "::1"
    end
  end
  if(port == nil) then
    port = 27960
  end
  
  
  print ("Starting demo state")
  local count = 0
  if(ip_version == 6) then
    c = socket.udp6()
  else
    c = socket.udp()
  end
  
  -- Used to reset uptime once and once only.
  starttime = os.time()
  local uptime = 0
  send(makemsg("uptime",uptime))
  memory_init()
  devices_init()
  
  for i = 0,numGroups-1 do
    devs[i] = {}
    init(i)
  end
  while(true) do
  	count = count + 1
    for i = 0,numGroups-1 do
      math.randomseed(math.random(0,memtotal))
      doUpdate(i)
    	if(count == 10) then
        print ("Re-init state " .. count)
        init(i)
        count = 0
    	end
    end
  end
end


main()
