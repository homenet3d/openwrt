/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/


#ifndef STR_H_
#define STR_H_

#include "homenet3d.h"

void attr_to_string(char *value,int attr);
int string_to_attr(char *attr);
void array_to_string(char *buf, entity_t *entity, int attr);
void leases_to_string(char * buf, int bufsize, group_t *group);
void lease_to_string(char * buf, int id, int index);
void wifis_to_string(char * buf, int bufsize, group_t *group);
void wifi_to_string(char * buf, int id, int index);
void nats_to_string(char * buf, int bufsize, group_t *group);
void nat_to_string(char * buf, int group_id, int index);
void update_to_string(char * buf, int attr, const char *value, int group_id, int index);
void ent_to_string(char * buf, entity_t *entity);
void group_to_string(char * buf, group_t *group);
int get_update_bufsize(int attr, int index, int group_id, int msglen);
#endif