#!/usr/bin/lua
--[[ 
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
]]


socket = require "socket"
sys = require "luci.sys"
ipt = require "luci.sys.iptparser"

server = arg[1]
port = arg[2]
currentState = nil

function getState()
  local hostname = sys.hostname()
  local uptime = sys.uptime()
  local systemvers, model, memtotal_value, memcached_value, membuffers_value, memfree_value = sys.sysinfo()
  model = string.sub(model,0,-2)
  local total = memtotal_value - (memcached_value + membuffers_value + memfree_value)
  local lan,wan = getNetwork()
  local conns, max = getActConns()
  local rx, tx = getRates()
  local dhcp = getLeases()
  local wifi = getWdev()
  local nat = getNAT()
  
	local state = {["hostname"] = hostname,["system"] = systemvers,["model"] = model,["uptime"] = uptime,["mem_inuse"] = total,["mem_cached"] = memcached_value,["mem_buffers"] = membuffers_value,["mem_free"] = memfree_value, ["mem_total"] = memtotal_value, ["lan"] = lan,["wan"] = wan, ["act_conns"] = conns, ["max_conns"] = max,["rx_rate"] = rx,["tx_rate"] = tx, ["dhcp"] = dhcp, ["wifi"] = wifi, ["nat"] = nat}	
	return state
end

function getChanges(currentState)
	local changes = getState()
		for ent,val in pairs(changes) do
			if type(val) ~= "table" then
				if val == currentState[ent] then
					changes[ent] = nil
				else 
					currentState[ent] = val
				end
			else
				for entity,value in pairs(changes[ent]) do
					if type(value) ~= "table" then
						if value == currentState[ent][entity] then
							changes[ent][entity] = nil
						else
							currentState[ent][entity] = value
						end
					else
						for sub_entity,sub_value in pairs(changes[ent][entity]) do
              if type(sub_value) ~= "table" then
                -- print("ent: " .. ent .. " entity: " .. entity .. " sub_entity:" .. sub_entity .. " value: ".. sub_value)
                if currentState[ent][entity] ==  nil then
                  currentState[ent][entity] = {}
                end
							  if sub_value == currentState[ent][entity][sub_entity] then
						      changes[ent][entity][sub_entity] = nil
						    else
							    currentState[ent][entity][sub_entity] = sub_value
							  end
              else
              end
						end
					end
				end
			end
		end

	return changes
end


function run(c,currentState)
	local changes = getChanges(currentState)
	sendUpdate(c,changes,currentState)
end

function sendUpdate(c,changes,currentState)
	if changes ~= nil then
		for attr,value in pairs(changes) do
			if value ~= nil then
				if type(value) ~= "table" then
					msg = attr .. ":" .. value
					c:sendto(msg,server, port)
          print("Message Sent: " .. msg)
					sleep(0.01)
				else
					for attr_tab,value_tab in pairs(changes[attr]) do
						if type(value_tab) ~= "table" and value_tab ~= nil then
              msg = attr .. "_" .. attr_tab .. ":" .. value_tab
							c:sendto(msg,server,port)
              print("Message Sent: " .. msg)
              sleep(0.01)
            else
              for subattr_tab,subvalue_tab in pairs(changes[attr][attr_tab]) do
                if type(subvalue_tab) ~= "table" and value_tab ~= nil then
                  msg = attr .. "_" .. attr_tab .. ":" .. subattr_tab .. "_" .. subvalue_tab
							    c:sendto(msg,server,port)
                  print("Message Sent: " .. msg)
                  sleep(0.01)
                end
              end
            end
            
					end
				end
			end
		end
	end
end

function file_exists(file)
	local f = io.open(file, "rb")
	if f then f:close() end
	return f ~= nil
end

function getNetwork()
	local lan = {}
	local wan = {}
  
  local lanip_subnet = io.popen("echo `. /lib/functions/network.sh; network_get_subnet lanip lan; echo $lanip`")
  lanip_subnet = lanip_subnet:read("*a")
  lanip_subnet = lanip_subnet:gsub("\r","")
  lanip_subnet = lanip_subnet:gsub("\n","")
  local split= lanip_subnet:split("/");
  lanip = split[1]
  lannm = split[2]
  local langw = io.popen("echo `. /lib/functions/network.sh; network_get_gateway langw lan; echo $langw`")
  langw = langw:read("*a")
  langw = langw:gsub("\r","")
  langw = langw:gsub("\n","")
  
  local wanip_subnet = io.popen("echo `. /lib/functions/network.sh; network_get_subnet wanip wan; echo $wanip`")
  wanip_subnet = wanip_subnet:read("*a")
  wanip_subnet = wanip_subnet:gsub("\r","")
  wanip_subnet = wanip_subnet:gsub("\n","")
  local split = wanip_subnet:split("/");
  wanip = split[1]
  wannm = split[2]
  local wangw = io.popen("echo `. /lib/functions/network.sh; network_get_gateway wangw wan; echo $wangw`")
  wangw = wangw:read("*a")
  wangw = wangw:gsub("\r","")
  wangw = wangw:gsub("\n","")
  print( lanip);
	lan["ip"] = lanip
	lan["nm"] = lannm
	lan["gw"] = langw
	wan["ip"] = wanip
	wan["nm"] = wannm
	wan["gw"] = wangw

	return lan,wan
end

function getActConns()
	local fin = false
  local t
  local s = "net.netfilter.nf_conntrack_max"
	local conns = sys.net.conntrack()
	local count = 0
	local max = 0

	if(conns ~= nil) then
		for k,v in pairs(conns) do
	        	count = count + 1
		end
		local f = assert(io.open("/etc/sysctl.conf","r"))
		local fin = false
		local t
		local s = "net.netfilter.nf_conntrack_max"
		while not fin do
		        t = f:read()
		        if t:find(s) ~= nil  then
		                fin = true
		        end
		end
		max = t:sub(s:len()+2)
		f:close()
	end
	return count,max
end

function getNAT()
	local conns = sys.net.conntrack()
  local nat_table = {}

	if conns ~= nil then
		for k,v in pairs(conns) do
      nat_table[k] = {}
	      for key,value in pairs(conns[k]) do
	        nat_table[k][key] = value
	      end
		end
	end
	return nat_table
end

function getRates()
	local rx = 0
	local tx = 0
	local f = assert(io.popen("cat /var/rx","r"))
	rx = assert(f:read('*a'))
	f:close()

	f = assert(io.popen("cat /var/tx","r"))
	tx = assert(f:read('*a'))
	f:close()

	return rx,tx
end

function getLeases()
	local filename = "/var/dhcp.leases"
	if not file_exists(filename) then return {} end
	local file = io.open(filename, "r")
	local leases = {}
	
	for line in file:lines() do
	   table.insert (leases, line)
	end
	for k,v in pairs(leases) do
		leases[k] = leases[k]:gsub(":", "-")
		leases[k] = leases[k]:gsub(" ", "~")
	end
	return leases
	
end

function getWifi(wdev,attr)
	local output = ""
	if attr == "channel" then
		local f = assert(io.popen("iw dev "..wdev.." info | grep \"channel\" | cut -d ',' -f 1 | cut -c 10-25","r"))
		local channel = assert(f:read('*a'))
		f:close()
		output = channel
	elseif attr == "ssid" then
		f = assert(io.popen("iw dev "..wdev.." info | grep \"ssid\" | cut -d ',' -f 1 | cut -c 7-25","r"))
		local ssid = assert(f:read('*a'))
		f:close()
		output = ssid
	elseif attr == "type" then
		f = assert(io.popen("iw dev "..wdev.." info | grep \"type\" | cut -d ',' -f 1 | cut -c 7-25","r"))
		local mode = assert(f:read('*a'))
		f:close()
		output = mode
	elseif attr == "assocs" then
		f = assert(io.popen("iw dev "..wdev.." station dump | grep \"Station\" | cut -d ' ' -f 2","r"))
		local assocs = ""
		for line in f:lines() do
			assocs = assocs .. line .. "$"
		end
		assocs = assocs:gsub(":","-")
		f:close()
		output = assocs
	end
	return output
		-- f = assert(io.popen("cat /var/rx_wifi","r"))
		-- 		rx_wifi = assert(f:read('*a'))
		-- 		f:close()
		-- 		rx_wifi = rx_wifi:gsub("\r","");
		-- 		rx_wifi = rx_wifi:gsub("\n","");
		-- 	
		-- 		f = assert(io.popen("cat /var/tx_wifi","r"))
		-- 		tx_wifi = assert(f:read('*a'))
		-- 		f:close()
		-- 		tx_wifi = tx_wifi:gsub("\r","");
		-- 		tx_wifi = tx_wifi:gsub("\n","");

end

function getWdev()
	local wifis = {}
	local f = assert(io.popen("iw dev | grep \"Interface\" | cut -d ' ' -f 2","r"))
	if f == nil then
		wifis = nil
	else
		local devs = {}
		for line in f:lines() do
			table.insert(devs,line)
		end
		f:close()
		if devs ~= nil then
			local i = 0
			for k,v in pairs(devs) do
				local ssid = getWifi(v,"ssid")
				ssid = ssid:gsub("\r","")
				ssid = ssid:gsub("\n","")
				local channel = getWifi(v,"channel")
				channel = channel:gsub("\r","")
				channel = channel:gsub("\n","")
				wifis[i] = v .. "~" .. channel .. "~" .. ssid .. "~" .. getWifi(v,"mode") .. "~" .. getWifi(v,"assocs")
				i = i + 1
			end
		end
	end
	return wifis
end

function sleep(n)
  socket.sleep(n)
  --os.execute("usleep " .. n*1000)
end

function main()
  -- Init socket variables
  if(server == nil) then
    server = "127.0.0.1"
  end
  if(port == nil) then
    port = 27960
  end

  
  print('Get Initial System State')
  currentState = getState()
  c = socket.udp()
  sendUpdate(c,currentState,nil)
  print('Monitoring System State')
  local count = 0
  while true do
    run(c,currentState)
    sleep(2)
    count = count + 1
    if count == 10 then
      currentState = getState()
      sendUpdate(c,currentState,nil)
      print('Sent Full Update')
      count = 0
    end
  end
  c:close()
end

main()
