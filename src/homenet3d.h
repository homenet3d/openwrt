/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

#ifndef HOMENET3D_H_
#define HOMENET3D_H_

#define MAX_ENTS      100
#define MAX_CLIENTS   20
#define MAX_MAPPINGS  5
#define MAX_STATETYPES 10
#define MAX_GROUPS    50

#define	MAC       0x01
#define NUM       0x02
#define EXPIRY    0x04
#define IP        0x08
#define HOSTNAME  0x10
#define ID        0x20
#define WIFI      0x40


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <syslog.h>
#include <signal.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/queue.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <libwebsockets.h>
#include <libconfig.h>
#include "uthash/uthash.h"

enum attribs {
  // Entity attrs
	POSITION,
  STATETYPES,
	BOUNCE_HEIGHT,
	BOUNCE_FREQ,
	ROTATE_SPEED,
	COLOUR,
	LABEL,
	RADIUS,
	SCALE,
	SHAPE,
	TEXT_COLOUR,
	ROTATION,
  MAPPINGS,
  
  // Group attrs
	SYS_HOST,
	SYS_SYS,
	SYS_MODEL,
	SYS_UPTIME,
	MEM_TOTAL,
	MEM_CACHED,
	MEM_BUFFERS,
	MEM_FREE,
  MEM_INUSE,
	LAN_IP,
	LAN_NM,
	LAN_GW,
	WAN_IP,
	WAN_NM,
	WAN_GW,
	ACT_CONNS,
	MAX_CONNS,
	RX_RATE,
	TX_RATE,
	RADIO,
	LEASES,
  NAT_TABLE
};

enum vectors {
  X_COORD = 0,
  Y_COORD = 1,
  Z_COORD = 2
};

enum colours {
	RED = 0,
	GREEN = 1,
	BLUE = 2
};



typedef struct attr_struct{
  const char *name, *min, *max, *gran;
}attr_t;
typedef struct metric_struct{
  const char *name, *upper_thres, *lower_thres;
}metric_t;

typedef struct mapping_struct{
  int attrs_count;
  attr_t *attrs;
  metric_t metric;
}mapping_t;

struct radio_struct{
  int id;
	int channel;
	char *mode,*ssid,*assocs,*interface;
  TAILQ_ENTRY(radio_struct) next;
};

struct lease_struct{
	int expiry, num;
	char *mac,*ip,*name,*id;
	bool wifi;
  TAILQ_ENTRY(lease_struct) next;
};

struct nat_entry_struct{
  int id;
	int bytes,sport,dport,packets;
	char *src,*dst,*layer3,*layer4;
  TAILQ_ENTRY(nat_entry_struct) next;
};


typedef struct entity_struct{
	int id, group_id, width, height, bounce_height, radius, shape, state_type_count, mappings_count;
	double scale,rotate_speed, bounce_freq;
  int *position,*colour,*text_colour;
  double *rotation;
	const char *label, *url, *objfile, *mtlfile, *autoscale;
	const char **state_types;
  mapping_t *mappings;
}entity_t;

TAILQ_HEAD(nat_head, nat_entry_struct);
TAILQ_HEAD(lease_head, lease_struct);
TAILQ_HEAD(radio_head, radio_struct);

typedef struct group_struct{
  int id;
  entity_t *entities;
	// System info
	const char *hostname,*host_ip, *system, *model;
	int uptime;
	
	// Memory info
	int mem_inuse,mem_buffers,mem_cached,mem_free,mem_total;
	
	// Network info
	const char *lan_ip,*lan_gw,*lan_nm,*wan_ip,*wan_gw,*wan_nm;
	int act_conns,max_conns,rx_rate,tx_rate;
	attr_t conns, datarate;
	
	// Wifi info
	struct radio_head radios;
	
	// Leases info
	struct lease_head leases;
  
  // NAT Table info
  struct nat_head nat_table;
  
} group_t;



typedef struct config_struct {
    int id;
    entity_t entity;
    UT_hash_handle hh;         /* makes this structure hashable */
}entity_config_t;

typedef struct group_hash_struct  {
    int id;
    group_t group;
    UT_hash_handle hh;         /* makes this structure hashable */
}group_hash_t;

typedef struct update_struct{
	int group_id, index, attr, *clients, clients_size;
	char *message;
}update_t;


int group_count, *entity_count, statusEnt, topLease, statusLease, topRadio, statusRadio;
int count_pollfds;

bool debug, update_needed, update_ready;
config_t cfg;




#include "entity.h"
#include "system.h"
#include "conf.h"
#include "str.h"

#endif