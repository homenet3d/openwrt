#!/bin/bash
##########################################################################
# Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
# Swinburne University of Technology.
#
# Authors:    Dominic Allan       (6513476@student.swin.edu.au)
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
##########################################################################
# Data rate checking script

PERIOD=$1


touch /var/rx /var/tx
COUNT=0
RX=()
TX=()

RX_CMD="cat /proc/eth0.2 | grep \"bytes re\" | tr -s \" \" | cut -d \" \" -f 5"
TX_CMD="cat /proc/eth0.2 | grep \"bytes tr\" | tr -s \" \" | cut -d \" \" -f 5"

if [[ ! -z `eval $RX_CMD` && ! -z `eval $TX_CMD` ]]; then
	while [[ $COUNT -lt $PERIOD ]]
	do
		RX[$COUNT]=`eval $RX_CMD`
		TX[$COUNT]=`eval $TX_CMD`
		echo ${RX[COUNT]}
		RX_WIFI[$COUNT]=`eval $RX_WIFI_CMD`
		TX_WIFI[$COUNT]=`eval $TX_WIFI_CMD`
		sleep 1
		COUNT=$((COUNT+1))
		echo -n $((PERIOD - COUNT))
		
		echo -en "\r"
	done
	while true
	do
		INDEX=$((PERIOD-1))
		if [[ "${RX[INDEX]}" -gt 0 && "${RX[0]}" -gt 0 ]]; then 
			RX_RATE=$(((${RX[INDEX]}-${RX[0]})/$PERIOD))
			echo $RX_RATE > /var/rx
			for ((i=0;i<$INDEX;i++))  
			do
				RX[i]=${RX[i+1]}
			done
			RX[$INDEX]=`eval $RX_CMD`
		else
			echo "Error: RX a number\r\n"
		fi
		if [[ "${TX[INDEX]}" -gt 0 && "${TX[0]}" -gt 0 ]]; then 
			TX_RATE=$(((${TX[INDEX]}-${TX[0]})/$PERIOD))
			echo $TX_RATE > /var/tx
			for ((i=0;i<$INDEX;i++))  
			do
				TX[i]=${TX[i+1]}
			done
			TX[$INDEX]=`eval $TX_CMD`
		else
			echo "Error: TX a number\r\n"
		fi
		
	sleep 1	
	done
fi
