/*
Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology.

Authors:    Dominic Allan       (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
*/

/*
*  This set of functions are used to parse the config file set by homenet3d -f 
*   option.
*/


#include "conf.h"

entity_config_t *configs_table = NULL;

/*
* config_start()
* description:
*   This function starts reading the full configuration and kicks off
*   the parsing of it into a config hash table. 
* 
* returns:
*   a status number depending on the success of the reading a config file.
*
*/
int config_start(const char* filename){
  config_init(&cfg);
  fprintf(stderr,"Checking Config File: %s\n",filename);
	if(! config_read_file(&cfg,filename)){
		lwsl_err("ERROR: main() . %s:%d - %s\n", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
    fprintf(stderr,"ERROR: main() . %s:%d - %s\n", config_error_file(&cfg), config_error_line(&cfg), config_error_text(&cfg));
		config_destroy(&cfg);
    return 1;
	}
  else{
    fprintf(stderr,"Loading Config from: %s\n",filename);
    config_set_auto_convert(&cfg,1);
    parse_config(config_lookup(&cfg,"application"));
    return 0;
  }
}

/*
* parse_app_config()
* description:
*   This function parses the general application settings:
*     Name
*     Version
*     Webpage URL
* 
* passed:
*   name: a char buffer in which Name is printed
*   version: a char buffer in which Version is printed
*   url: a char buffer in which Webpage URL is printed
*
*/
void parse_app_config(char * name, char * version, char * url){
  config_setting_t *setting = config_lookup(&cfg,"name");
  sprintf(name,"%s",config_setting_get_string(setting));
  setting = config_lookup(&cfg,"version");
  sprintf(version,"%s",config_setting_get_string(setting));
  setting = config_lookup(&cfg,"url");
  sprintf(url,"%s",config_setting_get_string(setting));
}

/*
* config_get_s_port()
* description:
*   Gets the UDP socket port specified in the config file. This port is
*   listened on for system state updates from the localhost and other 
*   slave host routers.
* 
* returns:
*   an integer of the UDP socket port.
*
*/
int config_get_s_port(){
  config_setting_t *setting = config_lookup(&cfg,"port_socket");
  return config_setting_get_int(setting);
}

/*
* config_get_ws_port()
* description:
*   Gets the WebSocket port specified in the config file. This port
*   is what's used to send Homenet3D server updates to the web clients.
* 
* returns:
*   an integer of the WebSocket port.
*
*/
int config_get_ws_port(){
  config_setting_t *setting = config_lookup(&cfg,"port_ws");
  return config_setting_get_int(setting);
}

/*
* get_entity_count(group_id)
* description:
*   This function gets the number of system groups.
* 
* passed:
*   group_id: the ID of the group
*	
* returns:
*   an integer of the number of entities in a specific group.
*
*/
int get_entity_count(int group_id){
  config_setting_t *config_setting = config_lookup(&cfg,"application.systems");
  config_setting_t *entity_group = config_setting_get_member(config_setting_get_elem(config_setting,group_id),"entities");
  return config_setting_length(entity_group);
}

/*
* get_group_count()
* description:
*   This function gets the number of system groups.
* 	
* returns:
*   an integer of the number of system groups.
*
*/
int get_group_count(){
  config_setting_t *config_setting = config_lookup(&cfg,"application.systems");
  return config_setting_length(config_setting);
}

/*
* get_ip(buf,group_id)
* description:
*   This function gets the IP address of the host of the specified group.
* 	
* passed:
*   buf: a char buffer that the IP as a string is printed to.
*   group_id: the ID of the group who's IP is retrieved.
*
*/
void get_ip(const char *buf, int group_id){
  config_setting_t *setting, *elem_setting,*memb_setting;
  setting = config_lookup(&cfg,"application.systems");
  elem_setting = config_setting_get_elem(setting,group_id);
  memb_setting = config_setting_get_member(elem_setting,"host_ip");
  if(memb_setting){
    sprintf((char*)buf,"%s",config_setting_get_string(memb_setting));
  }
  else{
    sprintf((char*)buf,"127.0.0.1");
  }
}

/*
* get_entity_config(group_id,id)
* description:
*   This function gets the full configuration for an entity given it's 
*   id and the ID of its group.
* 	
* passed:
*   group_id: the ID of the group the entity belongs to.
*   id: the ID of the entity.
*
* returns:
*   An entity object that is identified by 'id' and it's group's 'group_id'
*   
*/
entity_t get_entity_config(int group_id, int id){
  entity_t ent;
  entity_config_t *global = find_entry(-100);
  entity_config_t *group_global = find_entry(group_id);
  entity_config_t *ent_config = find_entry(group_id + id);
  init_entity(&ent);
  ent = get_entity(&ent, &(global->entity));
  ent = get_entity(&ent, &(group_global->entity));
  ent = get_entity(&ent, &(ent_config->entity));
  
  return ent;
}

/*
* get_entity(source,ent_config)
* description:
*   This function compares two entities and merges any valid settings 
*   from ent_config to source.
* 	
* passed:
*   source: a pointer to the resulting entity.
*   ent_config: a pointer to the configurations to merge.
*
* returns:
*   a entity with a combination of the source configurations and the
*   valid configurations of ent_config.
*   
*/
entity_t get_entity(entity_t *source, entity_t *ent_config){
	double gran, max, min;
	char str[10]; 
  entity_t entity = *source;
  int i = 0,j = 0;
  if(ent_config->state_type_count > 0)
    entity.state_type_count = ent_config->state_type_count;
  if(ent_config->state_types){
    entity.state_types = (const char **)malloc(entity.state_type_count*sizeof(void*));
    for(i = 0; i < entity.state_type_count; i++){
      entity.state_types[i] = (char *)malloc(strlen(ent_config->state_types[i])* sizeof(char));
      entity.state_types[i] = ent_config->state_types[i];
    }
  }
  if(entity.position == NULL){
    entity.position = (int *)malloc(3 * sizeof(int));
    entity.position[X_COORD] = 0;
    entity.position[Y_COORD] = 0;
    entity.position[Z_COORD] = 0;
  }
  if (ent_config->position){    
    entity.position[X_COORD] = entity.position[X_COORD] + ent_config->position[X_COORD];
    entity.position[Y_COORD] = entity.position[Y_COORD] + ent_config->position[Y_COORD];
    entity.position[Z_COORD] = entity.position[Z_COORD] + ent_config->position[Z_COORD];
  }
  if(entity.rotation == NULL){
    entity.rotation = (double *)malloc(3 * sizeof(double));
    entity.rotation[X_COORD] = 0;
    entity.rotation[Y_COORD] = 0;
    entity.rotation[Z_COORD] = 0;
  }
  if(ent_config->rotation){
    entity.rotation[X_COORD] = ent_config->rotation[X_COORD];
    entity.rotation[Y_COORD] = ent_config->rotation[Y_COORD];
    entity.rotation[Z_COORD] = ent_config->rotation[Z_COORD];
  }
  if(entity.colour == NULL)
    entity.colour = (int *)malloc(3 * sizeof(int));
  if(ent_config->colour){
    entity.colour[RED] = ent_config->colour[RED];
    entity.colour[GREEN] = ent_config->colour[GREEN];
    entity.colour[BLUE] = ent_config->colour[BLUE];
  }
  if(entity.text_colour == NULL)
    entity.text_colour = (int *)malloc(3 * sizeof(int));
  if(ent_config->text_colour){
    entity.text_colour[RED] = ent_config->text_colour[RED];
    entity.text_colour[GREEN] = ent_config->text_colour[GREEN];
    entity.text_colour[BLUE] = ent_config->text_colour[BLUE];
  }
  if(ent_config->shape >= 0)
    entity.shape = ent_config->shape;
  if(ent_config->label)
    entity.label = ent_config->label;
  if(ent_config->bounce_height >= 0)
    entity.bounce_height = ent_config->bounce_height;
  if(ent_config->rotate_speed >= 0)
    entity.rotate_speed = ent_config->rotate_speed;
  if(ent_config->bounce_freq >= 0.0)
    entity.bounce_freq = ent_config->bounce_freq;
  if(ent_config->scale >= 0.0)
    entity.scale = ent_config->scale;
  if(ent_config->radius >= 0)
    entity.radius = ent_config->radius;
  if(ent_config->width >= 0)
    entity.width = ent_config->width;
  if(ent_config->height > 0)
    entity.height = ent_config->height;
  if(entity.autoscale == NULL && ent_config->autoscale)
    entity.autoscale = ent_config->autoscale;
  if(entity.url == NULL && ent_config->url)
    entity.url = ent_config->url;
  if(entity.objfile == NULL && ent_config->objfile)
    entity.objfile = ent_config->objfile;
  if(entity.mtlfile == NULL && ent_config->mtlfile)
    entity.mtlfile = ent_config->mtlfile;
  if(entity.mappings == NULL && ent_config->mappings){
    entity.mappings_count = ent_config->mappings_count;
    entity.mappings = (mapping_t*)malloc(ent_config->mappings_count * sizeof(mapping_t));
    for(i = 0; i < entity.mappings_count; i++){
      entity.mappings[i].attrs_count = ent_config->mappings[i].attrs_count;
      entity.mappings[i].metric = ent_config->mappings[i].metric;
      
      entity.mappings[i].metric.name = (const char *)malloc(strlen(ent_config->mappings[i].metric.name)* sizeof(char));
      entity.mappings[i].metric.name = ent_config->mappings[i].metric.name;

      entity.mappings[i].metric.upper_thres = (const char *)malloc(strlen(ent_config->mappings[i].metric.upper_thres)* sizeof(char));
      entity.mappings[i].metric.upper_thres = ent_config->mappings[i].metric.upper_thres;
      
      entity.mappings[i].metric.lower_thres = (const char *)malloc(strlen(ent_config->mappings[i].metric.lower_thres)* sizeof(char));
      entity.mappings[i].metric.lower_thres = ent_config->mappings[i].metric.lower_thres;
      
      entity.mappings[i].attrs = (attr_t *)malloc(entity.mappings[i].attrs_count *sizeof( attr_t));
      for(j = 0; j < entity.mappings[i].attrs_count; j++){
        
        entity.mappings[i].attrs[j].max = (const char *)malloc(strlen(ent_config->mappings[i].attrs[j].max)* sizeof(char));
        entity.mappings[i].attrs[j].max = ent_config->mappings[i].attrs[j].max;
                           
        entity.mappings[i].attrs[j].min = (const char *)malloc(strlen(ent_config->mappings[i].attrs[j].min)* sizeof(char));
        entity.mappings[i].attrs[j].min = ent_config->mappings[i].attrs[j].min;
        
				if(ent_config->mappings[i].attrs[j].gran){
        	entity.mappings[i].attrs[j].gran = (const char *)malloc(strlen(ent_config->mappings[i].attrs[j].gran)* sizeof(char));
        	entity.mappings[i].attrs[j].gran = ent_config->mappings[i].attrs[j].gran;
				}
				else{
					sscanf(entity.mappings[i].attrs[j].max,"%lf",&max);
					sscanf(entity.mappings[i].attrs[j].max,"%lf",&min);
					gran = (max - min)/100;
					sprintf(str,"%f",gran);
        	entity.mappings[i].attrs[j].gran = (const char *)malloc(strlen(str)* sizeof(char));
        	entity.mappings[i].attrs[j].gran = str;
				}
                           
        entity.mappings[i].attrs[j].name = (const char *)malloc(strlen(ent_config->mappings[i].attrs[j].name)* sizeof(char));
        entity.mappings[i].attrs[j].name = ent_config->mappings[i].attrs[j].name;
      }
    }
  }
  
  return entity;
}

/*
* parse_config(config_setting)
* description:
*   Wrapper function that calls parsing functions for all the groups.
* 	
* passed:
*   config_setting: the config setting object that is pointing to the global 
*   application settings.
*   
*/
void parse_config(config_setting_t *config_setting){
  config_setting_t *setting;
  int i,size;
  entity_t global;
  init_entity(&global);
  size = config_setting_length(config_setting);
  for(i = 0; i < size; i++){
    setting = config_setting_get_elem(config_setting,i);
    if(config_setting_is_list(setting) == CONFIG_TRUE ){
      parse_entity_groups(setting);
    }
    else
      parse_entity_attr(&global,setting);
  }
  add_entry(-100,global);
}

/*
* parse_entity_groups(setting)
* description:
*   Wrapper function that calls parsing functions for all the groups.
* 	
* passed:
*   setting: the config setting object that is pointing to the systems groups.
*   
*/
void parse_entity_groups(config_setting_t * setting){
  int i,j, no_groups, group_elems;
  config_setting_t *group_setting, *group_elem_setting;
  no_groups = config_setting_length(setting);
  entity_t global;
  for(i = 0; i < no_groups; i++)
  {
    init_entity(&global);
    group_setting = config_setting_get_elem(setting,i);
    group_elems = config_setting_length(group_setting);
    for(j = 0; j < group_elems; j++)
    {
      group_elem_setting = config_setting_get_elem(group_setting,j);
      if(config_setting_is_list(group_elem_setting) == CONFIG_TRUE )
        parse_entities(group_elem_setting,i);
      else
        parse_entity_attr(&global,group_elem_setting);
    }
    add_entry(i*100,global);
    // print_entity(&(find_entry(i*100)->entity));
  }
  
}

/*
* parse_entities(setting,group_id)
* description:
*   Wrapper function that calls parsing function depending on the type of
*   config file variable for a specific entity.
* 	
* passed:
*   entity: a pointer to the entity object that the information will be 
*     parsed to
*   setting: the config setting object that is pointing to the list of 
*     entities.
*   
*/
void parse_entities(config_setting_t *setting, int group_id){
  int size = config_setting_length(setting),i,j, sub_size;
  config_setting_t *subsetting, *entity_setting;
  entity_t entity;
  for(i = 0; i < size; i++){
    init_entity(&entity);
    entity_setting = config_setting_get_elem(setting,i);
    sub_size = config_setting_length(entity_setting);
    for(j = 0; j < sub_size; j++){
      subsetting = config_setting_get_elem(entity_setting,j);
      parse_entity_attr(&entity,subsetting);
    }
    add_entry(i+1+(group_id*100),entity);
  }
}

/*
* parse_entity_attr(entity,setting)
* description:
*   Wrapper function that calls parsing functions depending on the type of
*   config file variable for a specific entity.
* 	
* passed:
*   entity: a pointer to the entity object that the information will be 
*     parsed to
*   setting: the config setting object that is pointing to the variable 
*     in the config file
*   
*/
void parse_entity_attr(entity_t *entity, config_setting_t *setting){
  if(config_setting_is_array(setting) == CONFIG_TRUE)
    parse_array(entity,setting);
  else if(config_setting_is_list(setting) == CONFIG_TRUE)
    parse_list(entity,setting);   
  else
    parse(entity,setting);
}

/*
* parse_list(entity,setting)
* description:
*   Wrapper function that parses a list of elements in an entity specification.
*   E.G. mappings
* 	
* passed:
*   entity: a pointer to the entity object whose attributes will be set
*   setting: the config setting object that is pointing to the list setting 
*     in the config file
*   
*/
void parse_list(entity_t *entity,config_setting_t *setting){
  int i;
  config_setting_t *subsetting;
  if(strcmp(config_setting_name(setting),"mappings") == 0){
    entity->mappings_count = config_setting_length(setting);
    entity->mappings = (mapping_t *)malloc(sizeof( mapping_t)*(entity->mappings_count));
    for(i = 0; i < entity->mappings_count; i++){
      subsetting = config_setting_get_elem(setting,i);
      parse_mapping(&(entity->mappings[i]),subsetting);
    }
  }
}

/*
* parse_array(entity,setting)
* description:
*   Parses an attribute that is specified as an array in the config file.
*   E.G. state_types, colour, position, rotation
* 	
* passed:
*   entity: a pointer to the entity object whose attribute will be set
*   setting: the config setting object that is pointing to the attribute 
*     information in the config file
*   
*/
void parse_array(entity_t *entity, config_setting_t *setting){
  int size = config_setting_length(setting),i;
  config_setting_t *subsetting;
  if(strcmp(config_setting_name(setting),"state_types") == 0){
    entity->state_type_count = size;
    entity->state_types = (const char **)malloc(size*sizeof(void*));
    for(i = 0; i < size; i++){
      subsetting = config_setting_get_elem(setting,i);
      entity->state_types[i] = (char *)malloc(strlen(config_setting_get_string(subsetting))* sizeof(char));
      entity->state_types[i] = config_setting_get_string(subsetting);
    }
  }
  else if(strcmp(config_setting_name(setting),"position") == 0){
    entity->position = (int *)malloc(config_setting_length(setting)* sizeof(int));
    subsetting = config_setting_get_elem(setting,X_COORD);
    entity->position[X_COORD] = config_setting_get_int(subsetting);
    subsetting = config_setting_get_elem(setting,Y_COORD);
    entity->position[Y_COORD] = config_setting_get_int(subsetting);
    subsetting = config_setting_get_elem(setting,Z_COORD);
    entity->position[Z_COORD] = config_setting_get_int(subsetting);
  }
  else if(strcmp(config_setting_name(setting),"rotation") == 0){
    entity->rotation = (double *)malloc(config_setting_length(setting)* sizeof(double));
    subsetting = config_setting_get_elem(setting,X_COORD);
    entity->rotation[X_COORD] = config_setting_get_float(subsetting);
    subsetting = config_setting_get_elem(setting,Y_COORD);
    entity->rotation[Y_COORD] = config_setting_get_float(subsetting);
    subsetting = config_setting_get_elem(setting,Z_COORD);
    entity->rotation[Z_COORD] = config_setting_get_float(subsetting);
  }
  else if(strcmp(config_setting_name(setting),"colour") == 0){
    entity->colour = (int *)malloc(config_setting_length(setting)* sizeof(int));
    subsetting = config_setting_get_elem(setting,RED);
    entity->colour[RED] = config_setting_get_int(subsetting);
    subsetting = config_setting_get_elem(setting,GREEN);
    entity->colour[GREEN] = config_setting_get_int(subsetting);
    subsetting = config_setting_get_elem(setting,BLUE);
    entity->colour[BLUE] = config_setting_get_int(subsetting);
  }
  else if(strcmp(config_setting_name(setting),"text_colour") == 0){
    entity->text_colour = (int *)malloc(config_setting_length(setting)* sizeof(int));
    subsetting = config_setting_get_elem(setting,RED);
    entity->text_colour[RED] = config_setting_get_int(subsetting);
    subsetting = config_setting_get_elem(setting,GREEN);
    entity->text_colour[GREEN] = config_setting_get_int(subsetting);
    subsetting = config_setting_get_elem(setting,BLUE);
    entity->text_colour[BLUE] = config_setting_get_int(subsetting);
  }
}

/*
* parse_mapping(metric,setting)
* description:
*   Wrapper function the parses a mapping and its multiple attributes and its
*   metric calling parse_attr and parse_mapping_metric respectively.
* 	
* passed:
*   metric: a pointer to the metric object whose attribute will be set
*   setting: the config setting object that is pointing to the attribute 
*     information in the config file
*   
*/
void parse_mapping(mapping_t* mapping,config_setting_t *setting){
  int size = config_setting_length(setting), i,j;
  config_setting_t *subsetting, *attr_setting;
  for(i = 0; i < size; i++){
    subsetting = config_setting_get_elem(setting,i);
    if(strcmp(config_setting_name(subsetting),"metric") == 0){
      parse_mapping_metric(&(mapping->metric),subsetting);
    }
    else if(strcmp(config_setting_name(subsetting),"attrs") == 0){
      mapping->attrs_count = config_setting_length(subsetting);
      mapping->attrs = (attr_t *)malloc(sizeof(attr_t)*(mapping->attrs_count));
      for(j = 0; j < mapping->attrs_count; j++){
        attr_setting = config_setting_get_elem(subsetting,j);
        parse_attr(&(mapping->attrs[j]),attr_setting);
      }
    }
  }
}

/*
* parse_mapping_metric(metric,setting)
* description:
*   Parses the metric thresholds for a metric mapping from the config file 
*   into the metric pointer
* 	
* passed:
*   metric: a pointer to the metric object whose attribute will be set
*   setting: the config setting object that is pointing to the attribute 
*     information in the config file
*   
*/
void parse_mapping_metric(metric_t *metric,config_setting_t *setting){
  int size = config_setting_length(setting), i;
  config_setting_t *subsetting;
  for(i = 0; i < size; i++){
    subsetting = config_setting_get_elem(setting,i);
    if(strcmp(config_setting_name(subsetting),"upper_thresh") == 0){
      metric->upper_thres = (char *)malloc(strlen(config_setting_get_string(subsetting)) * sizeof(char));
      metric->upper_thres = config_setting_get_string(subsetting);
    }
    else if(strcmp(config_setting_name(subsetting),"lower_thresh") == 0){
      metric->lower_thres = (char *)malloc(strlen(config_setting_get_string(subsetting)) * sizeof(char));
      metric->lower_thres = config_setting_get_string(subsetting);
    }
    else if(strcmp(config_setting_name(subsetting),"metric_name") == 0){
      metric->name = (char *)malloc(strlen(config_setting_get_string(subsetting)) * sizeof(char));
      metric->name = config_setting_get_string(subsetting);  
    }
  }
}

/*
* parse_attr(attr,setting)
* description:
*   Parses the attribute thresholds for a metric mapping from the config file 
*   into the attr pointer
* 	
* passed:
*   attr: a pointer to the attr object whose attribute will be set
*   setting: the config setting object that is pointing to the attribute 
*     information in the config file
*   
*/
void parse_attr(attr_t *attr,config_setting_t *setting){
  int size = config_setting_length(setting), i;
  config_setting_t *subsetting;
  for(i = 0; i < size; i++){
    subsetting = config_setting_get_elem(setting,i);
    if(strcmp(config_setting_name(subsetting),"attr_max") == 0){
      attr->max = (char *)malloc(strlen(config_setting_get_string(subsetting))* sizeof(char));
      attr->max = config_setting_get_string(subsetting);
    }
    else if(strcmp(config_setting_name(subsetting),"attr_min") == 0){
      attr->min = (char *)malloc(strlen(config_setting_get_string(subsetting))* sizeof(char));
      attr->min = config_setting_get_string(subsetting);
    }
    else if(strcmp(config_setting_name(subsetting),"attr_gran") == 0){
      attr->gran = (char *)malloc(strlen(config_setting_get_string(subsetting))* sizeof(char));
      attr->gran = config_setting_get_string(subsetting);
    }
    else if(strcmp(config_setting_name(subsetting),"attr_name") == 0){
      attr->name = (char *)malloc(strlen(config_setting_get_string(subsetting))* sizeof(char));
      attr->name = config_setting_get_string(subsetting);  
    }
  }
}


/*
* parse(entity,setting)
* description:
*   Parses the end-level attributes from the config file in an entity object
* 	
* passed:
*   entity: a pointer to the entity object whose attribute will be set
*   setting: the config setting object that is pointing to the attribute 
*     information in the config file
*   
*/
void parse(entity_t *entity, config_setting_t *setting){
  if(strcmp(config_setting_name(setting),"shape") == 0)
    entity->shape = config_setting_get_int(setting);
  else if(strcmp(config_setting_name(setting),"label") == 0){
    entity->label = (char *)malloc(strlen(config_setting_get_string(setting))* sizeof(char));
    entity->label = config_setting_get_string(setting);
  }
  else if(strcmp(config_setting_name(setting),"bounce_height") == 0)
    entity->bounce_height = config_setting_get_int(setting);
  else if(strcmp(config_setting_name(setting),"bounce_freq") == 0)
    entity->bounce_freq = config_setting_get_float(setting);
  else if(strcmp(config_setting_name(setting),"rotate_speed") == 0)
    entity->rotate_speed = config_setting_get_float(setting);
  else if(strcmp(config_setting_name(setting),"scale") == 0){
    entity->scale = config_setting_get_float(setting);
  }
  else if(strcmp(config_setting_name(setting),"radius") == 0)
    entity->radius = config_setting_get_int(setting);
  else if(strcmp(config_setting_name(setting),"width") == 0)
    entity->width = config_setting_get_int(setting);
  else if(strcmp(config_setting_name(setting),"height") == 0)
    entity->height = config_setting_get_int(setting);
  else if(strcmp(config_setting_name(setting),"autoscale") == 0){
    entity->autoscale = (const char *)malloc(strlen(config_setting_get_string(setting))* sizeof(char));
    entity->autoscale = config_setting_get_string(setting);
  }
  else if(strcmp(config_setting_name(setting),"url") == 0){
    entity->url = (const char *)malloc(strlen(config_setting_get_string(setting))* sizeof(char));
    entity->url = config_setting_get_string(setting);
  }
  else if(strcmp(config_setting_name(setting),"objfile") == 0){
    entity->objfile = (const char *)malloc(strlen(config_setting_get_string(setting))* sizeof(char));
    entity->objfile = config_setting_get_string(setting);
  }
  else if(strcmp(config_setting_name(setting),"mtlfile") == 0){
    entity->mtlfile = (const char *)malloc(strlen(config_setting_get_string(setting))* sizeof(char));
    entity->mtlfile = config_setting_get_string(setting);
  }
}



/*
* add_entry(id,entity)
* description:
*   adds an entity struct to the configs hash table
* 	
* passed:
*   id: the ID of the entry to be added.
*   entity: the entity object to be added.
*   
*/
void add_entry(int id, entity_t entity) {
  entity_config_t *s;
  HASH_FIND_INT(configs_table, &id, s);  /* id already in the hash? */
  if (s==NULL) {
    s = ( entity_config_t*)malloc(sizeof( entity_config_t));
    s->id = id;
    HASH_ADD_INT( configs_table, id, s );  /* id: name of key field */
  }
  s->entity = entity;
  s->entity.id = id;
  s->entity.group_id = id/100;
  
  if(debug)
    lwsl_notice("DEBUG: add_entry() -> Adding Config: Group %d. Entity %d\n",s->entity.group_id,s->entity.id);
}

/*
* delete_entry(config)
* description:
*   deletes an entity config from the configs hash table
* 	
* passed:
*   config: the config object to be deleted.
*   
*/
void delete_entry(entity_config_t *config) {
  HASH_DEL( configs_table, config);  /* user: pointer to delete */
  free(config);              /* optional; it's up to you! */
}

/*
* delete_all()
* description:
*  deletes all entity_config entries from the configs hash table
* 	
*/
void delete_all() {
  entity_config_t *config, *tmp;

  HASH_ITER(hh, configs_table, config, tmp) {
    HASH_DEL(configs_table,config);  /* delete; users advances to next */
    free((void *)config->entity.position);
    free((void *)config->entity.colour);
    free((void *)config->entity.text_colour);
    free((void *)config->entity.rotation);
  	free((void *)config->entity.label);
    free((void *)config->entity.url);
    free((void *)config->entity.objfile);
    free((void *)config->entity.mtlfile);
    free((void *)config->entity.state_types);
    free((void *)config->entity.mappings);
    free(config);   /* optional- if you want to free  */
  }
}

/*
* find_entry(id)
* description:
*   Gets a pointer to a entity_config_t object for a given hash table id
* 	
* passed:
*   id: the entity_config ID
*
* returns:
*   A pointer to the to entity_config_t table entry assocaited with the id.
*   If the entry cannot be matched it returns a NULL pointer.
*   
*/
entity_config_t * find_entry(int id) {
  entity_config_t *s = NULL;

  HASH_FIND_INT( configs_table, &id, s );  /* s: output pointer */
  return s;
}
