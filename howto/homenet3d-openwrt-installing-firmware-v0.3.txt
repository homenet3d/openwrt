-----------------------------------------------------------------------------
Homenet3D for OpenWRT (v0.3): Installing Firmware (released 22nd August 2014)
-----------------------------------------------------------------------------

This document is part of Homenet3D for OpenWRT:
http://caia.swin.edu.au/urp/homenet3d/openwrt

It provides instructions for installing OpenWRT firmware and Homenet3D 
package on the TP-Link WR1043ND.

If you are using a TP Link WR1043ND a pre-packaged OpenWRT firmware is 
available at: 
http://caia.swin.edu.au/urp/homenet3d/downloads/

For instructions on building firmware, see: 
http://caia.swin.edu.au/urp/homenet3d/downloads/homenet3d-building-firmware-v0.3.txt

These instructions are specified for the TP-Link WR1043ND home router. 
They are fairly general though and you should be able to apply the same 
process to other routers. Make sure you build the RIGHT firmware for 
YOUR router!

Firmware install instructions for specific routers can be found at the 
OpenWRT website: http://wiki.openwrt.org/doc/howto/generic.flashing

INTRODUCTION
------------

The Homenet3D project is made up of two parts - the system server and the 
client page. The system server is made up of a compiled-C server and a Lua
and a bash script as helper daemons. The client page contains an HTML page
and multiple Javascript files that present and then render the virtual 
world.

The recommended method for installing new OpenWRT firmware is via the Web 
Interface (for stock firmware and for existing OpenWRT).

If you are looking for a more hands on approach or you have bricked your 
router, you may want to try hard-wiring a serial port to the router. You
will then need to install to firmware from Uboot over a serial connection, 
installing the firmware over TFTP.


WEB INTERFACE INSTALL
---------------------

Log into the Web Interface (using your existing username and password). 
Find the option to 'Flash new firmware image'. For existing OpenWRT 
installations this is under System -> Backup/Flash Firmware. For the stock 
firmware this is under System Tools -> Flash Firmware.

If you want to keep the existing settings make sure you tick 'keep settings'
under 'Flash new firmware image'. For the stock firmware this option is now
available, your settings will be ERASED. 

The interface will check the MD5 hash and ask for confirmation before
proceeding. The new firmware will be installed and the router will reboot.
Wait a couple of minutes then follow the FIRST BOOT INSTRUCTIONS.


SERIAL AND TFTP INSTALL
-----------------------

This is a slightly more complicated method for installing new firmware, 
and requires that the router has been modified with a serial port. You 
will also need to be running a TFTP server that is hosting the firmware 
images. Again note that these instructions are SPECIFIC FOR THE TP-LINK 
WR-1043ND router. There is also a greater potential for BRICKING YOUR 
ROUTER using this method, so try the other methods first. 
 
It is also worth visiting the OpenWRT page on the subject: 	
  - http://wiki.openwrt.org/doc/howto/generic.flashing.tftp

We have used this method and can verify that it will work for the WR1043ND.
This needs to be done via a serial console. See 
  - http://wiki.openwrt.org/toh/tp-link/tl-wr1043nd

To get into uboot, boot the router and when you see "Autoloading.." type 
in 'tpl' and hit enter.	

Please connect the Ethernet to the WAN port.

$ setenv ipaddr 192.168.1.10    # The IP of the router
$ setenv serverip 192.168.1.16  # The IP of the TFTP server

# This erases the flash between uboot and the firmware configuration area.  
# Whatever you do, don't mistype this!
$ erase 0xbf020000 +7c0000

# Load the firmware image into RAM
$ tftpboot 0x81000000 homenet3d-openwrt-tl-wr1043nd-v0.1.bin

# Copy the firmware image from RAM to the flash.  
# Again, don't mess this line up!
$ cp.b 0x81000000 0xbf020000 0x7c0000

# Transfer control to the kernel.
$ bootm 0xbf020000

As you are connected via serial you won't need to telnet in on the first 
boot up. Once booted disconnect the Ethernet cable from the WAN port and
connect it to one the LAN ports.


FIRST BOOT INSTRUCTIONS
-----------------------
If you cleared out your settings then the usual way to connect to your 
router may not work. If you connect to router on one of it's LAN ports, 
your computer should get DHCP lease of 192.168.1.*. In a browser go to 
192.168.1.1 and it should give you the OpenWRT Luci login. There is no 
password but the user interface will inform you on how to change it. 

Once you are there you can set up your desired settings include LAN and 
WLAN settings. Go nuts.

Multi-router Setup
------------------
By default Homenet3D is configured for a single router (the one it is
running on). Homenet3D v0.3 is capable of serving information about 
multiple routers on a home network. This requires some tweaks to the
configuration file. The package comes with an example config file at
/usr/local/share/homenet3D/homenet3d-multi.cfg

Instructions on how to configure all the routers in a multi-router setup
please see the MULTI-ROUTER section in:
http://caia.swin.edu.au/urp/homenet3d/downloads/homenet3d-starting-up-v0.3.txt

 
IF SOMETHING GOES WRONG
-----------------------
SSH into the router:
 ssh root@192.168.1.1
Try restarting the services:
~: /etc/init.d/homenet3d-server restart
~: /etc/init.d/sys-state restart
~: /etc/init.d/datarate restart 

Serial Cable:
A serial cable can help you access uboot and install new firmware if 
something goes really wrong and the two methods above don't work. Also 
if you are completely locked out. 

If you router lights all continuously blink at the same time when you 
power it up, a serial cable may not help. It may be bricked beyond repair.


CONTRIBUTIONS
-------------

We welcome your feedback and contributions! If you have discovered a bug,
have a request for a new feature or just want to ask a question, please 
contact us using the details below.


CONTACT
-------

The Homenet3D website is: http://www.caia.swin.edu.au/urp/homenet3d

If you have any questions or want to report any bugs please contact:
	Primary developer: Dominic Allan	(domallan8@gmail.com)
	Project lead: Grenville Armitage	(garmitage@swin.edu.au)

Centre for Advanced Internet Architectures
Swinburne University of Technology
Melbourne, Australia
CRICOS number 00111D
http://www.caia.swin.edu.au
