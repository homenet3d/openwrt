--------------------------------------------------
HOMENET3D v0.3 README  (released 22nd August 2014)
--------------------------------------------------

This document is part of the Homenet3D for OpenWrt Project
(http://caia.swin.edu.au/urp/homenet3d/openwrt)
which is part of the Homenet3D Project
(http://caia.swin.edu.au/urp/homenet3d/)


OVERVIEW
--------

Homenet3D is a project intended to represent the home network in a user-friendly
3D virtual world on portable and fixed end-user devices (such as smart phones,
tablets and PCs).

Homenet3D for OpenWRT v0.3 is our latest prototype of this idea. This system
presents the current system state of one of more OpenWRT devices inside a virtual 
world, rendered in any WebGL-enabled web browser. The actual 3D world presented in 
v0.3 is still rudimentary -- the code is intended as a platform for future
experiments in browser-side 3D visualisation techniques.


DOCUMENTATION
-------------

For installation instructions, see INSTALL.txt


Homenet3D is made up of a WebSockets server[5] and helper scripts on the server side 
and a Javascript/HTML client. The client and server communicate over WebSockets. 
The server itself sits on the home router and monitors system and network state,
constantly updating the server and therefore updating the virtual world presented 
to the client.

Homenet3D v0.3 can be used for a single router environment or a multi-router 
environment. For a multi-router environment the intention is to have one master
Homenet3D router and every additionally router being a slave. In this situation
the master runs a full version of the Homenet3D server (WebSockets server and 
helper scripts) and each slave running only the helper scripts which send there 
updates to the Homenet3D server instance on the master.

The mapping of system state is highly configurable (thanks to libconfig[6]). Any 
number of 3D entity can be used to represent any number of blocks of system state 
information. 

The system state updates are communicated on a local UDP port using C sockets and 
Lua sockets. 

The default WebSocket port used between server and client is 10001. The default UDP
port used internally on the server side is 27960 (legacy port for L3dgecomms [2]).
These ports can be configured in the Homenet3D configuration file [4].
The web pages for the client are served on default HTTP port (80) by a lightweight 
web server (uhttpd).

The client-side javascript was originally based on a previous project (W3bworld) [1]. 
But in v0.2 was moved to a new 3D javascript library: Three.js[3]. The implementation
has been custom made, taking what was needed from W3bworld and Homenet3D v0.1. This 
implementation is a collection of javascript files described below

* js/websockets.js:     contains websocket communication interpretation
* js/camera.js:         contains camera initialisation and manipulation functionality
* js/group.js:          contains the Group object and all the functionality that it
                        can perform
* js/entity.js:         contains the Entity object and all the functionality that it
                        can perform
* js/metrics.js:        contains the functionality for parsing and performing metric 
                        mappings
* js/info.js:           contains the functionality to display information in the infobox
* js/device.js:         contains Device Spawning functionality.

In addition to these, there are a number of third party libraries in use:

* jquery-1.11.0.min.js: jQuery, used for simpler DOM manipulation and JS
                        shorthand
* threejs/:             A selection of core Three.js library (r65) and extensions, 
                        added as needed (DOM events, OBJ/MTL models, TrackBall 
                        controls, Detector)

The server is separated out in to a server process and two helper daemons:

* /bin/homenet3d-server: contains entity management and virtual world state
                        In a multirouter network this is only needed on the
                        master server.
* /bin/sys-state:       Lua script to get and relay state updates to homenet3d-server
* /bin/datarate:        bash script that calculate average datarates on net interfaces
* /root/demo.lua:       A lua script used to simulate system state updates


To start using Homenet3D, read:
 howto/homenet3d-openwrt-starting-up-v0.3.txt
	

REFERENCES
----------

[1] http://caia.swin.edu.au/urp/l3dge/tools/w3bworld

[2] http://caia.swin.edu.au/reports/080222C/CAIA-TR-080222C.pdf

[3] http://threejs.org/

[4] http://caia.swin.edu.au/reports/140702B/CAIA-TR-140702B.pdf

[5] https://github.com/warmcat/libwebsockets

[6] http://www.hyperrealm.com/libconfig/

LICENSE
-------

Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
Swinburne University of Technology. 

Authors:    Dominic Allan        (6513476@student.swin.edu.au)

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.



CONTACT
-------

The Homenet3D for OpenWrt website is: http://www.caia.swin.edu.au/urp/homenet3d/openwrt
The Homenet3D website is: http://www.caia.swin.edu.au/urp/homenet3d/

If you have any questions or want to report any bugs please contact:
	Primary developer: Dominic Allan	(domallan8@gmail.com)
	Project lead: Grenville Armitage	(garmitage@swin.edu.au)

Centre for Advanced Internet Architectures
Swinburne University of Technology
Melbourne, Australia
CRICOS number 00111D
http://www.caia.swin.edu.au


