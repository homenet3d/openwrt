##########################################################################
# Copyright (c) 2013-2014 Centre for Advanced Internet Architectures,
# Swinburne University of Technology.
#
# Authors:    Dominic Allan       (6513476@student.swin.edu.au)
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.
##########################################################################

include $(TOPDIR)/rules.mk

PKG_NAME:=homenet3d
PKG_VERSION:=v0.3
PKG_SRC_URL:=http://caia.swin.edu.au/urp/homenet3d/downloads/
PKG_SRC:=homenet3d-openwrt-src-$(PKG_VERSION).tar.gz
PKG_BUILD_DIR:=$(BUILD_DIR)/$(PKG_NAME)-$(PKG_VERSION)

include $(INCLUDE_DIR)/package.mk

define Package/homenet3D
	SECTION:=Homenet3D
	CATEGORY:=Homenet3D
	TITLE:=Homenet3D server
	DEPENDS:=+libwebsockets +luci +bash +luasocket +libconfig +ip6tables
endef

define Build/Prepare
	-rm -rf src 
	mkdir -p $(PKG_BUILD_DIR)
	wget $(PKG_SRC_URL)$(PKG_SRC) && tar -zxvf $(PKG_SRC) -C $(PKG_BUILD_DIR)
	mv $(PKG_BUILD_DIR)/homenet3D-src/* $(PKG_BUILD_DIR)/
	rm -rf $(PKG_SRC) $(PKG_BUILD_DIR)/src/
endef


define Package/homenet3D/install
	mkdir -p $(1)/etc/config $(1)/root $(1)/etc/init.d $(1)/etc/rc.d $(1)/www/w3b $(1)/www/hnet3d $(1)/usr/local/share/$(PKG_NAME)
	$(INSTALL_DIR) $(1)/bin
#	$(INSTALL_BIN) $(PKG_BUILD_DIR)/rc.local $(1)/etc/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/homenet3d $(1)/bin/homenet3D-server
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/sys-state.lua $(1)/bin/sys-state
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/demo.lua $(1)/bin/demo
	$(CP) $(PKG_BUILD_DIR)/uhttpd-conf $(1)/etc/config/uhttpd
	$(CP) $(PKG_BUILD_DIR)/network $(1)/etc/config/network
	$(CP) $(PKG_BUILD_DIR)/hnet3d/* $(1)/www/hnet3d
	$(CP) $(PKG_BUILD_DIR)/*.cfg $(1)/usr/local/share/$(PKG_NAME)/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/hn-init $(1)/etc/init.d/homenet3D-server
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/hn-init $(1)/etc/rc.d/S65homenet3D-server
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/demo-init $(1)/etc/init.d/demo
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/band-init $(1)/etc/init.d/datarate
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/band-init $(1)/etc/rc.d/S66datarate
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/sys-state-init $(1)/etc/init.d/sys-state
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/sys-state-init $(1)/etc/rc.d/S67sys-state
endef

$(eval $(call BuildPackage,homenet3D))
